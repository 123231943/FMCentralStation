#pragma once

#include "FM_STRUCTS.h"

// CClientSocket ����Ŀ��
class CMainDlg;
class CClientSocket : public CAsyncSocket
{
private:
	CMainDlg* m_pu;
	BYTE m_psBuf[SOCKET_BUFFER_SIZE];
	int m_nBufLen;

public:
	CClientSocket(CMainDlg* pu);
	virtual ~CClientSocket();
	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
};


