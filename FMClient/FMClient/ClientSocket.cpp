// ClientSocket.cpp : 实现文件
//

#include "stdafx.h"
#include "FM_STRUCTS.h"
#include "MainDlg.h"
#include "ClientSocket.h"

// CClientSocket

CClientSocket::CClientSocket(CMainDlg* pu)
	: m_pu(pu)
	, m_nBufLen(0)
{
	ZeroMemory(m_psBuf, SOCKET_BUFFER_SIZE);
}

CClientSocket::~CClientSocket()
{
	m_pu = NULL;
	m_nBufLen = 0;
}

// CClientSocket 成员函数
void CClientSocket::OnConnect(int nErrorCode)
{
	if (m_pu) {
		m_pu->OnSocketConnect(nErrorCode);
	}
	CAsyncSocket::OnConnect(nErrorCode);
}

void CClientSocket::OnClose(int nErrorCode)
{
	if (m_pu) {
		m_pu->OnSocketClose(nErrorCode);
	}
	CAsyncSocket::OnClose(nErrorCode);
}

void CClientSocket::OnReceive(int nErrorCode)
{
/*
	int i, j;
	int parse_ret;
	int data_size;
	int expect_size;

	//接着上次的位置收数据
	memcpy(&(m_psUserBuf[m_nUserBufLen]), buf, size);
	m_nUserBufLen += size;

	CString info;
	info.Format(_T("\nReceiveProc，床号：%d\n"), m_nIndex+1);
	OutputDebugString(info);

	while (TRUE) {
		parse_ret = CStructTools::CheckData(m_psUserBuf, m_nUserBufLen, &data_size, &expect_size);
		if (parse_ret > 0) {
			CString info;
			info.Format(_T("CheckData 残留数据：%d字节\n"), parse_ret);
			OutputDebugString(info);
			//在“包起始标志”之前有parse_ret个字节的残留数据
			m_nUserBufLen -= parse_ret;
			for (i = 0, j = parse_ret; i < m_nUserBufLen; i++, j++) {
				m_psUserBuf[i] = m_psUserBuf[j];
			}
			//通知：清除了一定数量的无效数据。（以负数约定此类错误）
			OnReceiveError(-parse_ret);
			//去除多余数据后，应再次分析数据是否符合要求
			parse_ret = CStructTools::CheckData(m_psUserBuf, m_nUserBufLen, &data_size, &expect_size);
		}
		if (parse_ret == 0) {
			CString info;
			info.Format(_T("CheckData OK，可以处理数据。\n"));
			OutputDebugString(info);
			//正常，可以处理数据
			OnUserDataReceive((SOCK_PKG*)m_psUserBuf);

			//删除已经处理过的数据
			m_nUserBufLen -= data_size;
			for (i = 0, j = data_size; i < m_nUserBufLen; i++, j++) {
				m_psUserBuf[i] = m_psUserBuf[j];
			}
			if (m_nUserBufLen > 0) {
				continue;  //m_psBuf中还有数据没处理完
			}
			else {
				break;  //完美结束
			}
		}
		if (parse_ret == -1) {
			CString info;
			info.Format(_T("CheckData 期待数据：%d字节\n"), expect_size);
			OutputDebugString(info);
			//表示还没接收完，继续
			break;
		}
		if (parse_ret == -2) {
			CString info;
			info.Format(_T("！！！CheckData 出错，后面没法处理。\n"));
			OutputDebugString(info);
			theApp.WriteLog(_T("CFMSocketUnit::ReceiveProc()函数\n"));
			theApp.WriteLog(info);
			//接收出错，后面的没法处理了，清缓冲区。（以负数约定此类错误）
			OnReceiveError(-m_nUserBufLen);
			m_nUserBufLen = 0;
		}
	}

	//继续接收，或者接收下一个数据包头
	if (expect_size > 0) {
		CString info;
		info.Format(_T("期待字节：%d\n"), expect_size);
		OutputDebugString(info);
		return expect_size;
	}
	else {
		CString info;
		info.Format(_T("期待字节：%d\n"), sizeof(HEAD));
		OutputDebugString(info);
		return sizeof(HEAD);
	}
*/
	int i, j;
	int recv_ret;
	int parse_ret;
	int data_size;
	int expect_size;

	if (nErrorCode != 0) {
		if (m_pu) {
			m_pu->OnSocketReceiveError(nErrorCode);
		}
	}

	//接着上次的位置收数据
	//注意，在OnReceive中，只能Receive一次
	//如果buffer中还有数据，那么基类还会再次调用OnReceive函数
	recv_ret = Receive((&m_psBuf[m_nBufLen]), SOCKET_BUFFER_SIZE-m_nBufLen);
	if (recv_ret == 0) { //连接关闭了
		goto fin;
	}
	else if (recv_ret == SOCKET_ERROR) {
		goto fin;
	}
	m_nBufLen += recv_ret;

	while (TRUE) {
		parse_ret = CStructTools::CheckData(m_psBuf, m_nBufLen, &data_size, &expect_size);
		if (parse_ret > 0) {
			//在“包起始标志”之前有parse_ret个字节的残留数据
			m_nBufLen -= parse_ret;
			for (i = 0, j = parse_ret; i < m_nBufLen; i++, j++) {
				m_psBuf[i] = m_psBuf[j];
			}
			//通知用户界面：清除了一定数量的无效数据。（以负数约定此类错误）
			if (m_pu) {
				m_pu->OnSocketReceiveError(-parse_ret);
			}
			//去除多余数据后，应再次分析数据是否符合要求
			parse_ret = CStructTools::CheckData(m_psBuf, m_nBufLen, &data_size, &expect_size);
		}
		if (parse_ret == 0) {
			//正常，可以处理数据
			if (m_pu) {
				m_pu->OnSocketReceive((SOCK_PKG*)m_psBuf);
			}
			//删除已经处理过的数据
			m_nBufLen -= data_size;
			for (i = 0, j = data_size; i < m_nBufLen; i++, j++) {
				m_psBuf[i] = m_psBuf[j];
			}
			if (m_nBufLen > 0) {
				continue;  //m_psBuf中还有数据没处理完
			}
			else {
				break;  //完美结束
			}
		}
		if (parse_ret == -1) {
			//表示还没接收完，继续
			break;
		}
		if (parse_ret == -2) {
			//接收出错，后面的没法处理了，清缓冲区。（以负数约定此类错误）
			if (m_pu) {
				m_pu->OnSocketReceiveError(-m_nBufLen);
			}
			m_nBufLen = 0;
		}
	}

fin:
	CAsyncSocket::OnReceive(nErrorCode);
}

