#pragma once

typedef struct _NUMBER_DRAW_PARAM {
	int x;
	int y;
	int type;
	int sub_type;
	BOOL IsValid() { return (x > 0 && y > 0); }
} NUMBER_DRAW_PARAM;

#include "FMDisplayDataBuffer.h"
class CFMRecordUnit;
class CFMMonitorView;
class CFMMonitorUnit
{
public:
	CFMMonitorUnit(CFMMonitorView* pView);
	virtual ~CFMMonitorUnit(void);

	void SetPos(int x, int y, int w, int h, int nAreaType, BOOL bVisible = TRUE);
	BOOL PtInArea(CPoint pt);
	void SetType(int type);

	void OnGetFocus();
	void OnLoseFocus();
	void RenewData(CDC* pdc);
	void RenewBkConfig();

#ifdef DECIMUS_VERSION
	//改进的1/10秒绘制曲线函数
	//add:20150403
	void DrawWaveDecimus(CDC* pdc, int nDecimusCounter);
#endif

private:
	int GetType();
	void ResetNumber();
	void DrawBkgd(CDC* pdc);
	void RedrawNibpBkgd(CDC* pdc);
	void RedrawTempBkgd(CDC* pdc);
	void DrawInfo(CDC* pdc);
	void DrawNumber(CDC* pdc);
	void DrawWave(CDC* pdc);
	void DrawECGWave(CDC* pdc);
	void DrawSPO2Wave(CDC* pdc);
	void DrawRESPWave(CDC* pdc);
	void FillUpFHRWave(CDC* pdc);//补满从开头到当前时刻的FHR曲线
	void DrawFHRWave(CDC* pdc);
	void DrawFHRWave1(CDC* pdc); //1CM/MIN的绘制函数
	void DrawFHRWave3(CDC* pdc); //3CM/MIN的绘制函数
	void DrawFHRTimeRuler(CDC* pdc);
	void DrawTimeMark(CDC* pdc, COLORREF cr, CTime& tm, int x, int y, int limitH);
	void DrawDiagnosisFlag(CDC* pdc, COLORREF cr, int x, int y, int type);
	void DrawFMMark(CDC* pdc, int x);
	void DrawTOCOResetMark(CDC* pdc, int x);
	void DrawTechAlarm(CDC* pdc);
	CString GetTechErrorString();
	void DrawBioAlarm(CDC* pdc);
	CString GetBioAlarmString();

#ifdef DECIMUS_VERSION
	//改进的1/10秒绘制曲线函数
	void DrawECGWaveDecimus(CDC* pdc, int nDecimusCounter);
	void DrawSPO2WaveDecimus(CDC* pdc, int nDecimusCounter);
	void DrawRESPWaveDecimus(CDC* pdc, int nDecimusCounter);
#endif

	//测试用
	void DrawCheckArea(CDC* pdc);

	//更新工具条按钮上的字
	void RenewToolbar();

	int m_nTypeDebug;
	int m_nAliveIndex;
	BOOL m_bRedrawBkgd;
	BOOL m_bRedrawNibpBkgd;
	BOOL m_bRedrawTempBkgd;

	//最后一次的时间
	int m_nLastGetDataTimeSpan;
	void RenewNumb(int nTimeSpan);
	void RenewWave(int nTimeSpan);
	void RenewFHRWave(int nTimeSpan);

public:
	void DrawFocus(CDC* pdc);
	void DrawFrame(CDC* pdc);
	void SetRecordUnit(int nUnitIndex);
	inline void SetAliveIndex(int nIndex) { m_nAliveIndex = nIndex; };
	inline int GetAliveIndex() { return m_nAliveIndex; };

private:
	CFMRecordUnit* m_pUnit;
	CFMMonitorView* m_pView;
	CDC** m_ppBkDC;
	int m_x;
	int m_y;
	int m_w;
	int m_h;
	int m_nAreaType;
	int m_nOldType;
	int m_nOldAreaType;

	//各个监护区域的位置
	CRect m_rcECG;
	CRect m_rcRESP;
	CRect m_rcSPO2;
	CRect m_rcNIBP;
	CRect m_rcTEMP;
	CRect m_rcFHR;

	// 报警状态
	int m_nCurErrorIndex;
	int m_nECGError;
	int m_nRESPError;
	int m_nSPO2Error;
	int m_nNIBPError;
	int m_nTEMPError;
	int m_nFHRError;
	int m_nBioAlarmType;
	int m_nBioAlarmLevel;
	int m_nOldBioAlarmLevel;

	// 胎心率基线
	int m_nFHRBaseline1;
	int m_nFHRBaseline2;

	// 各种数值显示，以及显示位置
	CString StringBedNum();
	NUMBER_DRAW_PARAM m_stBedNum;
	int m_nHRFlag;
	inline CString StringHRFlag() { return itoIconString(m_nHRFlag); };
	NUMBER_DRAW_PARAM m_stHRFlag;
	int m_nHR;
	inline CString StringHR() { return itoNumString3(m_nHR); };
	NUMBER_DRAW_PARAM m_stHR;
	int m_nHRLimitH;
	inline CString StringHRLimitH() { return itoNumString3(m_nHRLimitH); };
	NUMBER_DRAW_PARAM m_stHRLimitH;
	int m_nHRLimitL;
	inline CString StringHRLimitL() { return itoNumString3(m_nHRLimitL); };
	NUMBER_DRAW_PARAM m_stHRLimitL;

	int m_nST1;
	int m_nST1LimitH;
	int m_nST1LimitL;
	int m_nST2;
	int m_nST2LimitH;
	int m_nST2LimitL;
	int m_nPVC;
	int m_nPVCLimitH;

	int m_nECGGainOpt;
	inline CString StringECGGainOpt() { return itoIconString(m_nECGGainOpt); };
	NUMBER_DRAW_PARAM m_stECGGainOpt;
	int m_nECG1LeadOpt;
	inline CString StringECG1LeadOpt() { return itoIconString(m_nECG1LeadOpt); };
	NUMBER_DRAW_PARAM m_stECG1LeadOpt;
	int m_nECG2LeadOpt;
	inline CString StringECG2LeadOpt() { return itoIconString(m_nECG2LeadOpt); };
	NUMBER_DRAW_PARAM m_stECG2LeadOpt;
	int m_nECGFilter;
	inline CString StringECGFilter() { return CFMDict::ECGFilter_itos(m_nECGFilter); };
	NUMBER_DRAW_PARAM m_stECGFilter;
	int m_nECGSpeed;
	inline CString StringECGSpeed() { return CFMDict::ECGSpeed_itos(m_nECGSpeed); };
	NUMBER_DRAW_PARAM m_stECGSpeed;

	int m_nNIBP_S;
	inline CString StringNIBP_S() { return itoNibpString(m_nNIBPUnit, m_nNIBP_S); };
	NUMBER_DRAW_PARAM m_stNIBP_S;
	int m_nNIBP_M;
	inline CString StringNIBP_M() { return itoNibpString(m_nNIBPUnit, m_nNIBP_M); };
	NUMBER_DRAW_PARAM m_stNIBP_M;
	int m_nNIBP_D;
	inline CString StringNIBP_D() { return itoNibpString(m_nNIBPUnit, m_nNIBP_D); };
	NUMBER_DRAW_PARAM m_stNIBP_D;
	int m_nNIBPLimitH;
	inline CString StringNIBPLimitH() { return itoNibpString(m_nNIBPUnit, m_nNIBPLimitH); };
	NUMBER_DRAW_PARAM m_stNIBPLimitH;
	int m_nNIBPLimitL;

	int m_nNIBP_M_LimitH;
	int m_nNIBP_M_LimitL;
	int m_nNIBP_D_LimitH;
	int m_nNIBP_D_LimitL;

	inline CString StringNIBPLimitL() { return itoNibpString(m_nNIBPUnit, m_nNIBPLimitL); };
	NUMBER_DRAW_PARAM m_stNIBPLimitL;
	int m_nNIBPUnit;
	inline CString StringNIBPUnit() { return itoIconString(m_nNIBPUnit); };
	NUMBER_DRAW_PARAM m_stNIBPUnit;
	int m_nNIBPPumpPressure;
	inline CString StringNIBPPumpPressure() { return itoNibpString(m_nNIBPUnit, m_nNIBPPumpPressure); };
	NUMBER_DRAW_PARAM m_stNIBPPumpPressure;
	int m_nNIBPPumpMode;
	inline CString StringNIBPPumpMode() { return CFMDict::NIBPMode_itos(m_nNIBPPumpMode); };
	NUMBER_DRAW_PARAM m_stNIBPPumpMode;

	int m_nT1;
	inline CString StringT1() { return itoTempString(m_nTEMPUnit, m_nT1); };
	NUMBER_DRAW_PARAM m_stT1;
	int m_nT1LimitH;
	inline CString StringT1LimitH() { return itoTempString(m_nTEMPUnit, m_nT1LimitH); };
	NUMBER_DRAW_PARAM m_stT1LimitH;
	int m_nT1LimitL;
	inline CString StringT1LimitL() { return itoTempString(m_nTEMPUnit, m_nT1LimitL); };
	NUMBER_DRAW_PARAM m_stT1LimitL;
	int m_nT2;
	inline CString StringT2() { return itoTempString(m_nTEMPUnit, m_nT2); };
	NUMBER_DRAW_PARAM m_stT2;
	int m_nT2LimitH;
	inline CString StringT2LimitH() { return itoTempString(m_nTEMPUnit, m_nT2LimitH); };
	NUMBER_DRAW_PARAM m_stT2LimitH;
	int m_nT2LimitL;
	inline CString StringT2LimitL() { return itoTempString(m_nTEMPUnit, m_nT2LimitL); };
	NUMBER_DRAW_PARAM m_stT2LimitL;
	int m_nTEMPUnit;
	inline CString StringTEMPUnit() { return itoIconString(m_nTEMPUnit); };
	NUMBER_DRAW_PARAM m_stTEMPUnit;

	int m_nSPO2;
	inline CString StringSPO2() { return itoNumString3(m_nSPO2); };
	NUMBER_DRAW_PARAM m_stSPO2;
	int m_nSPO2LimitH;
	inline CString StringSPO2LimitH() { return itoNumString3(m_nSPO2LimitH); };
	NUMBER_DRAW_PARAM m_stSPO2LimitH;
	int m_nSPO2LimitL;
	inline CString StringSPO2LimitL() { return itoNumString3(m_nSPO2LimitL); };
	NUMBER_DRAW_PARAM m_stSPO2LimitL;

	int m_nPR;
	inline CString StringPR() { return itoNumString3(m_nPR); };
	int m_nPRLimitH;
	inline CString StringPRLimitH() { return itoNumString3(m_nPRLimitH); };
	int m_nPRLimitL;
	inline CString StringPRLimitL() { return itoNumString3(m_nPRLimitL); };

	int m_nRR;
	inline CString StringRR() { return itoNumString3(m_nRR); };
	NUMBER_DRAW_PARAM m_stRR;
	int m_nRRLimitH;
	inline CString StringRRLimitH() { return itoNumString3(m_nRRLimitH); };
	NUMBER_DRAW_PARAM m_stRRLimitH;
	int m_nRRLimitL;
	inline CString StringRRLimitL() { return itoNumString3(m_nRRLimitL); };
	NUMBER_DRAW_PARAM m_stRRLimitL;

	int m_nFHR1;
	inline CString StringFHR1() { return itoNumString3(m_nFHR1); };
	NUMBER_DRAW_PARAM m_stFHR1;
	int m_nFHR2;
	inline CString StringFHR2() { return itoNumString3(m_nFHR2); };
	NUMBER_DRAW_PARAM m_stFHR2;
	int m_nFHRLimitH;
	inline CString StringFHRLimitH() { return itoNumString3(m_nFHRLimitH); };
	NUMBER_DRAW_PARAM m_stFHRLimitH;
	int m_nFHRLimitL;
	inline CString StringFHRLimitL() { return itoNumString3(m_nFHRLimitL); };
	NUMBER_DRAW_PARAM m_stFHRLimitL;

	int m_nTOCO;
	inline CString StringTOCO() { return itoNumString3(m_nTOCO); };
	NUMBER_DRAW_PARAM m_stTOCO;
	int m_nTOCOLimitH;
	inline CString StringTOCOLimitH() { return itoNumString3(m_nTOCOLimitH); };
	NUMBER_DRAW_PARAM m_stTOCOLimitH;
	int m_nTOCOLimitL;
	inline CString StringTOCOLimitL() { return itoNumString3(m_nTOCOLimitL); };
	NUMBER_DRAW_PARAM m_stTOCOLimitL;
	int m_nFM;
	inline CString StringFM() { return itoNumString2(m_nFM); };
	NUMBER_DRAW_PARAM m_stFM;

	// 波形绘制位置，以及当前屏幕上的波形数据
	int m_nWaveECG_X;
	int m_nWaveECG1_Y;
	int m_nWaveECG2_Y;
	int m_nWaveECG_W;
	int m_nWaveECG_H;
	COLORREF m_crWaveECG1;
	COLORREF m_crWaveECG2;
	BYTE* m_pWaveECG1;
	BYTE* m_pWaveECG2;
	BYTE* m_pWaveECGv;
	int m_nWaveECG_StartPos;

	int m_nWaveSPO2_X;
	int m_nWaveSPO2_Y;
	int m_nWaveSPO2_W;
	int m_nWaveSPO2_H;
	COLORREF m_crWaveSPO2;
	BYTE* m_pWaveSPO2;
	int m_nWaveSPO2_StartPos;

	int m_nWaveRESP_X;
	int m_nWaveRESP_Y;
	int m_nWaveRESP_W;
	int m_nWaveRESP_H;
	COLORREF m_crWaveRESP;
	BYTE* m_pWaveRESP;
	int m_nWaveRESP_StartPos;

	int m_nWaveFHR_X;
	int m_nWaveFHR_Y;
	int m_nWaveFHR_W;
	int m_nWaveFHR_H;
	COLORREF m_crWaveFHR1;
	COLORREF m_crWaveFHR2;
	COLORREF m_crFlagFHR1;
	COLORREF m_crFlagFHR2;
	COLORREF m_crTimeMark;
	COLORREF m_crTimeMarkDark;
	WORD* m_pWaveFHR1;
	WORD* m_pWaveFHR2;
	int m_nWaveFHR_StartPos;
	int m_nWaveFHR_Speed1Counter;//纸速为1cm/min时使用的计数器

#ifdef DECIMUS_VERSION
	BOOL m_bDrawWaveDecimus;
	int m_nWaveECG_DecimusDataPos;
	int m_nWaveSPO2_DecimusDataPos;
	int m_nWaveRESP_DecimusDataPos;
#endif

	// 注意，不存在m_nWave_TOCO_X，因为它必须与m_nWave_FHR_X相等
	// 注意，不存在m_nWave_TOCO_W，因为它必须与m_nWave_FHR_W相等
	int m_nWaveTOCO_Y;
	int m_nWaveTOCO_H;
	COLORREF m_crWaveTOCO;
	COLORREF m_crWaveMARK;
	WORD* m_pWaveTOCO;
	BYTE* m_pWaveFMMARK;
	BYTE* m_pWaveTOCOReset;
	DISPLAY_FLAG* m_pDiagno1;
	DISPLAY_FLAG* m_pDiagno2;

	int m_nFHRPaperSpeedOpt;

	int m_nInfoBarX;
	int m_nInfoBarY;
	int m_nInfoBarW;
	int m_nInfoBarH;
	COLORREF m_crInfoText;

	//报警区域的位置
	int m_nTechAlarmX;
	int m_nTechAlarmY;
	int m_nTechAlarmW;
	int m_nTechAlarmH;
	int m_nBioAlarmX;
	int m_nBioAlarmY;
	int m_nBioAlarmW;
	int m_nBioAlarmH;

private:
	// 设置资源
	void SetNumberResForAdult(int nIndex);
	void SetNumberResForFetus(int nIndex);
	// 曲线重新从头绘制
	void ResetWavePos();

public:
	BOOL m_bVisible;

public:
	void OnSwitchType();
	void OnMedicalRecord();

	BOOL OnDbClick(CPoint& pt);
};

