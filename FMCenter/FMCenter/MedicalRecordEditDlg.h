#pragma once
#include "afxwin.h"
#include "afxdtctl.h"


// CMedicalRecordEditDlg 对话框
class CMedicalRecordListDlg;
class CMedicalRecordEditDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMedicalRecordEditDlg)

public:
	CMedicalRecordEditDlg(CMedicalRecordListDlg* pParent, int nType, CString sMedNum);
	virtual ~CMedicalRecordEditDlg();

// 对话框数据
	enum { IDD = IDD_MEDICAL_RECORD_EDIT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

private:
	CMedicalRecordListDlg* m_pListDlg;
	int m_nType;
	CDatabase& m_db;
	CCriticalSection& m_dbLock;
	CString m_sMedNum;
	CString m_sMaxMedNum; //最大的病历号

	void GetMaxMedNum(CString& s);
	void IncreaseMedNum(CString& s);
	void QueryRecord();

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedSave();
	afx_msg void OnBnClickedCancel();
	CEdit m_edtName;
	CDateTimeCtrl m_dateBirthday;
	CEdit m_edtHeight;
	CEdit m_edtWeight;
	CEdit m_edtAddress;
	CComboBox m_cmbGender;
	CComboBox m_cmbType;
	CComboBox m_cmbBloodType;
	CEdit m_edtRecordNum;
	CEdit m_edtDoctor;
	CEdit m_edtClinic;
	CEdit m_edtPhoneNumber;
	CDateTimeCtrl m_datAdmissionDate;
};
