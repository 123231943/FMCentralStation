
// FMCenter.h : FMCenter 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"       // 主符号

// CFMCenterApp:
// 有关此类的实现，请参阅 FMCenter.cpp
//
class CLogFile;
class CFMRecordUnit;
class CFMSocketThread;
class CFMRecordThread;
class CFMMonitorView;
class CFMToolbarForm;
class CPrintFrame;
class CFMCenterApp : public CWinApp
{
public:
	CString m_strDBName;
	CDatabase m_db;
	CCriticalSection m_csDBLock;
	CLogFile* m_pLogFile;
	CCriticalSection m_csLogLock;

	CString m_sAdminName;
	int m_nAdminType;
	int m_nAdminID;

private:
	CUIntArray m_arAliveBedIndex;
	CCriticalSection m_csAliveList;
	CPrintFrame* m_pPrintFrame;

public:
	int GetFocusIndex();
	void SetFocusIndex(int nCurIndex);
	void AddAliveIndex(int nBedIndex);
	void DelAliveIndex(int nBedIndex);
	int GetAliveIndexList(CUIntArray& ar);
	CFMRecordUnit* FindRecordUnitByMRID(CString sMRID);
	void PostSocketMessage();
	void PostLayoutMessage();
	void PostInitBkMessage();
	void Close();
	BOOL SaveSystemSetup();
	int GetRecordFileList(CStringArray& sa, CString mrid,
		CUIntArray* paOffset=NULL, CUIntArray* paSize=NULL);

private:
	BOOL CreateDB();
	BOOL OpenConfig();
	BOOL InitSystemSetup();

public:
	BOOL m_bOK;
	CFMMonitorView* m_pMonitorView;
	CFMToolbarForm* m_pToolbarForm;
	CFMSocketThread* m_pSocketThread;
	CFMRecordThread* m_pRecordThread;

private:
	HANDLE m_hMutex;
	CString m_sDataRootPath;
	int m_nListenPort;

	// 调试开关
	BOOL m_bAutoDiagno;
	BOOL m_bDebugUI;
	BOOL m_bDebugArea;
	BOOL m_bDebugSocket;
	BOOL m_bDebugStorage;
	BOOL m_bDebugNoLogin;
	BOOL m_bDebugSocketDetail;

public:
	// 系统设置
	CString m_sHospitalName; //医院名称
	int m_nFHRSpeedOption;   //胎儿监护的走纸速度
	int m_nNIBPUnitOption;   //血压显示单位选项
	int m_nTEMPUnitOption;   //体温显示单位选项
	int m_nFHRReportType;    //默认的胎儿监护报告类型
	BOOL m_bTendPrintHR;
	BOOL m_bTendPrintSYS;
	BOOL m_bTendPrintMEA;
	BOOL m_bTendPrintDIA;
	BOOL m_bTendPrintRR;
	BOOL m_bTendPrintSPO2;
	BOOL m_bTendPrintT1;
	BOOL m_bTendPrintT2;

public:
	CFMCenterApp();
	void WriteLog(CString s);
	int GetNoOfProcessors();
	CFMRecordUnit* GetRecordUnit(int nIndex);
	inline CString GetDataRoot() { return m_sDataRootPath; };
	inline int GetListenPort() { return m_nListenPort; };
	inline BOOL HasAutoDiagno() { return m_bAutoDiagno; };
	inline BOOL DebugUI() { return m_bDebugUI; };
	inline BOOL DebugArea() { return m_bDebugArea; };
	inline BOOL DebugSocket() { return m_bDebugSocket; };
	inline BOOL DebugStorage() { return m_bDebugStorage; };
	inline BOOL DebugNoLogin() { return m_bDebugNoLogin; };
	inline BOOL DebugSocketDetail() { return m_bDebugSocketDetail; };
	inline void SetPrintMainWnd(CPrintFrame* pf) { m_pPrintFrame = pf; };
	inline CPrintFrame* GetPrintMainWnd() { return m_pPrintFrame; };
	int GetDoctorDiagno(CString mrid);
	void SaveDoctorDiagno(CString mrid, int n);

// 重写
public:
	virtual BOOL InitInstance();
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();
	virtual BOOL OnIdle(LONG lCount);
};

extern CFMCenterApp theApp;
