#pragma once
#include "afxwin.h"



// CFMToolbarForm 窗体视图

class CFMToolbarForm : public CFormView
{
	DECLARE_DYNCREATE(CFMToolbarForm)

private:
	CUIntArray m_test_arAliveBedIndex;
	CUIntArray m_test_arSleepBedIndex;
	void InitTestBedIndex();
public:
	int GetAliveIndexList(CUIntArray& ar) { ar.Copy(m_test_arAliveBedIndex); return ar.GetCount(); };
	int GetSleepIndexList(CUIntArray& ar) { ar.Copy(m_test_arSleepBedIndex); return ar.GetCount(); };

private:
	void RenewDatetime();
	void PushDownPageBtnState(int n);

protected:
	CFMToolbarForm();           // 动态创建所使用的受保护的构造函数
	virtual ~CFMToolbarForm();

public:
	enum { IDD = IDD_BAR_FORM };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

private:
	int m_nAliveCount;
	int m_nFocusIndex;

public:
	void SetBedButton(void);
	void SetComboSel(int nSel);
	inline int GetFocusIndex() { return m_nFocusIndex; };
	BOOL QueryQuit();
	void CloseTimer();

	DECLARE_MESSAGE_MAP()
public:
	CButton m_btnPage1;
	CButton m_btnPage2;
	CButton m_btnPage3;
	CButton m_btnPage4;
	CButton m_btnPage5;
	CButton m_btnPage6;
	CButton m_btnPage7;
	CButton m_btnPage8;
	CComboBox m_cmbSelectBed;
	CButton m_btnAdvancedSetup;
	CEdit m_edtDatetime;
	virtual void OnInitialUpdate();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedPage1();
	afx_msg void OnBnClickedPage2();
	afx_msg void OnBnClickedPage3();
	afx_msg void OnBnClickedPage4();
	afx_msg void OnBnClickedPage5();
	afx_msg void OnBnClickedPage6();
	afx_msg void OnBnClickedPage7();
	afx_msg void OnBnClickedPage8();
	afx_msg void OnCbnSelchangeCmbSelectBed();
	afx_msg void OnBnClickedQuit();
	afx_msg void OnBnClickedMedicalRecord();
	afx_msg void OnBnClickedMonitorRecord();
	afx_msg void OnBnClickedAdvancedSetup();
	afx_msg void OnBnClickedBtnSimAdd();
	afx_msg void OnBnClickedBtnSimDel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CButton m_btnSimAdd;
	CButton m_btnSimDel;
	afx_msg void OnBnClickedBtnSysMetrics();
	CButton m_btnSysMetrics;
	CEdit m_edtAlarmInfo;
};


