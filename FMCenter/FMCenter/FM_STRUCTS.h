// FM_STRUCTS.h

#ifndef __FM_STRUCTS_H__
#define __FM_STRUCTS_H__

//定义Socket接收缓冲区大小
//（注意：SOCKET_BUFFER_SIZE一定要确保能够装下最大的数据包）
#define SOCKET_BUFFER_SIZE  (2000)

//定义每秒钟数据量
#define ECG_WAVE_DATA_PER_SECOND       (250)
#define RESP_WAVE_DATA_PER_SECOND      (125)
#define SPO2_WAVE_DATA_PER_SECOND      (125)
//

//重新定义数据类型
#undef BYTE
#undef WORD
#undef DWORD

#define BYTE    unsigned char
#define WORD    unsigned short
#define DWORD   unsigned int

#define INVALID_VALUE            ((WORD)-100)
//

//4．网络上包类型
#define PKG_QUIT                 (0xFF)
#define PKG_NEXT                 (0x31)     //服务器发出的心跳包，每5秒钟回一个
#define PKG_BUSY                 (0x32)
#define PKG_PATIENTINFO          (0x02)
#define PKG_CLIENT_DATA          (0x0A)
#define PKG_ECG_COMMAND          (0x50)     //更改心电设置命令
#define PKG_NIBPSTART_COMMAND    (0x51)     //血压启动/停止打气命令
#define PKG_NIBP_COMMAND         (0x52)     //更改NIBP设置命令
#define PKG_RESP_COMMAND         (0x53)     //更改呼吸设置命令
#define PKG_SPO2_COMMAND         (0x54)     //更改血氧设置命令
#define PKG_TEMP_COMMAND         (0x55)     //更改体温设置命令
#define PKG_FHR_COMMAND          (0x56)     //更改胎儿监护设置命令
//

//定义各种数值的最大值和最小值
#define HR_MAX               (300)
#define HR_MIN               (15)
#define ST_MAX               (200)
#define ST_MIN               (-200)
#define RR_MAX               (120)
#define RR_MIN               (6)
#define SPO2_MAX             (100)
#define SPO2_MIN             (0)
#define PR_MAX               (254)
#define PR_MIN               (0)
#define NIBP_MAX             (270)
#define NIBP_MIN             (10)
#define TEMP_MAX             (500)
#define TEMP_MIN             (00)
#define FHR_MAX              (240)
#define FHR_MIN              (30)
#define TOCO_MAX             (100)
#define TOCO_MIN             (0)

#define CHECK_HR(n)          ((n>HR_MAX  || n<HR_MIN)   ? INVALID_VALUE : n)
#define CHECK_ST(n)          ((n>ST_MAX  || n<ST_MIN)   ? INVALID_VALUE : n)
#define CHECK_RR(n)          ((n>RR_MAX  || n<RR_MIN)   ? INVALID_VALUE : n)
#define CHECK_SPO2(n)        ((n>SPO2_MAX|| n<SPO2_MIN) ? INVALID_VALUE : n)
#define CHECK_PR(n)          ((n>PR_MAX  || n<PR_MIN)   ? INVALID_VALUE : n)
#define CHECK_NIBP(n)        ((n>NIBP_MAX|| n<NIBP_MIN) ? INVALID_VALUE : n)
#define CHECK_TEMP(n)        ((n>TEMP_MAX|| n<TEMP_MIN) ? INVALID_VALUE : n)
#define CHECK_FHR(n)         ((n>FHR_MAX || n<FHR_MIN)  ? INVALID_VALUE : n)
#define CHECK_TOCO(n)        ((n>TOCO_MAX|| n<TOCO_MIN) ? INVALID_VALUE : n)
//

#pragma pack(1)

//包头
typedef struct _HEAD {
	BYTE flag[4];     //固定的包起始标志，0xA555AA5A（确保在数据中不会出现这样的组合）
	WORD body_size;   //包体长度，从第9个字节开始计算
	BYTE bed_num;     //床号
	BYTE pkg_type;    //包类型
} HEAD;

//病人信息PATIENTINFO包
typedef struct _PATIENTINFO {
	BYTE name[24];    //病人信息（ASCII码）
	BYTE medno[24];   //病例号
	BYTE doctor[24];  //医生信息
	BYTE age[6];      //年龄
	BYTE height[6];   //身高
	BYTE weight[6];   //体重
	BYTE gender;      //0->男，1->女
	BYTE bloodtype;   //0->不祥，1->AB，2->A，3->B，4->O
	BYTE patienttype; //0->成人，1->小儿，2->新生儿
	BYTE ommit;       //全部默认为00
	BYTE comment[24]; //全部默认为00
} PATIENTINFO;

//ECG数据包
typedef struct _ECG {
	BYTE ecg_lead1;   //通道1，0:I, 1:II, 2:III, 3:AVR, 4:AVL, 5:AVF, 6:V
	BYTE ecg_lead2;   //通道2，0:I, 1:II, 2:III, 3:AVR, 4:AVL, 5:AVF, 6:V
	BYTE ecg_filter;  //滤波方式，0:监护, 1:手术, 2:诊断
	BYTE ecg_speed;   //扫描速度0:25mm/s， 1:50mm/s, 2:12.5mm/s
	BYTE ecg_gain;    //增益，0:1倍, 1:2倍, 2:0.25倍, 3:0.5倍
	BYTE ecg_stop;      // 工频抑制0:关 1：开
	BYTE ecg_paceangy;  // 起博分析0:关 1：开
	BYTE ecg_stangy;    // ST段分析0:关 1：开
	BYTE ecg_rhythm;    // 心律分析0:关 1：开
	WORD hr;          //心率值，无效值为-100
	WORD hr_high;     //心率上限
	WORD hr_low;      //心率下限
	BYTE hr_level;    //心率报警级别，1:高级报警, 2:中级报警, 3:低级报警
	WORD st1;         //st1值，无效值为-100, 扩大了100倍，应显示小数2位
	WORD st1_high;
	WORD st1_low;
	WORD st2;         //st2值
	WORD st2_high;
	WORD st2_low;
	BYTE st_level;    //3:高级报警, 2:中级报警, 1:低级报警
	WORD pvc;         //pvc值，即：每分钟室性早搏次数
	WORD pvc_high;
	BYTE pvc_level;   //3:高级报警, 2:中级报警, 1:低级报警
	BYTE error;       //错误代码
	BYTE hr_source;   //心律来源：0:正常；1:来自SPO2的PR
	BYTE arr_status;  //心率失常状态码
	BYTE ecg1data[ECG_WAVE_DATA_PER_SECOND];//1通道心电波形
	BYTE ecg2data[ECG_WAVE_DATA_PER_SECOND];//2通道心电波形
	BYTE ecg3data[ECG_WAVE_DATA_PER_SECOND];//3通道心电波形，一般表示V导连数据
} ECG;
// ECG波形数据的中间值（基线）值
#define ECG_MID_VALUE            (128)
// error
typedef enum {
	net_ECG_LEAD_LL_OFF = 1,    // LL导联脱落
	net_ECG_LEAD_RL_OFF,        // RL导联脱落
	net_ECG_LEAD_LA_OFF,        // LA导联脱落
	net_ECG_LEAD_RA_OFF,        // RA导联脱落
	net_ECG_LEAD_V_OFF,		    // V 导联脱落
	net_ECG_LEAD_MAX
} net_ECG;
// status
typedef enum {
	net_ECG_HARTSOUND = 0x01,   // 心跳
	net_ECG_PACEPLUS  = 0x02    // 起博脉冲
} net_ECG_SOUND;
// arr_code
typedef  enum  {
	net_ARR_NUL = 0,            // NULL
	net_ARR_ASY = 1,            // asystole
	net_ARR_VTA = 2,            // ventricular tachycardia
	net_ARR_ROT = 3,            // VPB R on T
	net_ARR_RUN = 4,            // VPB runs of 3 or 4
	net_ARR_CPT = 5,            // VPB couple
	net_ARR_VPB = 6,            // accidental VPB
	net_ARR_BGM = 7,            // VPB bigeminy
	net_ARR_TGM = 8,            // VPB trigeminy
	net_ARR_TAC = 9,            // supraventricular tachycaridia
	net_ARR_BRD = 10,           // supraventricular bradycaridia
	net_ARR_PNC = 11,           // pace not capture
	net_ARR_PNP = 12,           // pacer not paced
	net_ARR_MIS = 13,           // missed beat
	net_ARR_LRN = 14,           // Arrhythmia analysis learning
	net_ARR_NML = 15,           // normal qrs
	net_ARR_NOS = 16,           // noise beat
	net_ARR_WEAK= 17,           // signal weak
	net_ARR_CODE_MAX
} net_ARR;

//呼吸数据包
typedef struct _RESP {
	BYTE rr_speed;              // 扫描速度0:25mm/s， 1:50mm/s, 2:12.5mm/s
	BYTE rr_gain;               // 增益 0:2倍，1:1倍，2:0.5倍，3:0.25倍
	WORD rr;                    // 呼吸率值,无效值为-100
	WORD rr_high;               // 监护仪的报警上限
	WORD rr_low;                // 监护仪的报警下限
	BYTE rr_level;              // 3:高级报警, 2:中级报警, 1:低级报警
	BYTE error;                 // 错误代码(当erro=0时，值才有意义)

	BYTE respdata[RESP_WAVE_DATA_PER_SECOND];         // 1通道呼吸波形。
} RESP;
// error
typedef enum {
	net_RESP_APNEA = 1          // 窒息报警
} net_RESP_SOUND;

//血氧数据包
typedef struct _SPO2 {
	BYTE spo2_speed;            // 扫描速度0:25mm/s， 1:50mm/s, 2:12.5mm/s
	BYTE spo2_gain;             // 增益 0:1倍，1:2倍，2:0.25倍，3:0.5倍	

	WORD spo2; 				    // 血氧值,无效值为-100
	WORD spo2_high ; 		    // 监护仪的报警上限
	WORD spo2_low ; 		    // 监护仪的报警下限
	BYTE spo2_level;		    // 3:高级报警, 2:中级报警, 1:低级报警

	WORD pr; 				    // 脉率值,无效值为-100
	WORD pr_high; 			    // 监护仪的报警上限
	WORD pr_low ; 			    // 监护仪的报警下限
	BYTE pr_level;		 	    // 3:高级报警, 2:中级报警, 1:低级报警

	BYTE error;				    // 错误代码(当erro=0时，值没有意义)
	BYTE status;			    // 状态

	BYTE wavedata[SPO2_WAVE_DATA_PER_SECOND];// 波形数据
} SPO2;
// error
typedef enum {
	net_SPO2_FINGER_OFF = 1,   // 手指脱落
	net_SPO2_SEARCHPULSE,      // 搜索脉博
	net_SPO2_LEAD_OFF,         // 探头脱落
	net_SPO2_SEARCHTOOLONG,    // 搜索脉博太长
} net_SPO2_ERR;
// status
typedef enum {
	net_SPO2_PULSEBEEP = 1     // 脉博间提示
} net_SPO2_SOUND;

//血压数据包
typedef struct _NIBP {
	//NIBP涉及单位，发送的数值均为mmgh的值，kpa需要中央站计算Kpa显示一个小数点
	WORD sys;               // 高压值,无效值为-100
	WORD sys_high;          // 监护仪的报警上限
	WORD sys_low;           // 监护仪的报警下限

	WORD mea;               // 平均压值,无效值为-100
	WORD mea_high;          // 监护仪的报警上限
	WORD mea_low;           // 监护仪的报警下限

	WORD dia;               // 低压值,无效值为-100
	WORD dia_high;          // 监护仪的报警上限
	WORD dia_low;           // 监护仪的报警下限

	WORD press;             // 袖带实时压力值
	BYTE measure;           // 正在测试标志(TRUE：正在测量)
	BYTE nibp_level;        // 3:高级报警, 2:中级报警, 1:低级报警
	BYTE unit;              // 0 – mmgh; 1 - kpa
	BYTE error;             // 错误代码(对测量结果的状态)
	BYTE mode;              // 测量模式
} NIBP;
// error
typedef enum {
	net_NIBP_ERR_LOOSECUFF = 1,   // 袖带过松
	net_NIBP_ERR_AIRLEAK,         // 漏气
	net_NIBP_ERR_PRESSUREERROR,   // 气压错误
	net_NIBP_ERR_WEAKSIGNAL,      // 弱信号
	net_NIBP_ERR_RANGEEXCEEDED,   // 超范围
	net_NIBP_ERR_EXCESSIVEMOTION, // 过分运行
	net_NIBP_ERR_OVERPRESSURE,    // 过压
	net_NIBP_ERR_SIGNALSATURATED, // 信号饱和
	net_NIBP_ERR_PNEUMATICLEAK,   // 漏气检测失败
	net_NIBP_ERR_SYSFAIL,         // 系统错误
	net_NIBP_ERR_TIMEOUT,         // 超时
	net_NIBP_ERR_SELFTESTFAIL,    // 自检失败
	net_NIBP_ERR_FAIL             // 测量失败
} net_NIBP_ERR;
// mode
typedef enum {
	net_NIBP_MANUAL,              // 手动
	net_NIBP_AUTO_1MIN,           // 1分钟自动
	net_NIBP_AUTO_2MINS,
	net_NIBP_AUTO_3MINS,
	net_NIBP_AUTO_4MINS,
	net_NIBP_AUTO_5MINS,
	net_NIBP_AUTO_10MINS,
	net_NIBP_AUTO_15MINS,
	net_NIBP_AUTO_30MINS,
	net_NIBP_AUTO_60MINS,
	net_NIBP_AUTO_90MINS,
	net_NIBP_AUTO_120MINS,
	net_NIBP_AUTO_180MINS,
	net_NIBP_AUTO_240MINS,
	net_NIBP_AUTO_480MINS
} net_NIBP;

//体温数据包
typedef struct _TEMP {
	//体温数据因为设计一个小数点，所以上发数据都是扩大了10倍的。
	WORD t1;            // 体温1值,无效值为-100
	WORD t1_high;       // 监护仪的报警上限
	WORD t1_low;        // 监护仪的报警下限
	WORD t2;            // 体温2值,无效值为-100
	WORD t2_high;       // 监护仪的报警上限
	WORD t2_low;        // 监护仪的报警下限
	BYTE t_level;       // 3:高级报警, 2:中级报警, 1:低级报警
	BYTE unit;          // 单位℃，℉
	BYTE error;         // 错误代码(当erro=0时，值没有意义) 
} TEMP;
// error
typedef enum {
	net_TEMP_LEAD_CH1_OFF = 1,  // 通道1脱落
	net_TEMP_LEAD_CH2_OFF,      // 通道2脱落
	net_TEMP_LEAD_CHALL_OFF     // 通道1,2脱落
} net_TEMP;

//胎监数据包
typedef struct _FETUS{
	BYTE fetusmode;     // 分辨率
	BYTE fetusvolume;   // 胎心音量
	BYTE marked;        // 胎动标记， 1–有胎动标记；0–无胎动标记
	BYTE ucreset;       // 宫缩复位标记
	WORD fhr1[4];       // 胎心率值
	WORD fhr2[4];
	WORD fhr_high;
	WORD fhr_low;
	WORD fhr_level;
	WORD uc[4];         // 宫缩压值
	WORD uc_high;
	WORD uc_low;
	WORD uc_level;
	WORD fm;            // 胎动个数
	BYTE error;         // 错误代码(当erro=0时，值没有意义)
	WORD mark_info;     // 医生标记信息
} FETUS;
// error
typedef enum {
	net_FHR1_LEADOFF = 1,	// FHR1探头脱落
	net_FHR2_LEADOFF = 2,	// FHR2探头脱落
} net_FHR_ALARM;
// mark_info
typedef enum {
	DOCTOR_MARK_INFO_MOVEOVER,            //翻身
	DOCTOR_MARK_INFO_LEFT_LATERAL,        //左侧卧位
	DOCTOR_MARK_INFO_RIGHT_LATERAL,       //右侧卧位
	DOCTOR_MARK_INFO_PARAMERE_LATERA,     //半侧卧位
	DOCTOR_MARK_INFO_OXYTOCIN,            //滴注催产素
	DOCTOR_MARK_INFO_OXYTOCIN_STOP,       //停滴催产素
	DOCTOR_MARK_INFO_SENSOR_ADJUST,       //调整腹带及探头
	DOCTOR_MARK_INFO_VAGINA_CHECK,        //阴道检查
	DOCTOR_MARK_INFO_PAUSE,               //暂停
	DOCTOR_MARK_INFO_WC,                  //去卫生间
	DOCTOR_MARK_INFO_GLUCOSE,             //滴注葡萄糖
	DOCTOR_MARK_INFO_WET,                 //滴注液体
	DOCTOR_MARK_INFO_OXYGENUPTAKE,        //吸氧
	DOCTOR_MARK_INFO_OXYGENUPTAKE_STOP,   //停止吸氧
	DOCTOR_MARK_INFO_FOOD_INTAKE,         //进食
	DOCTOR_MARK_INFO_MAX
}DOCTOR_MARK_INFO;

//参数状态情况
typedef struct __ECGCTRLINFO{
	BYTE ecg_lead1;     // 通道 1，0:I, 1:II, 2:III, 3:AVR, 4:AVL, 5:AVF, 6:V
	BYTE ecg_lead2;     // 通道 2，0:I, 1:II, 2:III, 3:AVR, 4:AVL, 5:AVF, 6:V
	BYTE ecg_filter;    // 滤波方式 0:监护， 1:手术，2:诊断
	BYTE ecg_gain;      // 增益 0:1倍，1:2倍，2:0.25倍，3:0.5倍	
	BYTE ecg_stop;      // 工频抑制
	BYTE ecg_paceangy;  // 起博分析
	BYTE ecg_stangy;    // ST段分析
	BYTE ecg_rhythm;    // 心律分析
	WORD hr_high;       // 监护仪的报警上限
	WORD hr_low;        // 监护仪的报警下限
	WORD st1_high;      // 监护仪的报警上限
	WORD st1_low;       // 监护仪的报警下限
	WORD st2_high;      // 监护仪的报警上限
	WORD st2_low;       // 监护仪的报警下限
	WORD pvc_high;      // 监护仪的报警上限
	WORD pvc_low;       // 监护仪的报警下限
} ECGCTRLINFO;

// 启动/停止血压测量，类型0x51
typedef struct __NIBPCTRLINFO{
	BYTE status;       // TRUE：启动，FALSE：停止
} NIBPCTRLINFO;

// 血压数据包，类型0x52
typedef struct __NIBPALARMINFO {
	WORD sys_high;     // 监护仪的报警上限
	WORD sys_low;      // 监护仪的报警下限
	WORD mea_high;     // 监护仪的报警上限
	WORD mea_low;      // 监护仪的报警下限
	WORD dia_high;     // 监护仪的报警上限
	WORD dia_low;      // 监护仪的报警下限
	BYTE bp_mode;      // 血压测量模式及周期模式
}NIBPALARMINFO;

// RESP数据包，类型0x53
typedef struct __RESPALARMINFO{
	WORD rr_high;      // 监护仪的报警上限
	WORD rr_low;       // 监护仪的报警下限
} RESPALARMINFO;

// 体温报警上下限参数数据包，类型0x54
typedef struct __TEMPALARMINFO{
	WORD t1_high;      // 监护仪的报警上限
	WORD t1_low;       // 监护仪的报警下限
	WORD t2_high;      // 监护仪的报警上限
	WORD t2_low;       // 监护仪的报警下限
}TEMPALARMINFO;

// 血氧报警上下限参数数据包，类型0x55
typedef struct __SPO2ALARMINFO{
	WORD spo2_high;    // 监护仪的报警上限
	WORD spo2_low;     // 监护仪的报警下限
	WORD pr_high;      // 监护仪的报警上限
	WORD pr_low;       // 监护仪的报警下限
} SPO2ALARMINFO;

// 胎心率报警上下限参数数据包，类型0x56
typedef struct __FHRALARMINFO{
	WORD fhr_high;     // 胎心率报警上限
	WORD fhr_low;      // 胎心率报警下限
} FHRALARMINFO;


//数据选择，指示哪些数据有效
typedef enum _DATA_OPTION{
	OPT_ECG      = 0x00000001,
	OPT_RESP     = 0x00000002,
	OPT_SPO2     = 0x00000004,
	OPT_NIBP     = 0x00000008,
	OPT_TEMP     = 0x00000010,
	OPT_FETUS    = 0x00000020
} DATA_OPTION;

//数据包最大的情况
#define DATA_BUF_MAX   (sizeof(ECG)+sizeof(RESP)+sizeof(SPO2)+sizeof(NIBP)+sizeof(TEMP)+sizeof(FETUS))

//床边机数据单元
typedef struct _CLIENT_DATA {
	DWORD data_opt;
	BYTE  data_buf[DATA_BUF_MAX];
} CLIENT_DATA;

//定义显示数据单元
typedef struct _DISPLAY_DATA {
	BYTE ecg1_lead;
	BYTE ecg2_lead;
	BYTE ecg_filter;
	BYTE ecg_gain;
	BYTE ecg_stop;
	BYTE ecg_paceangy;
	BYTE ecg_stangy;
	BYTE ecg_rhythm;
	WORD hr; //心率
	WORD hr_h;
	WORD hr_l;
	WORD st1;
	WORD st1_h;
	WORD st1_l;
	WORD st2;
	WORD st2_h;
	WORD st2_l;
	WORD pvc;
	WORD pvc_h;
	WORD rr; //呼吸率
	WORD rr_h;
	WORD rr_l;
	WORD sys; //收缩压
	WORD mea; //平均压
	WORD dia; //舒张压
	WORD sys_h;
	WORD sys_l;
	WORD mea_h;
	WORD mea_l;
	WORD dia_h;
	WORD dia_l;
	WORD nibp_pump;
	WORD nibp_mode;
	WORD t1; //体温1
	WORD t1_h;
	WORD t1_l;
	WORD t2; //体温2
	WORD t2_h;
	WORD t2_l;
	WORD spo2; //血氧
	WORD spo2_h;
	WORD spo2_l;
	WORD pr;
	WORD pr_h;
	WORD pr_l;
	WORD fhr1; //胎心率1
	WORD fhr2; //胎心率2
	WORD fhr_h;
	WORD fhr_l;
	WORD toco; //宫缩
	WORD fm;   //胎动次数
	WORD ecg_error;
	WORD resp_error;
	WORD nibp_error;
	WORD spo2_error;
	WORD temp_error;
	WORD fhr_error;
	WORD bio_alarm_type;
	WORD bio_alarm_level;
	BYTE hr_src;//心率来源
} DISPLAY_DATA;

typedef enum _BIO_ALARM_TYPE{
	BIO_ALARM_HR = 1,
	BIO_ALARM_ARR,
	BIO_ALARM_PVC,
	BIO_ALARM_ST,
	BIO_ALARM_RR,
	BIO_ALARM_PR,
	BIO_ALARM_SPO2,
	BIO_ALARM_NIBP,
	BIO_ALARM_TEMP,
	BIO_ALARM_FHR
} BIO_ALARM_TYPE;

//定义各种类型的通讯数据体
typedef union _BODY {
	PATIENTINFO pi;
	CLIENT_DATA cd;
	ECGCTRLINFO setup_ecg;
	NIBPALARMINFO setup_nibp;
	RESPALARMINFO setup_resp;
	TEMPALARMINFO setup_temp;
	SPO2ALARMINFO setup_spo2;
	NIBPCTRLINFO nibp_pump;
	FHRALARMINFO setup_fhr;
} BODY;

//定义数据包
typedef struct _SOCK_PKG {
	HEAD head;
	BODY body;
} SOCK_PKG;
//

class CFMSocketUnit;
class CStructTools
{
private:
	static int GetClientDataSize(DWORD opt);
	static BOOL CheckFlag(BYTE* buffer);
	static void InitFlag(HEAD& h);
	static void InitHead(HEAD& h, BYTE bed_num, BYTE type, WORD size);

public:
	static BOOL SendData(CFMSocketUnit* pu, BYTE* buffer, int length);
	static int  CheckData(BYTE* buffer, int length, int* data_size, int* expect_size);

	//只有数据头的通讯包
	static BOOL SendQuit(CFMSocketUnit* pu);
	static BOOL SendNext(CFMSocketUnit* pu);
	static BOOL SendBusy(CFMSocketUnit* pu);
	static BOOL SendSetupECG(CFMSocketUnit* pu,
		int ecg_l1, int ecg_l2, int filter, int gain, int stop, int paceangy, int stangy, int rhythm, 
		int hr_h, int hr_l, int st1_h, int st1_l, int st2_h, int st2_l, int pvc_h, int pvc_l);
	static BOOL SendSetupNIBP(CFMSocketUnit* pu,
		int sys_h, int sys_l, int mea_h, int mea_l, int dia_h, int dia_l, int nibp_mode);
	static BOOL SendSetupRESP(CFMSocketUnit* pu,
		int rr_h, int rr_l);
	static BOOL SendSetupSPO2(CFMSocketUnit* pu,
		int spo2_h, int spo2_l, int pr_h, int pr_l);
	static BOOL SendSetupTEMP(CFMSocketUnit* pu,
		int t1_h, int t1_l, int t2_h, int t2_l);
	static BOOL SendNIBPPumpCmd(CFMSocketUnit* pu, int cmd);
	static BOOL SendSetupFHR(CFMSocketUnit* pu,
		int fhr_h, int fhr_l);

	//带有数据体的通讯包
	static BOOL SendPatientInfo(CFMSocketUnit* pu, PATIENTINFO& pi);
	static BOOL SendClientData(CFMSocketUnit* pu, DWORD opt,
		ECG& e, RESP& r, SPO2& s, NIBP& n, TEMP& t, FETUS& f);

	//解析数据体
	static void ParseClientData(CLIENT_DATA* pcd,
		ECG** ppe, RESP** ppr, SPO2** pps, NIBP** ppn, TEMP** ppt, FETUS** ppf);

	//打印出接收数据包的内容
	static CString DumpHead(SOCK_PKG* pk);
	static CString DumpClientData(SOCK_PKG* pk);
	static CString DumpPatientInfo(SOCK_PKG* pk);
	static CString DumpECG(ECG* pe);
	static CString DumpRESP(RESP* pr);
	static CString DumpSPO2(SPO2* ps);
	static CString DumpNIBP(NIBP* pn);
	static CString DumpTEMP(TEMP* pt);
	static CString DumpFETUS(FETUS* pf);

	//判断是不是空数据包
	static BOOL IsNullData(CLIENT_DATA* pcd);
};

//抽值插值算法
void CompressData(BYTE *dest, BYTE *src, int destCount, int srcCount, float scaleY = 1.0, int baseY = 0);

//当使用二通道参数板（目前出货的为此板）监护时，如果监护显示导联为I  II导时，
//中央机可显示最多六道波形。如监护显示为非I II导时，那中央机就只能显示二道波形。
//如果使用三道波的参数板，但中央机就可一直收到I  II V三道波形，则其它导联都可计算得出。则可一直最大显示7导心电波。
//I=LA-RA,
//II=LL-RA,
//III=LL-LA,
//AVR=RA-(LA+LL)/2,
//AVL=LA-(RA+LL)/2,
//AVF=LL-(RA+LA)/2,
//V=V-(RA+LA+LL)/3。

#endif // __FM_STRUCTS_H__
