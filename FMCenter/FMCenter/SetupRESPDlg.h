#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CSetupRESPDlg 对话框
class CFMRecordUnit;
class CSetupRESPDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSetupRESPDlg)

private:
	CFMRecordUnit* m_pru;

public:
	CSetupRESPDlg(CWnd* pParent, CFMRecordUnit* pru);
	virtual ~CSetupRESPDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_SETUP_RESP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edtRRHigh;
	CEdit m_edtRRLow;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
