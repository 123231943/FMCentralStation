#include "stdafx.h"
#include "FMCenter.h"
#include "PrintFrame.h"
#include "PrintWrapperView.h"


IMPLEMENT_DYNAMIC(CPrintFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CPrintFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CPrintFrame)
	ON_WM_CREATE()    
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CPrintFrame construction/destruction
CPrintFrame::CPrintFrame(DRAWFUN pDraw, CWnd*pOldW, CWnd* pCallW, BOOL bDirect, LPCTSTR stTitle)
{
	theApp.SetPrintMainWnd(this);

	Draw     = pDraw;
	pOldWnd  = pOldW;
	pCallWnd = pCallW;
	bDirectPrint = bDirect;
	CString ss;
	if (stTitle != NULL) {
		ss = stTitle;
	}
	else {        
		pOldW->GetWindowText(ss);
		if (! ss.IsEmpty()) {
			ss = ss + _T(" (预览)");
		}
		else {
			ss = ss + _T("打印预览");
		}
	}

	if (! Create(NULL,ss,WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE,CRect(200,200,500,500))) {
		TRACE0("Failed to create view window\n");
	}
}

CPrintFrame::~CPrintFrame()
{
	pOldWnd->ShowWindow(SW_SHOW);
	pCallWnd = NULL;
	pOldWnd  = NULL;
	bDirectPrint = FALSE;
}

int CPrintFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1) {
		return -1;
	}

	CCreateContext context;
	context.m_pNewViewClass = RUNTIME_CLASS(CPrintWrapperView);
	context.m_pCurrentFrame = this;
	context.m_pCurrentDoc = NULL;
	context.m_pLastView = NULL;
	m_pView = STATIC_DOWNCAST(CPrintWrapperView, CreateView(&context)); //CreateView(&context);
	if(NULL!=m_pView){
		if (! bDirectPrint) {
			m_pView->ShowWindow(SW_SHOW);
		}
		SetActiveView(m_pView);
	}
	SetIcon(pOldWnd->GetIcon(FALSE),FALSE);
	SetIcon(pOldWnd->GetIcon(TRUE),TRUE);
	ShowWindow(SW_MAX);//SW_MAXIMIZE);
    
	//theApp.m_pMainWnd = this;
	if (bDirectPrint) {
		m_pView->SendMessage(WM_COMMAND, ID_FILE_PRINT);        
	}
	else {
		m_pView->OnFilePrintPreview(this);    
		pOldWnd->ShowWindow(SW_HIDE);
	}

	//使窗口全屏，强占顶层
	MoveWindow(0, 0, CLIENT_W, CLIENT_H);
#ifndef DEBUG
	//下面这句话会影响调试器工作，暂时屏蔽。
	SetWindowPos(&wndTopMost, 0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
#endif
	return 0;
}

BOOL CPrintFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if (! CFrameWnd::PreCreateWindow(cs)) {
		return FALSE;
	}
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	//去掉预览窗口的最大化最小化按钮
	cs.style = cs.style & ~WS_MAXIMIZEBOX & ~WS_MINIMIZEBOX; 
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CPrintFrame diagnostics

#ifdef _DEBUG
void CPrintFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CPrintFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPrintFrame message handlers
BOOL CPrintFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// let the view have first crack at the command
	if (m_pView->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo)) {
		return TRUE;
	}

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

//////////////////////////////////////////////////////////////////////////
// 源代码中使用的框架销毁函数有bug，这里暂时不使用正常销毁-AWEN
void CPrintFrame::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	//CFrameWnd::OnClose();
	CFrameWnd::DestroyWindow();
}