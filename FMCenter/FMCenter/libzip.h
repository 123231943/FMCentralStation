#pragma once

#include "zip.h"

class libzip
{
private:
	BYTE* m_pFileBuffer;
	HZIP  m_hz;

public:
	libzip(void);
	virtual ~libzip(void);

	BOOL load(BYTE* buffer, int len);
	BOOL open(CString sFilePath);
	void close(void);
	int  getItemSize(CString sFileName);
	BOOL loadItem(CString sFileName, BYTE* buff, int buff_size);
};

