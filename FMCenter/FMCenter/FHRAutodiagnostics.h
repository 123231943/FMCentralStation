#pragma once

//自动诊断使用的dll中的函数形式声明
typedef short (__stdcall *FHR_AutodiagnosticsFunction)(BYTE* buf_fhr, BYTE* buf_toco, BYTE* buf_result, int data_len);
#define FHR_AUTODIAGNO_INPUT_LEN       (2400)
#define FHR_AUTODIAGNO_RESULT_LEN      (500)

typedef struct _DIAGNOSIS_FLAG {
	short nIndex;
	short nY;
	char  nType; //正的为加速类型，负的为减速类型
} DIAGNOSIS_FLAG;

typedef struct _FISCHER {
	BYTE iAccCount;     //加速的个数
	BYTE iSmallAcc;     //小加速的个数
	BYTE iDelayAcc;     //稽留加速的个数
	BYTE iDelayDec;     //迟发减速的个数
	BYTE iHeavyDec;     //重变化减速的个数
	BYTE iLightDec;     //轻变化减速的个数
	BYTE iEarlyDec;     //早期减速的个数
	BYTE iProlongDec;   //延长减速的个数
	BYTE iBaseline;     //20分钟内基线的值
	BYTE iLTV_AB;       //振幅变异值（放大10倍）
	BYTE iLTV_T;        //周期变异值（放大10倍）
	BYTE iBaselineMark; //基线得分
	BYTE iLTV_AB_Mark;  //振幅变异得分
	BYTE iLTV_T_Mark;   //周期变异得分
	BYTE iAccMark;      //加速得分
	BYTE iDecMark;      //减速得分
	BYTE iTotalMark;    //总分
} FISCHER;

class CFHRAutodiagnostics
{
private:
	static FHR_AutodiagnosticsFunction m_fnFHRAutodiagno;
	static CCriticalSection m_csDiagnoLock;

public:
	static BOOL InitModule(CString sDLLPath);

private:
	BOOL m_bSuccess;
	BYTE m_fhr_buf[FHR_AUTODIAGNO_INPUT_LEN];
	BYTE m_toco_buf[FHR_AUTODIAGNO_INPUT_LEN];
	BYTE m_autodiagnostics_result[FHR_AUTODIAGNO_RESULT_LEN];
	
	DIAGNOSIS_FLAG m_flag_buf[FHR_AUTODIAGNO_INPUT_LEN]; //不会超过这个长度了
	int m_nFlagBufLen;
	FISCHER m_fischer;

private:
	void ParseResult(void);
	void ParseAcc(BYTE* buf, int* pi, DIAGNOSIS_FLAG* pFlagBuf, int* pi_flag);
	void ParseDec(BYTE* buf, int* pi, DIAGNOSIS_FLAG* pFlagBuf, int* pi_flag);

public:
	CFHRAutodiagnostics(void);
	virtual ~CFHRAutodiagnostics(void);

public:
	void SetBuffer(BYTE* fhr_buf, BYTE* toco_buf, short buf_len);
	BOOL StartAutodiagno(void);
	inline BOOL Success(void) { return m_bSuccess; };

	//查找加速减速标注
	DIAGNOSIS_FLAG* FindFlag(int indexStart, int indexCount);

	//获得胎心率基线
	inline int GetFhrBaseline(void) { return m_fischer.iBaseline; };   

	//获得加速次数
	inline int GetAccCount(void) { return (m_fischer.iAccCount + m_fischer.iSmallAcc + m_fischer.iDelayAcc); };      

	//获得Fischer评分
	inline int GetFischerMark(void) { return m_fischer.iTotalMark; };   

	//胎心率基线评分
	inline int GetBaselineMark(void) { return m_fischer.iBaselineMark; };  

	//振幅变异评分
	inline int GetLTV_AB_Mark(void) { return m_fischer.iLTV_AB_Mark; };   

	//周期变异评分
	inline int GetLTV_T_Mark(void) { return m_fischer.iLTV_T_Mark; };    

	//加速评分
	inline int GetAccMark(void) { return m_fischer.iAccMark; };       

	//减速评分
	inline int GetDecMark(void) { return m_fischer.iDecMark; };       
};

