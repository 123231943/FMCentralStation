#pragma once
#include "reviewdlg.h"
class CPrintTendTable : public CPrintTool
{
public:
	CPrintTendTable(CReviewDlg* pdlg);
	virtual ~CPrintTendTable(void);

public:
	virtual void InitSetup(int type=0);
	virtual void Print(CDC* pdc,  CPrintInfo* pInfo);

private:
	CFont m_fontEcgInfo;
	CFont* m_pOldFont;

private:
	void SelectEcgInfoFont(CDC* pdc);
	void ResetFont(CDC* pdc);
	void DrawHead(CDC* pdc, CRect& rc);
	void DrawGrid(CDC* pdc, CRect& rc);
	void DrawInfo(CDC* pdc, CRect& rc);
	void DrawData(CDC* pdc, CRect& rc);
	void DrawAlarmGrid(CDC* pdc, int x, int y);
};

