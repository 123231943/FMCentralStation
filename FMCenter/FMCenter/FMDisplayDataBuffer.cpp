#include "stdafx.h"
#include "FMDisplayDataBuffer.h"
#include "FHRAutodiagnostics.h"

CFMDisplayDataBuffer::CFMDisplayDataBuffer(void)
{
	CSmartLock lock(m_csBufLock);

	//创建各个缓冲区
	m_arNUM_Buf  = new DISPLAY_DATA[DISPLAY_DATA_BUFFER_SIZE * 2];
	m_arECG1_Buf = new BYTE[DISPLAY_DATA_BUFFER_SIZE * 2 * DISPLAY_ECG_UNIT_SIZE];
	m_arECG2_Buf = new BYTE[DISPLAY_DATA_BUFFER_SIZE * 2 * DISPLAY_ECG_UNIT_SIZE];
	m_arECGv_Buf = new BYTE[DISPLAY_DATA_BUFFER_SIZE * 2 * DISPLAY_ECG_UNIT_SIZE];
	m_arSPO2_Buf = new BYTE[DISPLAY_DATA_BUFFER_SIZE * 2 * DISPLAY_SPO2_UNIT_SIZE];
	m_arRESP_Buf = new BYTE[DISPLAY_DATA_BUFFER_SIZE * 2 * DISPLAY_RESP_UNIT_SIZE];

	m_arFHR1_Buf = new WORD[DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * DISPLAY_FHR_UNIT_SIZE];
	m_arFHR2_Buf = new WORD[DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * DISPLAY_FHR_UNIT_SIZE];
	m_arTOCO_Buf = new WORD[DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * DISPLAY_FHR_UNIT_SIZE];
	m_arMARK_Buf = new BYTE[DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * DISPLAY_MARK_UNIT_SIZE];
	m_arTOCOReset_Buf = new BYTE[DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * DISPLAY_MARK_UNIT_SIZE];

	m_arDiagno1Buf = new DISPLAY_FLAG[DISPLAY_DATA_FHR_BUFFER_SIZE * 2];
	m_arDiagno2Buf = new DISPLAY_FLAG[DISPLAY_DATA_FHR_BUFFER_SIZE * 2];
	ResetBuffer();

	m_pAuto = new CFHRAutodiagnostics();
}

CFMDisplayDataBuffer::~CFMDisplayDataBuffer(void)
{
	CSmartLock lock(m_csBufLock);

	//释放各个缓冲区
	if (m_arNUM_Buf) {
		delete[] m_arNUM_Buf;
		m_arNUM_Buf = NULL;
	}
	if (m_arECG1_Buf) {
		delete[] m_arECG1_Buf;
		m_arECG1_Buf = NULL;
	}
	if (m_arECG2_Buf) {
		delete[] m_arECG2_Buf;
		m_arECG2_Buf = NULL;
	}
	if (m_arECGv_Buf) {
		delete[] m_arECGv_Buf;
		m_arECGv_Buf = NULL;
	}
	if (m_arSPO2_Buf) {
		delete[] m_arSPO2_Buf;
		m_arSPO2_Buf = NULL;
	}
	if (m_arRESP_Buf) {
		delete[] m_arRESP_Buf;
		m_arRESP_Buf = NULL;
	}
	if (m_arFHR1_Buf) {
		delete[] m_arFHR1_Buf;
		m_arFHR1_Buf = NULL;
	}
	if (m_arFHR2_Buf) {
		delete[] m_arFHR2_Buf;
		m_arFHR2_Buf = NULL;
	}
	if (m_arTOCO_Buf) {
		delete[] m_arTOCO_Buf;
		m_arTOCO_Buf = NULL;
	}
	if (m_arMARK_Buf) {
		delete[] m_arMARK_Buf;
		m_arMARK_Buf = NULL;
	}
	if (m_arTOCOReset_Buf) {
		delete[] m_arTOCOReset_Buf;
		m_arTOCOReset_Buf = NULL;
	}
	if (m_arDiagno1Buf) {
		delete[] m_arDiagno1Buf;
		m_arDiagno1Buf = NULL;
	}
	if (m_arDiagno2Buf) {
		delete[] m_arDiagno2Buf;
		m_arDiagno2Buf = NULL;
	}
	if (m_pAuto) {
		delete m_pAuto;
		m_pAuto = NULL;
	}
}

void CFMDisplayDataBuffer::ResetBuffer(void)
{
	CSmartLock lock(m_csBufLock);
	ZeroMemory(m_arNUM_Buf,  DISPLAY_DATA_BUFFER_SIZE * 2 * sizeof(DISPLAY_DATA));
	ZeroMemory(m_arECG1_Buf, DISPLAY_DATA_BUFFER_SIZE * 2 * DISPLAY_ECG_UNIT_SIZE);
	ZeroMemory(m_arECG2_Buf, DISPLAY_DATA_BUFFER_SIZE * 2 * DISPLAY_ECG_UNIT_SIZE);
	ZeroMemory(m_arECGv_Buf, DISPLAY_DATA_BUFFER_SIZE * 2 * DISPLAY_ECG_UNIT_SIZE);
	ZeroMemory(m_arSPO2_Buf, DISPLAY_DATA_BUFFER_SIZE * 2 * DISPLAY_SPO2_UNIT_SIZE);
	ZeroMemory(m_arRESP_Buf, DISPLAY_DATA_BUFFER_SIZE * 2 * DISPLAY_RESP_UNIT_SIZE);

	ZeroMemory(m_arFHR1_Buf, DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * DISPLAY_FHR_UNIT_SIZE * sizeof(WORD));
	ZeroMemory(m_arFHR2_Buf, DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * DISPLAY_FHR_UNIT_SIZE * sizeof(WORD));
	ZeroMemory(m_arTOCO_Buf, DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * DISPLAY_FHR_UNIT_SIZE * sizeof(WORD));
	ZeroMemory(m_arMARK_Buf, DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * DISPLAY_MARK_UNIT_SIZE);
	ZeroMemory(m_arTOCOReset_Buf, DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * DISPLAY_MARK_UNIT_SIZE);

	ZeroMemory(m_arDiagno1Buf, DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * sizeof(DISPLAY_FLAG));
	ZeroMemory(m_arDiagno2Buf, DISPLAY_DATA_FHR_BUFFER_SIZE * 2 * sizeof(DISPLAY_FLAG));

	m_nTimeSpan = 0;
	m_nBufferLen = 0;
	m_nBufferStart = 0;
	m_nFHRBufferLen = 0;
	m_nFHRBufferStart = 0;
}

void CFMDisplayDataBuffer::Add(CLIENT_DATA& cd)
{
	CSmartLock lock(m_csBufLock);

	//写入位置
	int nWriteIndex = m_nBufferStart + m_nBufferLen;
	int nFHRWriteIndex = m_nFHRBufferStart + m_nFHRBufferLen;

	//拷入各个列表中
	DISPLAY_DATA* pNum = &(m_arNUM_Buf[nWriteIndex]);
	BYTE* pECG1 = &(m_arECG1_Buf[nWriteIndex * DISPLAY_ECG_UNIT_SIZE]);
	BYTE* pECG2 = &(m_arECG2_Buf[nWriteIndex * DISPLAY_ECG_UNIT_SIZE]);
	BYTE* pECGv = &(m_arECGv_Buf[nWriteIndex * DISPLAY_ECG_UNIT_SIZE]);
	BYTE* pSPO2 = &(m_arSPO2_Buf[nWriteIndex * DISPLAY_SPO2_UNIT_SIZE]);
	BYTE* pRESP = &(m_arRESP_Buf[nWriteIndex * DISPLAY_RESP_UNIT_SIZE]);

	WORD* pFHR1 = &(m_arFHR1_Buf[nFHRWriteIndex * DISPLAY_FHR_UNIT_SIZE]);
	WORD* pFHR2 = &(m_arFHR2_Buf[nFHRWriteIndex * DISPLAY_FHR_UNIT_SIZE]);
	WORD* pTOCO = &(m_arTOCO_Buf[nFHRWriteIndex * DISPLAY_FHR_UNIT_SIZE]);
	BYTE* pFMMARK = &(m_arMARK_Buf[nFHRWriteIndex * DISPLAY_MARK_UNIT_SIZE]);
	BYTE* pTOCOReset = &(m_arTOCOReset_Buf[nFHRWriteIndex * DISPLAY_MARK_UNIT_SIZE]);

	DISPLAY_FLAG* pDiagno1 = &(m_arDiagno1Buf[nFHRWriteIndex]);
	DISPLAY_FLAG* pDiagno2 = &(m_arDiagno2Buf[nFHRWriteIndex]);
	pDiagno1->baseline = 0;
	pDiagno1->baseline = 0;
	pDiagno2->baseline = 0;
	pDiagno2->baseline = 0;

	//第一个值复制上一组的最后一个值，以使绘制曲线连续(胎动标志除外)
	if (nWriteIndex > 0) {
		(*pECG1) = (*(pECG1 - 1)); 
		(*pECG2) = (*(pECG2 - 1)); 
		(*pECGv) = (*(pECGv - 1)); 
		(*pSPO2) = (*(pSPO2 - 1)); 
		(*pRESP) = (*(pRESP - 1)); 
		(*pFHR1) = (*(pFHR1 - 1)); 
		(*pFHR2) = (*(pFHR2 - 1)); 
		(*pTOCO) = (*(pTOCO - 1)); 
	}

	pECG1 ++;
	pECG2 ++;
	pECGv ++;
	pSPO2 ++;
	pRESP ++;
	pFHR1 ++;
	pFHR2 ++;
	pTOCO ++;

	ECG* pe;
	RESP* pr;
	SPO2* ps;
	NIBP* pn;
	TEMP* pt;
	FETUS* pf;
	CStructTools::ParseClientData(&cd, &pe, &pr, &ps, &pn, &pt, &pf);

	pNum->ecg_error = 0;
	pNum->resp_error = 0;
	pNum->spo2_error = 0;
	pNum->nibp_error = 0;
	pNum->temp_error = 0;
	pNum->fhr_error = 0;
	pNum->bio_alarm_type = 0;
	pNum->bio_alarm_level = 0;

	if (pe) {
		pNum->ecg1_lead  = pe->ecg_lead1;
		pNum->ecg2_lead  = pe->ecg_lead2;
		pNum->ecg_filter = pe->ecg_filter;
		pNum->ecg_gain   = pe->ecg_gain;
		pNum->ecg_stop   = pe->ecg_stop;
		pNum->ecg_paceangy = pe->ecg_paceangy;
		pNum->ecg_stangy = pe->ecg_stangy;
		pNum->ecg_rhythm = pe->ecg_rhythm;
		pNum->hr         = CHECK_HR(pe->hr);
		pNum->hr_h       = CHECK_HR(pe->hr_high);
		pNum->hr_l       = CHECK_HR(pe->hr_low);
		pNum->st1        = CHECK_ST(pe->st1);
		pNum->st1_h      = CHECK_ST(pe->st1_high);
		pNum->st1_l      = CHECK_ST(pe->st1_low);
		pNum->st2        = CHECK_ST(pe->st2);
		pNum->st2_h      = CHECK_ST(pe->st2_high);
		pNum->st2_l      = CHECK_ST(pe->st2_low);
		pNum->pvc        = CHECK_ST(pe->pvc);
		pNum->pvc_h      = pe->pvc_high;
		memcpy(pECG1, &(pe->ecg1data[0]), DISPLAY_ECG_UNIT_SIZE - 1);
		memcpy(pECG2, &(pe->ecg2data[0]), DISPLAY_ECG_UNIT_SIZE - 1);
		memcpy(pECGv, &(pe->ecg3data[0]), DISPLAY_ECG_UNIT_SIZE - 1);
		pNum->hr_src     = pe->hr_source;
		if (pe->error != 0) {
			pNum->ecg_error = pe->error;
		}
		else if (pe->arr_status != 0) {
			pNum->bio_alarm_type = BIO_ALARM_ARR;
			pNum->bio_alarm_level = pe->arr_status;
		}
		else {
			if (pe->hr_level > pNum->bio_alarm_level) {
				pNum->bio_alarm_type = BIO_ALARM_HR;
				pNum->bio_alarm_level = pe->hr_level;
			}
			if (pe->pvc_level > pNum->bio_alarm_level) {
				pNum->bio_alarm_type = BIO_ALARM_PVC;
				pNum->bio_alarm_level = pe->pvc_level;
			}
			if (pe->st_level > pNum->bio_alarm_level) {
				pNum->bio_alarm_type = BIO_ALARM_ST;
				pNum->bio_alarm_level = pe->st_level;
			}
		}
	}

	if (pr) {
		pNum->rr   = CHECK_RR(pr->rr);
		pNum->rr_h = CHECK_RR(pr->rr_high);
		pNum->rr_l = CHECK_RR(pr->rr_low);
		memcpy(pRESP, &(pr->respdata[0]), DISPLAY_RESP_UNIT_SIZE - 1);
		if (pr->error != 0) {
			pNum->resp_error = pr->error;
		}
		else {
			if (pr->rr_level > pNum->bio_alarm_level) {
				pNum->bio_alarm_type = BIO_ALARM_RR;
				pNum->bio_alarm_level = pr->rr_level;
			}
		}
	}

	if (ps) {
		pNum->spo2   = CHECK_SPO2(ps->spo2);
		pNum->spo2_h = CHECK_SPO2(ps->spo2_high);
		pNum->spo2_l = CHECK_SPO2(ps->spo2_low);
		pNum->pr     = CHECK_PR(ps->pr);
		pNum->pr_h   = CHECK_PR(ps->pr_high);
		pNum->pr_l   = CHECK_PR(ps->pr_low);
		memcpy(pSPO2, &(ps->wavedata[0]), DISPLAY_SPO2_UNIT_SIZE - 1);
		if (ps->error != 0) {
			pNum->spo2_error = ps->error;
		}
		else {
			if (ps->pr_level > pNum->bio_alarm_level) {
				pNum->bio_alarm_type = BIO_ALARM_PR;
				pNum->bio_alarm_level = ps->pr_level;
			}
			if (ps->spo2_level > pNum->bio_alarm_level) {
				pNum->bio_alarm_type = BIO_ALARM_SPO2;
				pNum->bio_alarm_level = ps->spo2_level;
			}
		}
	}

	if (pn) {
		pNum->sys = CHECK_NIBP(pn->sys);
		pNum->mea = CHECK_NIBP(pn->mea);
		pNum->dia = CHECK_NIBP(pn->dia);
		pNum->sys_h = CHECK_NIBP(pn->sys_high);
		pNum->sys_l = CHECK_NIBP(pn->sys_low);
		pNum->mea_h = CHECK_NIBP(pn->mea_high);
		pNum->mea_l = CHECK_NIBP(pn->mea_low);
		pNum->dia_h = CHECK_NIBP(pn->dia_high);
		pNum->dia_l = CHECK_NIBP(pn->dia_low);
		pNum->nibp_pump = CHECK_NIBP(pn->press);
		pNum->nibp_mode = pn->mode;
		if (pn->error != 0) {
			pNum->nibp_error = pn->error;
		}
		else {
			if (pn->nibp_level > pNum->bio_alarm_level) {
				pNum->bio_alarm_type = BIO_ALARM_NIBP;
				pNum->bio_alarm_level = pn->nibp_level;
			}
		}
	}

	if (pt) {
		pNum->t1   = CHECK_TEMP(pt->t1);
		pNum->t1_h = CHECK_TEMP(pt->t1_high);
		pNum->t1_l = CHECK_TEMP(pt->t1_low);
		pNum->t2   = CHECK_TEMP(pt->t2);
		pNum->t2_h = CHECK_TEMP(pt->t2_high);
		pNum->t2_l = CHECK_TEMP(pt->t2_low);
		if (pt->error != 0) {
			pNum->temp_error = pt->error;
		}
		else {
			if (pt->t_level > pNum->bio_alarm_level) {
				pNum->bio_alarm_type = BIO_ALARM_TEMP;
				pNum->bio_alarm_level = pt->t_level;
			}
		}
	}

	if (pf) {
		pNum->fhr1  = CHECK_FHR(pf->fhr1[3]);
		pNum->fhr2  = CHECK_FHR(pf->fhr2[3]);
		pNum->toco  = CHECK_TOCO(pf->uc[3]);
		pNum->fm    = pf->fm;
		pNum->fhr_h = CHECK_FHR(pf->fhr_high);
		pNum->fhr_l = CHECK_FHR(pf->fhr_low);
		pFHR1[0]    = CHECK_FHR(pf->fhr1[1]);
		pFHR1[1]    = CHECK_FHR(pf->fhr1[3]);
		pFHR2[0]    = CHECK_FHR(pf->fhr2[1]);
		pFHR2[1]    = CHECK_FHR(pf->fhr2[3]);
		pTOCO[0]    = CHECK_TOCO(pf->uc[1]);
		pTOCO[1]    = CHECK_TOCO(pf->uc[3]);
		pFMMARK[0]  = pf->marked;
		pTOCOReset[0] = pf->ucreset;
		if (pf->error != 0) {
			pNum->fhr_error = pf->error;
		}
		else {
			if (pf->fhr_level > pNum->bio_alarm_level) {
				pNum->bio_alarm_type = BIO_ALARM_FHR;
				pNum->bio_alarm_level = pf->fhr_level;
			}
		}
	}

	//如果是第一次赋值，那么让第一个值等于第二个值
	if (nWriteIndex == 0) {
		(*(pECG1 - 1)) = (*pECG1); 
		(*(pECG2 - 1)) = (*pECG2); 
		(*(pECGv - 1)) = (*pECGv); 
		(*(pSPO2 - 1)) = (*pSPO2); 
		(*(pRESP - 1)) = (*pRESP); 
		(*(pFHR1 - 1)) = (*pFHR1); 
		(*(pFHR2 - 1)) = (*pFHR2); 
		(*(pTOCO - 1)) = (*pTOCO); 
	}

	//调整写入后的长度
	if (m_nBufferLen < DISPLAY_DATA_BUFFER_SIZE) {
		m_nBufferLen ++;
	}
	else {
		m_nBufferStart ++;
		if (DISPLAY_DATA_BUFFER_SIZE == m_nBufferStart) {
			//进行一次数据搬移，把数据从缓冲区尾部搬到缓冲区顶端
			memcpy(&(m_arNUM_Buf[0]),
				&(m_arNUM_Buf[DISPLAY_DATA_BUFFER_SIZE]),
				DISPLAY_DATA_BUFFER_SIZE * sizeof(DISPLAY_DATA));
			memcpy(&(m_arECG1_Buf[0]),
				&(m_arECG1_Buf[DISPLAY_DATA_BUFFER_SIZE * DISPLAY_ECG_UNIT_SIZE]),
				DISPLAY_DATA_BUFFER_SIZE * DISPLAY_ECG_UNIT_SIZE);
			memcpy(&(m_arECG2_Buf[0]),
				&(m_arECG2_Buf[DISPLAY_DATA_BUFFER_SIZE * DISPLAY_ECG_UNIT_SIZE]),
				DISPLAY_DATA_BUFFER_SIZE * DISPLAY_ECG_UNIT_SIZE);
			memcpy(&(m_arECGv_Buf[0]),
				&(m_arECGv_Buf[DISPLAY_DATA_BUFFER_SIZE * DISPLAY_ECG_UNIT_SIZE]),
				DISPLAY_DATA_BUFFER_SIZE * DISPLAY_ECG_UNIT_SIZE);
			memcpy(&(m_arSPO2_Buf[0]),
				&(m_arSPO2_Buf[DISPLAY_DATA_BUFFER_SIZE * DISPLAY_SPO2_UNIT_SIZE]),
				DISPLAY_DATA_BUFFER_SIZE * DISPLAY_SPO2_UNIT_SIZE);
			memcpy(&(m_arRESP_Buf[0]),
				&(m_arRESP_Buf[DISPLAY_DATA_BUFFER_SIZE * DISPLAY_RESP_UNIT_SIZE]),
				DISPLAY_DATA_BUFFER_SIZE * DISPLAY_RESP_UNIT_SIZE);

			m_nBufferStart = 0;
		}
	}

	if (m_nFHRBufferLen < DISPLAY_DATA_FHR_BUFFER_SIZE) {
		m_nFHRBufferLen ++;
	}
	else {
		m_nFHRBufferStart ++;
		if (DISPLAY_DATA_FHR_BUFFER_SIZE == m_nFHRBufferStart) {
			memcpy(&(m_arFHR1_Buf[0]),
				&(m_arFHR1_Buf[DISPLAY_DATA_FHR_BUFFER_SIZE * DISPLAY_FHR_UNIT_SIZE]),
				DISPLAY_DATA_FHR_BUFFER_SIZE * DISPLAY_FHR_UNIT_SIZE * sizeof(WORD));
			memcpy(&(m_arFHR2_Buf[0]),
				&(m_arFHR2_Buf[DISPLAY_DATA_FHR_BUFFER_SIZE * DISPLAY_FHR_UNIT_SIZE]),
				DISPLAY_DATA_FHR_BUFFER_SIZE * DISPLAY_FHR_UNIT_SIZE * sizeof(WORD));
			memcpy(&(m_arTOCO_Buf[0]),
				&(m_arTOCO_Buf[DISPLAY_DATA_FHR_BUFFER_SIZE * DISPLAY_FHR_UNIT_SIZE]),
				DISPLAY_DATA_FHR_BUFFER_SIZE * DISPLAY_FHR_UNIT_SIZE * sizeof(WORD));
			memcpy(&(m_arMARK_Buf[0]),
				&(m_arMARK_Buf[DISPLAY_DATA_FHR_BUFFER_SIZE * DISPLAY_MARK_UNIT_SIZE]),
				DISPLAY_DATA_FHR_BUFFER_SIZE * DISPLAY_MARK_UNIT_SIZE);
			memcpy(&(m_arTOCOReset_Buf[0]),
				&(m_arTOCOReset_Buf[DISPLAY_DATA_FHR_BUFFER_SIZE * DISPLAY_MARK_UNIT_SIZE]),
				DISPLAY_DATA_FHR_BUFFER_SIZE * DISPLAY_MARK_UNIT_SIZE);
			memcpy(&(m_arDiagno1Buf[0]),
				&(m_arDiagno1Buf[DISPLAY_DATA_FHR_BUFFER_SIZE]),
				DISPLAY_DATA_FHR_BUFFER_SIZE);
			memcpy(&(m_arDiagno2Buf[0]),
				&(m_arDiagno2Buf[DISPLAY_DATA_FHR_BUFFER_SIZE]),
				DISPLAY_DATA_FHR_BUFFER_SIZE);

			m_nFHRBufferStart = 0;
		}
	}

	m_nTimeSpan ++;
}

int CFMDisplayDataBuffer::GetReadIndex(int nTimeSpan, int* pIndex)
{
	(*pIndex) = nTimeSpan - (m_nTimeSpan - m_nBufferLen);
	
	if ((*pIndex) < 0 || m_nBufferLen == 0) {
		return WAVE_DATA_BEFORE_START;
	}
	if ((*pIndex) >= m_nBufferLen) {
		// 至少读出1组数据
		(*pIndex) = m_nBufferLen - 1;
		return WAVE_DATA_AFTER_END;
	}
	return WAVE_DATA_VALID;
}

int CFMDisplayDataBuffer::GetFHRReadIndex(int nTimeSpan, int* pIndex)
{
	(*pIndex) = nTimeSpan - (m_nTimeSpan - m_nFHRBufferLen);
	
	if ((*pIndex) < 0 || m_nFHRBufferLen == 0) {
		return WAVE_DATA_BEFORE_START;
	}
	if ((*pIndex) >= m_nFHRBufferLen) {
		// 至少读出1组数据
		(*pIndex) = m_nFHRBufferLen - 1;
		return WAVE_DATA_AFTER_END;
	}
	return WAVE_DATA_VALID;
}

DISPLAY_DATA* CFMDisplayDataBuffer::GetNumbData(int nTimeSpan)
{
	CSmartLock lock(m_csBufLock);

	int nIndex;
	if (WAVE_DATA_BEFORE_START == GetReadIndex(nTimeSpan, &nIndex)) {
		return NULL;
	}

	return &(m_arNUM_Buf[m_nBufferStart + nIndex]);
}

DISPLAY_DATA* CFMDisplayDataBuffer::GetLatestNumbData()
{
	CSmartLock lock(m_csBufLock);
	if (m_nBufferLen > 1) {
		return &(m_arNUM_Buf[m_nBufferStart + m_nBufferLen - 1]);
	}
	return NULL;
}

int CFMDisplayDataBuffer::GetWaveData(int nTimeSpan,
	BYTE** ppECG1, BYTE** ppECG2, BYTE** ppECGv, BYTE** ppSPO2, BYTE** ppRESP)
{
	CSmartLock lock(m_csBufLock);

	//返回tm之后的数据
	int nIndex;
	int nRet = GetReadIndex(nTimeSpan, &nIndex);
	if (WAVE_DATA_BEFORE_START == nRet) {
		return nRet;
	}
	
	int nReadIndex = m_nBufferStart + nIndex;
	(*ppECG1) = &(m_arECG1_Buf[nReadIndex * DISPLAY_ECG_UNIT_SIZE]);
	(*ppECG2) = &(m_arECG2_Buf[nReadIndex * DISPLAY_ECG_UNIT_SIZE]);
	(*ppECGv) = &(m_arECGv_Buf[nReadIndex * DISPLAY_ECG_UNIT_SIZE]);
	(*ppSPO2) = &(m_arSPO2_Buf[nReadIndex * DISPLAY_SPO2_UNIT_SIZE]);
	(*ppRESP) = &(m_arRESP_Buf[nReadIndex * DISPLAY_RESP_UNIT_SIZE]);
	return nRet;
}

int CFMDisplayDataBuffer::GetFHRWaveData(int nTimeSpan,
	WORD** ppFHR1, WORD** ppFHR2, WORD** ppTOCO, BYTE** ppMARK, BYTE** ppTOCOReset, DISPLAY_FLAG** ppDiagno1, DISPLAY_FLAG** ppDiagno2)
{
	CSmartLock lock(m_csBufLock);

	//返回tm之后的数据
	int nIndex;
	int nRet = GetFHRReadIndex(nTimeSpan, &nIndex);
	if (WAVE_DATA_BEFORE_START == nRet) {
		return nRet;
	}
	
	int nReadIndex = m_nFHRBufferStart + nIndex;
	(*ppFHR1) = &(m_arFHR1_Buf[nReadIndex * DISPLAY_FHR_UNIT_SIZE]);
	(*ppFHR2) = &(m_arFHR2_Buf[nReadIndex * DISPLAY_FHR_UNIT_SIZE]);
	(*ppTOCO) = &(m_arTOCO_Buf[nReadIndex * DISPLAY_FHR_UNIT_SIZE]);
	(*ppMARK) = &(m_arMARK_Buf[nReadIndex * DISPLAY_MARK_UNIT_SIZE]);
	(*ppTOCOReset) = &(m_arTOCOReset_Buf[nReadIndex * DISPLAY_MARK_UNIT_SIZE]);
	(*ppDiagno1) = &(m_arDiagno1Buf[nReadIndex]);
	(*ppDiagno2) = &(m_arDiagno2Buf[nReadIndex]);
	return nRet;
}

void CFMDisplayDataBuffer::Autodiagnostics()
{
	int i;
	BYTE fhr1_buf[2400];
	BYTE fhr2_buf[2400];
	BYTE toco_buf[2400];
	int nCount = min(m_nFHRBufferLen, 1200);
	int nStart = m_nFHRBufferStart + m_nFHRBufferLen - nCount;
	for (i=0; i<nCount; i++) {
		int nReadIndex = nStart + i;
		int nBuffIndex = i * 2;
		WORD* pFHR1 = &(m_arFHR1_Buf[nReadIndex * DISPLAY_FHR_UNIT_SIZE]);
		WORD* pFHR2 = &(m_arFHR2_Buf[nReadIndex * DISPLAY_FHR_UNIT_SIZE]);
		WORD* pTOCO = &(m_arTOCO_Buf[nReadIndex * DISPLAY_FHR_UNIT_SIZE]);
		fhr1_buf[nBuffIndex]   = INVALID_VALUE == pFHR1[1] ? 0 : (BYTE)pFHR1[1];
		fhr1_buf[nBuffIndex+1] = INVALID_VALUE == pFHR1[2] ? 0 : (BYTE)pFHR1[2];
		fhr2_buf[nBuffIndex]   = INVALID_VALUE == pFHR2[1] ? 0 : (BYTE)pFHR2[1];
		fhr2_buf[nBuffIndex+1] = INVALID_VALUE == pFHR2[2] ? 0 : (BYTE)pFHR2[2];
		toco_buf[nBuffIndex]   = INVALID_VALUE == pTOCO[1] ? 0 : (BYTE)pTOCO[1];
		toco_buf[nBuffIndex+1] = INVALID_VALUE == pTOCO[2] ? 0 : (BYTE)pTOCO[2];
	}

	//分别诊断并保存结果
	m_pAuto->SetBuffer(fhr1_buf, toco_buf, nCount * 2);
	if (m_pAuto->StartAutodiagno()) {
		int nFhr1Baseline = m_pAuto->GetFhrBaseline();
		for (i=0; i<nCount; i++) {
			int nWriteIndex = nStart + i;
			DISPLAY_FLAG* pDiagno1 = &(m_arDiagno1Buf[nWriteIndex]);
			pDiagno1->baseline = nFhr1Baseline;
			DIAGNOSIS_FLAG* pf = m_pAuto->FindFlag(i * 2, 2);
			if (pf) {
				pDiagno1->flag_type = pf->nType;
			}
			else {
				pDiagno1->flag_type = 0;
			}
		}
	}

	m_pAuto->SetBuffer(fhr2_buf, toco_buf, nCount * 2);
	if (m_pAuto->StartAutodiagno()) {
		int nFhr2Baseline = m_pAuto->GetFhrBaseline();
		for (i=0; i<nCount; i++) {
			int nWriteIndex = nStart + i;
			DISPLAY_FLAG* pDiagno2 = &(m_arDiagno2Buf[nWriteIndex]);
			pDiagno2->baseline = nFhr2Baseline;
			DIAGNOSIS_FLAG* pf = m_pAuto->FindFlag(i * 2, 2);
			if (pf) {
				pDiagno2->flag_type = pf->nType;
			}
			else {
				pDiagno2->flag_type = 0;
			}
		}
	}
}
