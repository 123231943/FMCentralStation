#include "stdafx.h"
#include "FMCenter.h"
#include "PrintECG.h"

static const int lft_margin = 200;
static const int top_margin = 150;
static const int page_width = 2500;
static const int page_height = 1750;

static const COLORREF cr_grid_light = RGB(225,225,0);
static const COLORREF cr_grid_dark = RGB(128,128,0);
static const COLORREF cr_ecg_wave = RGB(0,0,0);

static const int grid_w = 10; //1mm一个格子
static const int grid_h = 10;

CPrintECG::CPrintECG(CReviewDlg* pdlg)
	: CPrintTool(pdlg)
{
	m_fontEcgInfo.CreateFont(36, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_pOldFont = NULL;
}

CPrintECG::~CPrintECG(void)
{
	m_pOldFont = NULL;
}

void CPrintECG::SelectEcgInfoFont(CDC* pdc)
{
	m_pOldFont = pdc->SelectObject(&m_fontEcgInfo);
}

void CPrintECG::ResetFont(CDC* pdc)
{
	if (m_pOldFont) {
		pdc->SelectObject(m_pOldFont);
	}
}

void CPrintECG::InitSetup(int type)
{
}

void CPrintECG::Print(CDC* pdc,  CPrintInfo* pInfo)
{
	//在MM_LOMETRIC映射模式下，Y坐标是反向的
	CRect rc(lft_margin, - top_margin, lft_margin + page_width, - top_margin - page_height);
	SelectEcgInfoFont(pdc);
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor((COLORREF)0);
	DrawGrid(pdc, rc);
	DrawInfo(pdc, rc);
	DrawWave(pdc, rc);
	ResetFont(pdc);
}

void CPrintECG::DrawGrid(CDC* pdc, CRect& rc)
{
	CPen penLight(PS_SOLID, 1, cr_grid_light);
	CPen penDark(PS_SOLID, 1, cr_grid_dark);
	CPen* pOldPen = pdc->SelectObject(&penDark);

	int x, y;
	int dark_grid_counter = 0;
	for (x=rc.left; x<=rc.right; x+= grid_w) {
		if (0 == dark_grid_counter) {
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, rc.top);
		pdc->LineTo(x, rc.bottom);

		dark_grid_counter ++;
		if (5 == dark_grid_counter) {
			dark_grid_counter = 0;
		}
	}

	dark_grid_counter = 0;
	for (y=rc.bottom; y<=rc.top; y+= grid_h) {
		if (0 == dark_grid_counter) {
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(rc.left, y);
		pdc->LineTo(rc.right, y);

		dark_grid_counter ++;
		if (5 == dark_grid_counter) {
			dark_grid_counter = 0;
		}
	}

	pdc->SelectObject(pOldPen);
}

void CPrintECG::DrawInfo(CDC* pdc, CRect& rc)
{
	CString sPatientInfo;
	CString sWaveformInfo;

	CString sBedNum;
	CString sMedNum;
	CString sPatName;
	CString sPatGender;
	CString sPatType;
	CString sPatAge;

	sBedNum.Format(_T("床位：%02s"), m_pReviewDlg->m_sBedNum.IsEmpty() ? _T("----") : m_pReviewDlg->m_sBedNum);
	sMedNum.Format(_T("病历号：%s"), m_pReviewDlg->m_sMedicalNum.IsEmpty() ? _T("----") : m_pReviewDlg->m_sMedicalNum);
	sPatName.Format(_T("姓名：%s"), m_pReviewDlg->m_sPatientName.IsEmpty() ? _T("----") : m_pReviewDlg->m_sPatientName);
	sPatGender.Format(_T("性别：%s"), m_pReviewDlg->m_sPatientGender.IsEmpty() ? _T("----") : m_pReviewDlg->m_sPatientGender);
	sPatType.Format(_T("类型：%s"), m_pReviewDlg->m_sPatientType.IsEmpty() ? _T("----") : m_pReviewDlg->m_sPatientType);
	sPatAge.Format(_T("年龄：%s"), m_pReviewDlg->m_sPatientType.IsEmpty() ? _T("--") : m_pReviewDlg->m_sPatientAge);

	sPatientInfo.Format(_T("%5s%20s%30s%20s%20s%20s"), sBedNum, sMedNum, sPatName, sPatGender, sPatType, sPatAge);
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor((COLORREF)0);
	pdc->TextOut(rc.left, rc.top + 36, sPatientInfo);

	CString sStartTime;
	CString sWaveformLen;
	CString sPrintTime;
	int nValidLen;
	CTime tmTableStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	sStartTime = tmTableStart.Format(_T("开始时间：%Y-%m-%d %H:%M:%S"));
	int nWaveformLen = min(60, nValidLen);
	sWaveformLen.Format(_T("波形长度：%d秒 [X:25毫米/秒；Y:10毫米/毫伏]"), nWaveformLen);
	CTime tmNow = CTime::GetCurrentTime();
	sPrintTime = tmNow.Format(_T("打印时间：%Y-%m-%d %H:%M:%S"));

	sWaveformInfo.Format(_T("%20s%30s%55s"), sStartTime, sWaveformLen, sPrintTime);
	pdc->TextOut(rc.left, rc.bottom - 6, sWaveformInfo);
	
	if (! theApp.m_sHospitalName.IsEmpty()) {
		pdc->TextOut(rc.left, rc.bottom - 44, theApp.m_sHospitalName);
	}
}

void CPrintECG::DrawWave(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CLIENT_DATA* pData = m_pReviewDlg->GetCurrentBufferPointer(&nValidLen);
	if (! pData) {
		return;
	}

	CPen penBlack(PS_SOLID, 3, cr_ecg_wave);
	CPen* pOldPen = pdc->SelectObject(&penBlack);

	//用户选择的通道
	int nChannel = m_pReviewDlg->m_cmbSelectChannel.GetCurSel();

	int nDispCount = min(nValidLen, 60);//一张图纸最多显示60秒的波形
	int i;
	BYTE data[ECG_WAVE_DATA_PER_SECOND + 1];
	WORD lead;
	int h = rc.Height() / 7;
	int x = rc.left;
	int y = rc.top + h;
	for (i=0; i<nDispCount; i++) {
		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		switch (nChannel) {
		case 0:
			if (0 == i) {
				data[0] = pe->ecg1data[0];
				lead = pe->ecg_lead1;
				DrawLeadInfo(pdc, x, y+100, h, lead);
			}
			else {
				data[0] = data[ECG_WAVE_DATA_PER_SECOND];
				if (lead != pe->ecg_lead1) {
					lead = pe->ecg_lead1;
					DrawLeadInfo(pdc, x, y+100, h, lead);
				}
			}
			memcpy(&(data[1]), pe->ecg1data, ECG_WAVE_DATA_PER_SECOND);
			break;
		case 1:
			if (0 == i) {
				data[0] = pe->ecg2data[0];
				lead = pe->ecg_lead2;
				DrawLeadInfo(pdc, x, y+100, h, lead);
			}
			else {
				data[0] = data[ECG_WAVE_DATA_PER_SECOND];
				if (lead != pe->ecg_lead2) {
					lead = pe->ecg_lead2;
					DrawLeadInfo(pdc, x, y+100, h, lead);
				}
			}
			memcpy(&(data[1]), pe->ecg2data, ECG_WAVE_DATA_PER_SECOND);
			break;
		}

		int j;
		for (j = 0; j < ECG_WAVE_DATA_PER_SECOND; j ++) {
			int y1 = y - ECG_MID_VALUE + data[j];
			int y2 = y - ECG_MID_VALUE + data[j+1];
			pdc->MoveTo(x, y1);
			pdc->LineTo(x+1, y2);
			x ++;
			if (x >= rc.left + page_width) {
				//绘制到达画布最右端时，跳转到画布的最左端
				x = rc.left;
				y += h;
			}
		}
	}

	pdc->SelectObject(pOldPen);
}

void CPrintECG::DrawLeadInfo(CDC* pdc, int x, int y, int h, int lead)
{
	pdc->MoveTo(x + 1, y + 10);
	pdc->LineTo(x + 1, y + h / 2 + 10);
	pdc->TextOut(x + 5, y + 10, CFMDict::ECGLead_itos(lead));
}

