#include "stdafx.h"
#include "FMCenter.h"
#include "FMSocketUnit.h"
#include "FMSocketThread.h"

static DWORD WINAPI ThreadFuncListen(LPVOID lpParam);
static DWORD WINAPI ThreadFuncComplete(LPVOID lpParam);
static int GetBedIndex(struct sockaddr_in* pca);
static void AcceptConnection(CFMSocketThread* ps);
static BOOL AssociateWithIOCP(CFMSocketUnit* pu);
static BOOL SocketSend(CFMSocketUnit* pu);
static BOOL SocketRecv(CFMSocketUnit* pu);


CFMSocketThread::CFMSocketThread(CFMRecordThread* pRecord)
{
	m_pRecord = pRecord;

	m_sListenSocket = NULL;
	m_hAcceptEvent = NULL;
	m_hShutdownEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_hIOCompletionPort = NULL;
	ZeroMemory(m_arUnits, sizeof(m_arUnits));
	m_hListenThread = NULL;
	m_nWorkThreadCount = 0;
	m_arWorkThreadGroup = NULL;
}

CFMSocketThread::~CFMSocketThread(void)
{
	CleanUp();

	int i;
	for (i=0; i<CLIENT_COUNT; i++) {
		if (m_arUnits[i]) {
			delete m_arUnits[i];
			m_arUnits[i] = NULL;
		}
	}
	ZeroMemory(m_arUnits, sizeof(m_arUnits));

	CloseHandle(m_hIOCompletionPort);
	CloseHandle(m_hShutdownEvent);

	//Clean up memory allocated for the storage of thread handles
	delete[] m_arWorkThreadGroup;
	m_nWorkThreadCount = 0;
}

void CFMSocketThread::CleanUp()
{
	int i;

	//Ask all threads to start shutting down
	SetEvent(m_hShutdownEvent);

	//Let Accept thread go down
	WaitForSingleObject(m_hListenThread, INFINITE);
	CloseListenSocket();

	for (i = 0; i < m_nWorkThreadCount; i++) {
		//Help threads get out of blocking - GetQueuedCompletionStatus()
		PostQueuedCompletionStatus(m_hIOCompletionPort, 0, (DWORD) NULL, NULL);
	}

	//Let Worker Threads shutdown
	WaitForMultipleObjects(m_nWorkThreadCount, m_arWorkThreadGroup, TRUE, INFINITE);

	//We are done with this event
	WSACloseEvent(m_hAcceptEvent);

	//Cleanup dynamic memory allocations, if there are any.
	CleanSocketUnits();
}

BOOL CFMSocketThread::Start()
{
	//创建管理组
	int i;
	for (i=0; i<CLIENT_COUNT; i++) {
		m_arUnits[i] = new CFMSocketUnit(i);
	}

	//创建完成端口
	m_hIOCompletionPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0 );
	if (NULL == m_hIOCompletionPort) {
		CString info;
		info.Format(_T("创建IOCP出错: %ld\n"), WSAGetLastError());
		OutputDebugString(info);
		theApp.WriteLog(_T("CFMSocketThread::Start()函数\n"));
		theApp.WriteLog(info);
		return FALSE;
	}

	//创建监听端口
	if (!CreateListenSocket()) {
		return FALSE;
	}

	//启动监听线程
	DWORD nThreadID;
	m_hListenThread = CreateThread(NULL, 4096, (LPTHREAD_START_ROUTINE)&ThreadFuncListen, this, 0, (LPDWORD)&nThreadID);
	if (m_hListenThread == NULL) {
		return FALSE;
	}

	//启动接收线程
	m_nWorkThreadCount = 1;

	CString info;
	info.Format(_T("创建线程数量: %d\n"), m_nWorkThreadCount);
	OutputDebugString(info);

	m_arWorkThreadGroup = new HANDLE[m_nWorkThreadCount];
	for (i = 0; i < m_nWorkThreadCount; i++) {
		m_arWorkThreadGroup[i] = CreateThread(NULL, 4096,
			(LPTHREAD_START_ROUTINE)&ThreadFuncComplete, this, 0, (LPDWORD)&nThreadID);
		if (m_arWorkThreadGroup[i] == NULL) {
			return FALSE;
		}
	}

	return TRUE;
}

void CFMSocketThread::CloseListenSocket()
{
	//关闭监听端口
	if (m_sListenSocket) {
		closesocket(m_sListenSocket);
		m_sListenSocket = NULL;
	}
}

BOOL CFMSocketThread::CreateListenSocket()
{
	int ret;
	SOCKET s = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (INVALID_SOCKET == s) {
		return FALSE;
	}

	struct sockaddr_in ServerAddress;
	ZeroMemory((char *)&ServerAddress, sizeof(ServerAddress));
	ServerAddress.sin_family = AF_INET;
	ServerAddress.sin_addr.s_addr = INADDR_ANY;
	ServerAddress.sin_port = htons(theApp.GetListenPort());

	ret = bind(s, (struct sockaddr *) &ServerAddress, sizeof(ServerAddress));
	if (SOCKET_ERROR == ret) {
		closesocket(s);
		return FALSE;
	}

	ret = listen(s, CLIENT_COUNT);
	if (SOCKET_ERROR == ret) {
		closesocket(s);
		return FALSE;
	}
	m_sListenSocket = s;

	//创建监听事件
	m_hAcceptEvent = WSACreateEvent();
	if (WSA_INVALID_EVENT == m_hAcceptEvent) {
		return FALSE;
	}

	ret =  WSAEventSelect(m_sListenSocket, m_hAcceptEvent, FD_ACCEPT);
	if (SOCKET_ERROR == ret) {
		WSACloseEvent(m_hAcceptEvent);
		return FALSE;
	}

	return TRUE;
}

void CFMSocketThread::SetSocketUnit(int nBedIndex, BOOL bEnable)
{
	ASSERT(nBedIndex >= 0 && nBedIndex < CLIENT_COUNT);
	CSmartLock lock(m_cs);
	if (bEnable) {
		if (! m_arUnits[nBedIndex]->IsValid()) {
			m_arUnits[nBedIndex]->Enable(TRUE);
		}
	}
	else {
		if (m_arUnits[nBedIndex]->IsValid()) {
			m_arUnits[nBedIndex]->Enable(FALSE);
		}
	}
}

void CFMSocketThread::CleanSocketUnits()
{
	CSmartLock lock(m_cs);

	int i;
	for (i=0; i<CLIENT_COUNT; i++) {
		if (m_arUnits[i]) {
			delete m_arUnits[i];
			m_arUnits[i] = NULL;
		}
	}
}

void CFMSocketThread::CheckSockets()
{
	int i;
	CTime tmNow = CTime::GetCurrentTime();
	CSmartLock lock(m_cs);
	for (i=0; i<CLIENT_COUNT; i++) {
		if (m_arUnits[i]) {
			if (! m_arUnits[i]->CheckSocket(tmNow)) {
				//超时了，关掉它
				SetSocketUnit(i, FALSE);
			}
		}
	}
}

static int GetBedIndex(struct sockaddr_in* pca)
{
//	int nClientPort = htons(pca->sin_port);
	int nClientAddr = (int)(BYTE)pca->sin_addr.S_un.S_un_b.s_b4;
	int nBedNo = nClientAddr - CLIENT_ADDR_BASE;
	if ((nBedNo < 1) || (nBedNo > CLIENT_COUNT)) {
		if (nClientAddr == 1) {
			//本地端口，用第64床做测试
			return 63;
		}
		return -1;
	}
	return nBedNo - 1;
	

//按照新的协议，床旁机统一使用6000端口，不再检查端口号。
//	int nBedNoFromPort = nClientPort - CLIENT_PORT_BASE;
//	int nBedNoFromAddr = nClientAddr - CLIENT_ADDR_BASE;
//#ifdef TEST_MODE_LOCALHOST
//	//测试模式下使用localhost，无法验证ip地址
//	nBedNoFromAddr = nBedNoFromPort;
//#endif //TEST_MODE_LOCALHOST
//	if (nBedNoFromAddr != nBedNoFromPort) {
//		return -1;
//	}
//	if ((nBedNoFromPort < 1) || (nBedNoFromPort > CLIENT_COUNT)) {
//		return -1;
//	}
//	return (nBedNoFromPort - 1);
}

static DWORD WINAPI ThreadFuncListen(LPVOID lpParam)
{
	CFMSocketThread* ps = (CFMSocketThread*)lpParam;

	int nRet;
	BOOL bRun = TRUE;
	WSANETWORKEVENTS WSAEvents;
	HANDLE events[2] = {ps->m_hShutdownEvent, ps->m_hAcceptEvent};
	while(bRun) {

		nRet = WSAWaitForMultipleEvents(2, events, FALSE, SOCKET_WAIT_TIMEOUT, FALSE);
		if (nRet != 258) {//258是超时
			CString info;
			info.Format(_T("ThreadFuncListen被触发，事件：%d\n"), nRet);
			theApp.WriteLog(info);
		}
		switch (nRet) {
		case WAIT_FAILED:
		case WAIT_OBJECT_0:
			ResetEvent(ps->m_hShutdownEvent);
			bRun = FALSE;
			break;

		case (WAIT_OBJECT_0 + 1):
			ResetEvent(ps->m_hAcceptEvent);
			WSAEnumNetworkEvents(ps->m_sListenSocket, ps->m_hAcceptEvent, &WSAEvents);
			if ((WSAEvents.lNetworkEvents & FD_ACCEPT) && (0 == WSAEvents.iErrorCode[FD_ACCEPT_BIT])){
				AcceptConnection(ps);
			}
			break;

		case WSA_WAIT_TIMEOUT:
		default:
			break;
		}
	}
	return 0;
}

//处理新的床边机连接
static void AcceptConnection(CFMSocketThread* ps)
{
	sockaddr_in ClientAddress;
	int nAddrLen = sizeof(ClientAddress);

	//接收远程连接
	SOCKET s = accept(ps->m_sListenSocket, (sockaddr*)&ClientAddress, &nAddrLen);
	if (INVALID_SOCKET == s) {
		CString info;
		info.Format(_T("Accept 远程连接出错: %ld\n"), WSAGetLastError());
		OutputDebugString(info);
		theApp.WriteLog(_T("FMSocketThread.cpp文件static AcceptConnection()函数\n"));
		theApp.WriteLog(info);
		return;
	}

	int nBedIndex = GetBedIndex(&ClientAddress);
	if (nBedIndex < 0 || nBedIndex >= CLIENT_COUNT) {
		CString info;
		info.Format(_T("远程连接的床号不对: %d\n"), nBedIndex+1);
		OutputDebugString(info);
		theApp.WriteLog(_T("FMSocketThread.cpp文件static AcceptConnection()函数\n"));
		theApp.WriteLog(info);
		return;
	}

	//创建新的远程单元来管理连接
	CFMSocketUnit* pu = ps->GetUnit(nBedIndex);
	HANDLE hTemp = CreateIoCompletionPort((HANDLE)s, ps->m_hIOCompletionPort, (DWORD)pu, 0);
	if (NULL == hTemp) {
		CString info;
		info.Format(_T("运行CreateIoCompletionPort()出错\n"));
		OutputDebugString(info);
		theApp.WriteLog(_T("FMSocketThread.cpp文件static AcceptConnection()函数\n"));
		theApp.WriteLog(info);
		delete pu;
		return;
	}
	else {
		pu->SetSocket(s);
		ps->SetSocketUnit(nBedIndex, TRUE);
		CString info;
		info.Format(_T("成功接收socket，床号: %d\n"), nBedIndex+1);
		OutputDebugString(info);
		pu->OnConnect();
		pu->ResetRecvWSABUF();
		pu->ResetRecvOverlap();
		//启动接收
		info.Format(_T("启动 Recv，床号: %d\n"), nBedIndex+1);
		OutputDebugString(info);
		if (!SocketRecv(pu)) {
			CString info;
			info.Format(_T("初始化端口错误，床号: %d\n"), nBedIndex+1);
			OutputDebugString(info);
			theApp.WriteLog(_T("FMSocketThread.cpp文件static AcceptConnection()函数\n"));
			theApp.WriteLog(info);
			ps->SetSocketUnit(nBedIndex, FALSE);
		}
	}
}

static DWORD WINAPI ThreadFuncComplete(LPVOID lpParam)
{
	CFMSocketThread* ps = (CFMSocketThread*)lpParam;

	DWORD dwBytesTransfered = 0;
	void* lpCompletionKey = NULL;
	OVERLAPPED* lpOverlapped = NULL;
	CFMSocketUnit* pu = NULL;

	int nReserveLen;
	WSABUF* pbuf;

	//Worker thread will be around to process requests, until a Shutdown event is not Signaled.
	while (WAIT_OBJECT_0 != WaitForSingleObject(ps->m_hShutdownEvent, 0)) {

		ResetEvent(ps->m_hShutdownEvent);

		BOOL bReturn = GetQueuedCompletionStatus(
			ps->m_hIOCompletionPort,
			(LPDWORD)&dwBytesTransfered,
			(LPDWORD)&lpCompletionKey,
			&lpOverlapped,
			INFINITE);

		if (NULL == lpCompletionKey) {
			//We are shutting down
			break;
		}

		//Get the context
		pu = (CFMSocketUnit *)lpCompletionKey;
		CString info;
		info.Format(_T("GetQueuedCompletionStatus，床号: %d；dwBytesTransfered = %d\n"), pu->GetIndex()+1, dwBytesTransfered);
		OutputDebugString(info);
		info.Format(_T("GetQueuedCompletionStatus: %s\n"), (pu->OpSend() ? _T("发送") : _T("接收")));
		OutputDebugString(info);

		if ((! bReturn) || (bReturn && (0 == dwBytesTransfered)) ) {
			CString info;
			info.Format(_T("！！！完成端口出错：bReturn = %d, dwBytesTransfered = %d\n"), bReturn, dwBytesTransfered);
			OutputDebugString(info);
			theApp.WriteLog(_T("CFMSocketThread.cpp文件ThreadFuncComplete()函数\n"));
			theApp.WriteLog(info);
			ps->SetSocketUnit(pu->GetIndex(), FALSE);
			continue;
		}

		if (pu->OpSend()) {
			pu->AddSendLen(dwBytesTransfered);
			nReserveLen = pu->GetSendTotal() - pu->GetSendLen();

			//继续发送
			if (nReserveLen > 0) {
				pbuf = pu->GetSendWSABUF();
				pbuf->buf += pu->GetSendLen();
				pbuf->len = nReserveLen;

				info.Format(_T("继续发送，床号: %d\n"), pu->GetIndex()+1);
				OutputDebugString(info);
				if (!SocketSend(pu)) {
					ps->SetSocketUnit(pu->GetIndex(), FALSE);
				}
			}
			else {
				//发送完毕
				info.Format(_T("发送完毕，床号: %d\n"), pu->GetIndex()+1);
				OutputDebugString(info);
				pu->OnSend();
				pu->ResetSendWSABUF();
				pu->ResetSendOverlap();
				pu->ClrOpSend();
			}
		}
		else {
			pu->SetRecvLen(dwBytesTransfered);
			pu->OnReceive();
			pu->ResetRecvWSABUF();
			pu->ResetRecvOverlap();

			//继续接收
			info.Format(_T("启动接收，床号: %d\n"), pu->GetIndex()+1);
			OutputDebugString(info);
			if (! SocketRecv(pu)) {
				ps->SetSocketUnit(pu->GetIndex(), FALSE);
			}
		}
	} // while

	return 0;
}

static BOOL SocketSend(CFMSocketUnit* pu)
{
	pu->SetOpSend();
	DWORD dwBytes = 0;
	DWORD dwFlags = 0;
	int nRet = WSASend(pu->GetSocket(), pu->GetSendWSABUF(), 1, 
		(LPDWORD)&dwBytes, dwFlags, pu->GetSendOverlap(), NULL);

	if (SOCKET_ERROR == nRet) {
		int err = WSAGetLastError();
		if (WSA_IO_PENDING != err) {
			CString info;
			info.Format(_T("\n！！！发送出错：%d\n"), err);
			OutputDebugString(info);
			theApp.WriteLog(_T("FMSocketThread.cpp文件static SocketSend()函数\n"));
			theApp.WriteLog(info);
			return FALSE;
		}
	}
	return TRUE;
}

static BOOL SocketRecv(CFMSocketUnit* pu)
{
	DWORD dwBytes = 0;
	DWORD dwFlags = 0;
	int nRet = WSARecv(pu->GetSocket(), pu->GetRecvWSABUF(), 1,
		(LPDWORD)&dwBytes, (LPDWORD)&dwFlags, pu->GetRecvOverlap(), NULL);

	if (SOCKET_ERROR == nRet) {
		int err = WSAGetLastError();
		if (WSA_IO_PENDING != err) {
			CString info;
			info.Format(_T("\n！！！接收出错：%d\n"), err);
			OutputDebugString(info);
			theApp.WriteLog(_T("FMSocketThread.cpp文件static SocketRecv()函数\n"));
			theApp.WriteLog(info);
			return FALSE;
		}
	}
	return TRUE;
}

