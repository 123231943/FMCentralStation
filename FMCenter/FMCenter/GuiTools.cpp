#include "stdafx.h"

void DrawLine(CDC* pDC, COLORREF cr, int x, int y1, int y2)
{
	int y;
	if (y1 > y2) {
		for (y = y2+1; y <= y1; y ++) {
			pDC->SetPixel(x, y, cr);
		}
	}
	else if (y1 < y2){
		for (y = y1; y < y2; y ++) {
			pDC->SetPixel(x, y, cr);
		}
	}
	else {
		pDC->SetPixel(x, y2, cr);
	}
	pDC->SetPixel(x+1, y2, cr);
}

void DrawLineAA(CDC* pDC, COLORREF cr, int x, int y1, int y2)
{
	int min_y = min(y1, y2);
	int max_y = max(y1, y2);
	int y;
	COLORREF cr1, cr2;
	if (y1 == y2) {
		pDC->SetPixel(x,   y1, cr);
		pDC->SetPixel(x+1, y1, cr);
		return;
	}
	for (y = min_y; y <= max_y; y ++) {
		cr1 = MixColor(cr, COLOR_WAVE_BK, abs(y-y1)+1, abs(y-y2));
		cr2 = MixColor(COLOR_WAVE_BK, cr, abs(y-y1), abs(y-y2)+1);
		pDC->SetPixel(x,   y, cr2);
		pDC->SetPixel(x+1, y, cr1);
	}
}

COLORREF MixColor(COLORREF cr1, COLORREF cr2, int scale1, int scale2)
{
	int R, G, B;
	int denominator = scale1 + scale2;
	R = (GetRValue(cr1) * scale1 + GetRValue(cr2) * scale2) / (denominator);
	G = (GetGValue(cr1) * scale1 + GetGValue(cr2) * scale2) / (denominator);
	B = (GetBValue(cr1) * scale1 + GetBValue(cr2) * scale2) / (denominator);
	return RGB(R, G, B);
}

void DrawECGWave(CDC* pdc, COLORREF cr, int x, int y, BYTE* data, int limitW, int limitH)
{
	int i;
	int count = min(ECG_WAVE_DATA_PER_SECOND, limitW);
	for (i = 0; i < count; i ++) {
		int v1 = min(limitH, max(-limitH, ECG_MID_VALUE - data[i]));
		int v2 = min(limitH, max(-limitH, ECG_MID_VALUE - data[i+1]));
		DrawLineAA(pdc, cr,	x + i, y + v1, y + v2);
	}
}

void DrawECGWave50(CDC* pdc, COLORREF cr, int x, int y, BYTE* data, int limitW, int limitH)
{
	int mid = ECG_MID_VALUE / 2;
	int i;
	int count = min(ECG_WAVE_DATA_PER_SECOND / 2, limitW);
	BYTE buf[ECG_WAVE_DATA_PER_SECOND / 2 + 1];
	CompressData(buf, data, ECG_WAVE_DATA_PER_SECOND / 2 + 1, ECG_WAVE_DATA_PER_SECOND + 1, 0.5f);
	for (i = 0; i < count; i ++) {
		int v1 = min(limitH, max(-limitH, mid - buf[i]));
		int v2 = min(limitH, max(-limitH, mid - buf[i+1]));
		DrawLineAA(pdc, cr,	x + i, y + v1, y + v2);
	}
}

void DrawECGWave20(CDC* pdc, COLORREF cr, int x, int y, BYTE* data, int limitW, int limitH)
{
	int mid = ECG_MID_VALUE / 5;
	int i;
	int count = min(ECG_WAVE_DATA_PER_SECOND / 5, limitW);
	BYTE buf[ECG_WAVE_DATA_PER_SECOND / 5 + 1];
	CompressData(buf, data, ECG_WAVE_DATA_PER_SECOND / 5 + 1, ECG_WAVE_DATA_PER_SECOND + 1, 0.2f);
	for (i = 0; i < count; i ++) {
		int v1 = min(limitH, max(-limitH, mid - buf[i]));
		int v2 = min(limitH, max(-limitH, mid - buf[i+1]));
		DrawLineAA(pdc, cr,	x + i, y + v1, y + v2);
	}
}

void DrawECGWave10(CDC* pdc, COLORREF cr, int x, int y, BYTE* data, int limitW, int limitH)
{
	int mid = ECG_MID_VALUE / 10;
	int i;
	int count = min(ECG_WAVE_DATA_PER_SECOND / 10, limitW);
	BYTE buf[ECG_WAVE_DATA_PER_SECOND / 10 + 1];
	CompressData(buf, data, ECG_WAVE_DATA_PER_SECOND / 10 + 1, ECG_WAVE_DATA_PER_SECOND + 1, 0.1f);
	for (i = 0; i < count; i ++) {
		int v1 = min(limitH, max(-limitH, mid - buf[i]));
		int v2 = min(limitH, max(-limitH, mid - buf[i+1]));
		DrawLineAA(pdc, cr,	x + i, y + v1, y + v2);
	}
}

void DrawFMMark(CDC* pdc, COLORREF cr, int x, int y)
{
	const int nMarkWidth = 2;
	const int nArrawSize = 14;
	int i, j;
	for (i = 0; i < nArrawSize; i ++) {
		for (j = nMarkWidth - i / 2; j <= nMarkWidth; j ++) {
			pdc->SetPixel(x + j, y + i + 1, cr);
		}
	}
	CBrush br(cr);
	CRect rc(x, y + nArrawSize + 1, x + nMarkWidth + 1, y + 50);
	pdc->FillRect(&rc, &br);
}

void DrawTOCOResetMark(CDC* pdc, COLORREF cr, int x, int y)
{
	const int nMarkWidth = 2;
	const int nArrawSize = 7;
	int i, j;
	for (i = 0; i < nArrawSize; i ++) {
		for (j = nMarkWidth - i; j <= i + nMarkWidth; j ++) {
			pdc->SetPixel(x + j, y + i + 1, cr);
			pdc->SetPixel(x + j, y - i - 1, cr);
		}
	}
}

void DrawTimeMark(CDC* pdc, COLORREF cr, CTime& tm, int x, int y, int h)
{
	const int fontW = 9;
	const int fontH = 16;
	int i;
	for (i = 0; i < h; i ++) {
		pdc->SetPixel(x, y + i + 1, cr);
	}
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(cr);
	pdc->TextOut(x + 1, y + (h - fontH) / 2, tm.Format(_T("%H:%M")));
}