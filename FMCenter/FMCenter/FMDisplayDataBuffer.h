#pragma once

#define DISPLAY_DATA_FHR_BUFFER_SIZE   (3600)  //1秒钟1个单元，保存足够胎心曲线1屏幕的数据（1小时）

#define DISPLAY_DATA_BUFFER_SIZE       (10)
#define DISPLAY_ECG_UNIT_SIZE          (ECG_WAVE_DATA_PER_SECOND  +1)   //每单元增加一个值，目的是画线时可以跟前面接起来
#define DISPLAY_SPO2_UNIT_SIZE         (SPO2_WAVE_DATA_PER_SECOND +1)   //每单元增加一个值，目的是画线时可以跟前面接起来
#define DISPLAY_RESP_UNIT_SIZE         (RESP_WAVE_DATA_PER_SECOND +1)   //每单元增加一个值，目的是画线时可以跟前面接起来
#define DISPLAY_FHR_UNIT_SIZE          (3)     //每单元增加一个值，目的是画线时可以跟前面接起来
#define DISPLAY_MARK_UNIT_SIZE         (1)

#define WAVE_DATA_VALID                (0)
#define WAVE_DATA_BEFORE_START         (-1)
#define WAVE_DATA_AFTER_END            (-2)

typedef struct _DISPLAY_FLAG {
	char flag_type;
	BYTE baseline;
} DISPLAY_FLAG;

class CFHRAutodiagnostics;
class CFMDisplayDataBuffer
{
public:
	CFMDisplayDataBuffer(void);
	virtual ~CFMDisplayDataBuffer(void);

private:
	CFHRAutodiagnostics* m_pAuto; //自动诊断模块
	CCriticalSection m_csBufLock;

	//缓冲区的初始化时间
	int m_nTimeSpan;    //从复位到现在的时间间隔，每收到一个数据，此数值加一
	int m_nBufferLen;   //有效数据缓冲的长度，以单元为单位
	int m_nBufferStart; //记录起始位置
	int m_nFHRBufferLen;
	int m_nFHRBufferStart;
	//DisplayDataBuffer是一个双缓冲，它保持两倍的数据长度，
	//当数据填满之后，从头开始填充

	//数值类型数据缓冲
	DISPLAY_DATA* m_arNUM_Buf;

	//心电曲线缓冲I（每秒钟250个）
	BYTE* m_arECG1_Buf;

	//心电曲线缓冲II（每秒钟250个）
	BYTE* m_arECG2_Buf;

	//心电曲线缓冲III（每秒钟250个）
	BYTE* m_arECGv_Buf;

	//SPO2曲线（每秒钟125个）
	BYTE* m_arSPO2_Buf;

	//呼吸波曲线（每秒钟125个）
	BYTE* m_arRESP_Buf;

	//FHR1曲线缓冲（每秒钟2个）
	WORD* m_arFHR1_Buf;

	//FHR2曲线缓冲（每秒钟2个）
	WORD* m_arFHR2_Buf;

	//TOCO曲线缓冲（每秒钟2个）
	WORD* m_arTOCO_Buf;

	//胎动标记缓冲（每秒钟2个）
	BYTE* m_arMARK_Buf;

	//宫缩归零标记（每秒钟2个）
	BYTE* m_arTOCOReset_Buf;

	//诊断标记缓冲（每秒钟2个）
	DISPLAY_FLAG* m_arDiagno1Buf;
	DISPLAY_FLAG* m_arDiagno2Buf;

private:
	int GetReadIndex(int nTimeSpan, int* pIndex);
	int GetFHRReadIndex(int nTimeSpan, int* pIndex);

public:
	void ResetBuffer(void);
	void Add(CLIENT_DATA& cd);
	inline int GetTimeSpan() { return m_nTimeSpan; };
	DISPLAY_DATA* GetNumbData(int nTimeSpan);
	DISPLAY_DATA* GetLatestNumbData();
	void Autodiagnostics();
	int GetWaveData(int nTimeSpan,
		BYTE** ppECG1, BYTE** ppECG2, BYTE** ppECGv, BYTE** ppSPO2, BYTE** ppRESP);
	int GetFHRWaveData(int nTimeSpan,
		WORD** ppFHR1, WORD** ppFHR2, WORD** ppTOCO, BYTE** ppMARK, BYTE** ppTOCOReset, DISPLAY_FLAG** ppDiagno1, DISPLAY_FLAG** ppDiagno2);
};

