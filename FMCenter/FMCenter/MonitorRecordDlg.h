#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "afxdtctl.h"


// CMonitorRecordDlg 对话框

class CMonitorRecordDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMonitorRecordDlg)

public:
	CMonitorRecordDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CMonitorRecordDlg();

// 对话框数据
	enum { IDD = IDD_MONITOR_RECORD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	DECLARE_MESSAGE_MAP()


private:
	CDatabase& m_db;
	CCriticalSection& m_dbLock;
	int m_nRecordCount;
	int m_nPageStartIndex;
	int m_nListValidCount;
	int m_nListCurrentIndex;

	CString m_sSearchMedicalNum;
	CString m_sSearchPatientName;
	CString m_sSearchPatientType;
	CString m_sSearchGender;
	CString m_sSearchPhoneNumber;
	CString m_sSearchClinic;
	CString m_sSearchDoctor;
	CString m_sSearchBedNum;
	CString m_sSearchMonitorDate;
	CString m_sSearchMonitorNum;

private:
	void ClearSearchString();
	void InitRecordCounter(CString& sWhereStatement);
	void MakeWhereStatement(CString& sWhereStatement);
	void MakeQueryString(CString& sql, CString& sWhereStatement);
	void MonitorRecordSetMedicalNum(CString& sMRID, CString& sMedNum);
	void MonitorRecordSetPregnantWeek(CString& sMRID, CString& sPregnantWeek);
	void DeleteRecordFiles(CString mrid);
	BOOL MoreThan24Hour(CString mrid);
	void ShowDataPreviewDlg(int type);
	
public:
	void UpdateRecordList(BOOL bGotoLastPage = FALSE);
	void UpdateDetialPanel(CString& sMRID);
	void IncreaseRecordCounter() { m_nRecordCount ++; };
	void DecreaseRecordCounter() { if (m_nRecordCount > 0) m_nRecordCount --; };
	void SetNaviButtonState();

public:
	CListCtrl m_lstRecord;
	CEdit m_edtMedicalNum;
	CEdit m_edtPatientName;
	CEdit m_edtPatientType;
	CEdit m_edtGender;
	CEdit m_edtPhoneNumber;
	CEdit m_edtAdmissionDate;
	CEdit m_edtClinic;
	CEdit m_edtDoctor;
	CEdit m_edtBedNum;
	CEdit m_edtMonitorDate;
	CEdit m_edtMonitorNum;
	CEdit m_edtAdminName;
	CEdit m_edtPageInfo;
	CButton m_btnSelectMedicalNum;
	CButton m_btnFirst;
	CButton m_btnPrev;
	CButton m_btnNext;
	CButton m_btnLast;
	CButton m_btnDel;
	CButton m_btnECGWaveform;
	CButton m_btnTendWaveform;
	CButton m_btnTendTable;
	CButton m_btnFHRWaveform;
	CEdit m_edtSearchMedicalNum;
	CEdit m_edtSearchPatientName;
	CComboBox m_cmbSearchPatientType;
	CComboBox m_cmbSearchGender;
	CEdit m_edtSearchPhoneNumber;
	CEdit m_edtSearchClinic;
	CEdit m_edtSearchDoctor;
	CComboBox m_cmbSearchBedNum;
	CDateTimeCtrl m_datSearchMonitorDate;
	CEdit m_edtSearchMonitorNum;
	CEdit m_edtPregnantWeek;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnSearchAll();
	afx_msg void OnBnClickedBtnSelectMedicalNum();
	afx_msg void OnBnClickedBtnFirst();
	afx_msg void OnBnClickedBtnPrev();
	afx_msg void OnBnClickedBtnNext();
	afx_msg void OnBnClickedBtnLast();
	afx_msg void OnBnClickedBtnDelete();
	afx_msg void OnBnClickedBtnSearchByMedicalNum();
	afx_msg void OnBnClickedBtnSearchByPatientName();
	afx_msg void OnBnClickedBtnSearchByPatientType();
	afx_msg void OnBnClickedBtnSearchByGender();
	afx_msg void OnBnClickedBtnSearchByPhoneNumber();
	afx_msg void OnBnClickedBtnSearchByClinic();
	afx_msg void OnBnClickedBtnSearchByDoctor();
	afx_msg void OnBnClickedBtnSearchByBedNum();
	afx_msg void OnBnClickedBtnSearchByMonitorTime();
	afx_msg void OnBnClickedBtnSearchByMonitorNum();
	afx_msg void OnBnClickedBtnEcgWaveform();
	afx_msg void OnBnClickedBtnTendWaveform();
	afx_msg void OnBnClickedBtnTendTable();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnItemclickLstRecord(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnListUnconnectedItems();
	afx_msg void OnBnClickedBtnFhrWaveform();
	afx_msg void OnBnClickedBtnSavePregnantWeek();
};
