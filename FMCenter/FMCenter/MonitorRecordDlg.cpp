// MonitorRecordDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "MonitorRecordDlg.h"
#include "afxdialogex.h"
#include "MedicalRecordListDlg.h"
#include "FMRecordUnit.h"
#include "ReviewDlg.h"

// CMonitorRecordDlg 对话框
#define MONITOR_LIST_LINAGE                      (40) //定义每页显示的行数
#define MONITOR_LIST_COLUMN_COUNT                (12)
#define MONITOR_LIST_INDEX_OF_MEDICAL_NUM        (0)
#define MONITOR_LIST_INDEX_OF_PATIENT_NAME       (1)
#define MONITOR_LIST_INDEX_OF_PATIENT_AGE        (2)
#define MONITOR_LIST_INDEX_OF_PATIENT_TYPE       (3)
#define MONITOR_LIST_INDEX_OF_GENDER             (4)
#define MONITOR_LIST_INDEX_OF_BED_NUM            (8)
#define MONITOR_LIST_INDEX_OF_PREG_WEEK          (9)
#define MONITOR_LIST_INDEX_OF_MONITOR_DATE       (10)
#define MONITOR_LIST_INDEX_OF_MRID               (11)


//column上面显示的文字
static _TCHAR* l_arColumnLabel[MONITOR_LIST_COLUMN_COUNT] =
{
	_T("病历号"),
	_T("姓名"),
	_T("年龄"),
	_T("类型"),
	_T("性别"),
	_T("科室"),
	_T("主治医师"),
	_T("住院时间"),
	_T("床号"),
	_T("孕周"),
	_T("监护日期"),
	_T("监护记录号")
};
//column上文字的显示方式（靠左）
static int l_arColumnFmt[MONITOR_LIST_COLUMN_COUNT] = 
{
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT
};
//column的宽度
static int l_arColumnWidth[MONITOR_LIST_COLUMN_COUNT] = 
{
	90,
	90,
	40,
	50,
	40,
	80,
	80,
	120,
	40,
	70,
	120,
	80
};


IMPLEMENT_DYNAMIC(CMonitorRecordDlg, CDialogEx)

CMonitorRecordDlg::CMonitorRecordDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMonitorRecordDlg::IDD, pParent)
	, m_db(theApp.m_db)
	, m_dbLock(theApp.m_csDBLock)
{
}

CMonitorRecordDlg::~CMonitorRecordDlg()
{
}

void CMonitorRecordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_MEDICAL_NUM, m_edtMedicalNum);
	DDX_Control(pDX, IDC_EDT_NAME, m_edtPatientName);
	DDX_Control(pDX, IDC_EDT_TYPE, m_edtPatientType);
	DDX_Control(pDX, IDC_EDIT_GENDER, m_edtGender);
	DDX_Control(pDX, IDC_EDT_PHONE_NUMBER, m_edtPhoneNumber);
	DDX_Control(pDX, IDC_EDT_ADMISSION, m_edtAdmissionDate);
	DDX_Control(pDX, IDC_EDIT_CLINIC, m_edtClinic);
	DDX_Control(pDX, IDC_EDT_DOCTOR, m_edtDoctor);
	DDX_Control(pDX, IDC_EDIT_BED_NUM, m_edtBedNum);
	DDX_Control(pDX, IDC_EDT_MONITOR_DATE, m_edtMonitorDate);
	DDX_Control(pDX, IDC_EDT_MONITOR_NUM, m_edtMonitorNum);
	DDX_Control(pDX, IDC_EDT_ADMIN_NAME, m_edtAdminName);
	DDX_Control(pDX, IDC_EDT_PAGE_INFO2, m_edtPageInfo);
	DDX_Control(pDX, IDC_BTN_SELECT_MEDICAL_NUM, m_btnSelectMedicalNum);
	DDX_Control(pDX, IDC_BTN_FIRST2, m_btnFirst);
	DDX_Control(pDX, IDC_BTN_PREV, m_btnPrev);
	DDX_Control(pDX, IDC_BTN_NEXT, m_btnNext);
	DDX_Control(pDX, IDC_BTN_LAST, m_btnLast);
	DDX_Control(pDX, IDC_BTN_DELETE2, m_btnDel);
	DDX_Control(pDX, IDC_EDT_SEARCH_BY_MEDICAL_NUM, m_edtSearchMedicalNum);
	DDX_Control(pDX, IDC_EDT_SEARCH_BY_PATIENT_NAME, m_edtSearchPatientName);
	DDX_Control(pDX, IDC_CMB_SEARCH_BY_PATIENT_TYPE, m_cmbSearchPatientType);
	DDX_Control(pDX, IDC_CMB_SEARCH_BY_GENDER, m_cmbSearchGender);
	DDX_Control(pDX, IDC_EDT_SEARCH_BY_PHONE_NUMBER, m_edtSearchPhoneNumber);
	DDX_Control(pDX, IDC_CMB_SEARCH_BY_BED_NUM, m_cmbSearchBedNum);
	DDX_Control(pDX, IDC_DATE_SEARCH_BY_MONITOR_TIME, m_datSearchMonitorDate);
	DDX_Control(pDX, IDC_EDT_SEARCH_BY_MONITOR_NUM, m_edtSearchMonitorNum);
	DDX_Control(pDX, IDC_LST_RECORD, m_lstRecord);
	DDX_Control(pDX, IDC_EDT_SEARCH_BY_CLINIC, m_edtSearchClinic);
	DDX_Control(pDX, IDC_EDT_SEARCH_BY_DOCTOR, m_edtSearchDoctor);
	DDX_Control(pDX, IDC_BTN_ECG_WAVEFORM, m_btnECGWaveform);
	DDX_Control(pDX, IDC_BTN_TEND_WAVEFORM, m_btnTendWaveform);
	DDX_Control(pDX, IDC_BTN_TEND_TABLE, m_btnTendTable);
	DDX_Control(pDX, IDC_BTN_FHR_WAVEFORM, m_btnFHRWaveform);
	DDX_Control(pDX, IDC_EDT_PREGNANT_WEEK, m_edtPregnantWeek);
}

BEGIN_MESSAGE_MAP(CMonitorRecordDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_SEARCH_ALL, &CMonitorRecordDlg::OnBnClickedBtnSearchAll)
	ON_BN_CLICKED(IDC_BTN_SELECT_MEDICAL_NUM, &CMonitorRecordDlg::OnBnClickedBtnSelectMedicalNum)
	ON_BN_CLICKED(IDC_BTN_FIRST2, &CMonitorRecordDlg::OnBnClickedBtnFirst)
	ON_BN_CLICKED(IDC_BTN_PREV, &CMonitorRecordDlg::OnBnClickedBtnPrev)
	ON_BN_CLICKED(IDC_BTN_NEXT, &CMonitorRecordDlg::OnBnClickedBtnNext)
	ON_BN_CLICKED(IDC_BTN_LAST, &CMonitorRecordDlg::OnBnClickedBtnLast)
	ON_BN_CLICKED(IDC_BTN_DELETE2, &CMonitorRecordDlg::OnBnClickedBtnDelete)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_MEDICAL_NUM, &CMonitorRecordDlg::OnBnClickedBtnSearchByMedicalNum)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_PATIENT_NAME, &CMonitorRecordDlg::OnBnClickedBtnSearchByPatientName)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_PATIENT_TYPE, &CMonitorRecordDlg::OnBnClickedBtnSearchByPatientType)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_GENDER, &CMonitorRecordDlg::OnBnClickedBtnSearchByGender)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_PHONE_NUMBER, &CMonitorRecordDlg::OnBnClickedBtnSearchByPhoneNumber)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_CLINIC, &CMonitorRecordDlg::OnBnClickedBtnSearchByClinic)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_DOCTOR, &CMonitorRecordDlg::OnBnClickedBtnSearchByDoctor)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_BED_NUM, &CMonitorRecordDlg::OnBnClickedBtnSearchByBedNum)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_MONITOR_TIME, &CMonitorRecordDlg::OnBnClickedBtnSearchByMonitorTime)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_MONITOR_NUM, &CMonitorRecordDlg::OnBnClickedBtnSearchByMonitorNum)
	ON_BN_CLICKED(IDC_BTN_ECG_WAVEFORM, &CMonitorRecordDlg::OnBnClickedBtnEcgWaveform)
	ON_BN_CLICKED(IDC_BTN_TEND_WAVEFORM, &CMonitorRecordDlg::OnBnClickedBtnTendWaveform)
	ON_BN_CLICKED(IDC_BTN_TEND_TABLE, &CMonitorRecordDlg::OnBnClickedBtnTendTable)
	ON_BN_CLICKED(IDCANCEL, &CMonitorRecordDlg::OnBnClickedCancel)
	ON_NOTIFY(NM_CLICK, IDC_LST_RECORD, &CMonitorRecordDlg::OnItemclickLstRecord)
	ON_BN_CLICKED(IDC_BTN_LIST_UNCONNECTED_ITEMS, &CMonitorRecordDlg::OnBnClickedBtnListUnconnectedItems)
	ON_BN_CLICKED(IDC_BTN_FHR_WAVEFORM, &CMonitorRecordDlg::OnBnClickedBtnFhrWaveform)
	ON_BN_CLICKED(IDC_BTN_SAVE_PREGNANT_WEEK, &CMonitorRecordDlg::OnBnClickedBtnSavePregnantWeek)
END_MESSAGE_MAP()

void CMonitorRecordDlg::InitRecordCounter(CString& sWhereStatement)
{
	m_nRecordCount = 0;
	CRecordset rs(&m_db);
	CString sql;

	if (sWhereStatement.IsEmpty()) {
		sql.Format(
			_T("SELECT COUNT(*) AS count FROM monitor_record"));
	}
	else {
		sql.Format(
			_T("SELECT COUNT(*) AS count FROM monitor_record WHERE %s"),
			sWhereStatement);
	}

	OutputDebugString(sql + _T("\n"));

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::InitRecordCounter第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}

	// 读出结果
	try {
		rs.MoveFirst();
		if (!rs.IsEOF()) {
			CDBVariant var;
			rs.GetFieldValue(_T("count"), var);
			m_nRecordCount = var.m_lVal;
		}
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::InitRecordCounter第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}
}

void CMonitorRecordDlg::ClearSearchString()
{
	m_sSearchMedicalNum = _T("");
	m_sSearchPatientName = _T("");
	m_sSearchPatientType = _T("");
	m_sSearchGender = _T("");
	m_sSearchPhoneNumber = _T("");
	m_sSearchClinic = _T("");
	m_sSearchDoctor = _T("");
	m_sSearchBedNum = _T("");
	m_sSearchMonitorDate = _T("");
	m_sSearchMonitorNum = _T("");
}
	
void CMonitorRecordDlg::MakeWhereStatement(CString& sWhereStatement)
{
	if (! m_sSearchMedicalNum.IsEmpty()) {
		sWhereStatement += m_sSearchMedicalNum;
	}
	else if (! (m_sSearchPatientName.IsEmpty()
		&& m_sSearchPatientType.IsEmpty()
		&& m_sSearchGender.IsEmpty()
		&& m_sSearchPhoneNumber.IsEmpty()
		&& m_sSearchClinic.IsEmpty()
		&& m_sSearchDoctor.IsEmpty())) {
		sWhereStatement += _T(" med_num IN (SELECT medical_num FROM medical_record WHERE");

		CString sSubWhere = _T("");
		if (! m_sSearchPatientName.IsEmpty()) {
			sSubWhere += _T(" ");
			sSubWhere += m_sSearchPatientName;
		}

		if (! m_sSearchPatientType.IsEmpty()) {
			if (! sSubWhere.IsEmpty()) {
				sSubWhere += _T(" AND");
			}
			sSubWhere += _T(" medical_record.");
			sSubWhere += m_sSearchPatientType;
		}

		if (! m_sSearchGender.IsEmpty()) {
			if (! sSubWhere.IsEmpty()) {
				sSubWhere += _T(" AND");
			}
			sSubWhere += _T(" medical_record.");
			sSubWhere += m_sSearchGender;
		}

		if (! m_sSearchPhoneNumber.IsEmpty()) {
			if (! sSubWhere.IsEmpty()) {
				sSubWhere += _T(" AND");
			}
			sSubWhere += _T(" medical_record.");
			sSubWhere += m_sSearchPhoneNumber;
		}

		if (! m_sSearchClinic.IsEmpty()) {
			if (! sSubWhere.IsEmpty()) {
				sSubWhere += _T(" AND");
			}
			sSubWhere += _T(" medical_record.");
			sSubWhere += m_sSearchClinic;
		}

		if (! m_sSearchDoctor.IsEmpty()) {
			if (! sSubWhere.IsEmpty()) {
				sSubWhere += _T(" AND");
			}
			sSubWhere += _T(" medical_record.");
			sSubWhere += m_sSearchDoctor;
		}

		sWhereStatement += sSubWhere;
		sWhereStatement += _T(") ");
	}

	if (! m_sSearchBedNum.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchBedNum;
	}

	if (! m_sSearchMonitorDate.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchMonitorDate;
	}

	if (! m_sSearchMonitorNum.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchMonitorNum;
	}
}

void CMonitorRecordDlg::MakeQueryString(CString& sql, CString& sWhereStatement)
{
	// 没有有效的查询条件，那么就查询全部记录
	if (sWhereStatement.IsEmpty()) {
		sql.Format(
			_T("SELECT med_num,patient_name,birthday,patient_type,gender,clinic,doctor,admission_date,bed_num,pregnant_week,a.create_date,mrid ")
			_T("FROM monitor_record a LEFT JOIN medical_record b ON a.med_num=b.medical_num ")
			_T("WHERE mrid IN")
			_T("  (SELECT TOP %d mrid FROM ")
			_T("    (SELECT TOP %d mrid FROM monitor_record ORDER BY mrid ASC")
			_T("    ) w ORDER BY w.mrid DESC")
			_T("  ) ORDER BY a.mrid ASC "),
			min(MONITOR_LIST_LINAGE, m_nRecordCount - m_nPageStartIndex),
			m_nPageStartIndex + MONITOR_LIST_LINAGE);
	}
	else {
		sql.Format(
			_T("SELECT med_num,patient_name,birthday,patient_type,gender,clinic,doctor,admission_date,bed_num,pregnant_week,a.create_date,mrid ")
			_T("FROM monitor_record a LEFT JOIN medical_record b ON a.med_num=b.medical_num ")
			_T("WHERE mrid IN")
			_T("  (SELECT TOP %d mrid FROM ")
			_T("    (SELECT TOP %d mrid FROM monitor_record ")
			_T("     WHERE %s ORDER BY mrid ASC")
			_T("    ) w ORDER BY w.mrid DESC")
			_T("  ) ORDER BY a.mrid ASC"),
			min(MONITOR_LIST_LINAGE, m_nRecordCount - m_nPageStartIndex),
			m_nPageStartIndex + MONITOR_LIST_LINAGE,
			sWhereStatement);
	};
}

void CMonitorRecordDlg::UpdateRecordList(BOOL bGotoLastPage)
{
	// 执行查询
	CRecordset rs(&m_db);
	CString sWhereStatement;
	CString sql;

	m_lstRecord.DeleteAllItems();
	m_nListValidCount = 0;
	m_nListCurrentIndex = -1;

	MakeWhereStatement(sWhereStatement);
	InitRecordCounter(sWhereStatement);

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	if (0 == m_nRecordCount) {
		goto fini;//没有记录，也就没必要查询了
	}
	if (bGotoLastPage) {
		m_nPageStartIndex = ((m_nRecordCount - 1) / MONITOR_LIST_LINAGE) * MONITOR_LIST_LINAGE;
	}
	MakeQueryString(sql, sWhereStatement);
	OutputDebugString(sql + _T("\n"));
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::UpdateRecordList第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}

	// 将查询内容写入列表
	m_lstRecord.DeleteAllItems();
	int nCount;
	try {
		while (! rs.IsEOF()) {
			rs.MoveNext();
		}
		nCount = rs.GetRecordCount();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::UpdateRecordList第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		goto fini;
	}

	if (0 == nCount) {
		rs.Close();
		goto fini;
	}

	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::UpdateRecordList第3个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		goto fini;
	}

	int i = 0;
	while (!rs.IsEOF()) {
		
		LVITEM lvi;
		_TCHAR buffer[256];
		lvi.mask = LVIF_TEXT;
		lvi.iItem = i;
		lvi.pszText = buffer;
		lvi.cchTextMax = 255;
		i ++;

		try {
			CString sValue;
			CDBVariant var;

			rs.GetFieldValue(_T("med_num"), sValue);
			lvi.iSubItem = 0;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.InsertItem(&lvi);

			rs.GetFieldValue(_T("patient_name"), sValue);
			lvi.iSubItem = 1;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);

			rs.GetFieldValue(_T("birthday"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				CTime tm((var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day, 0, 0, 0);
				sValue.Format(_T("%d"), GetAge(tm));
			}
			lvi.iSubItem = 2;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);

			rs.GetFieldValue(_T("patient_type"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				sValue = CFMDict::PatientType_itos(var.m_lVal);
			}
			lvi.iSubItem = 3;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);

			rs.GetFieldValue(_T("gender"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				sValue = CFMDict::Gender_itos(var.m_lVal);
			}
			lvi.iSubItem = 4;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);

			rs.GetFieldValue(_T("clinic"), sValue);
			lvi.iSubItem = 5;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);

			rs.GetFieldValue(_T("doctor"), sValue);
			lvi.iSubItem = 6;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);

			rs.GetFieldValue(_T("admission_date"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				sValue.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),
					(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day,
					(var.m_pdate)->hour, (var.m_pdate)->minute, (var.m_pdate)->second);
			}
			lvi.iSubItem = 7;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);

			rs.GetFieldValue(_T("bed_num"), sValue);
			lvi.iSubItem = 8;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);

			rs.GetFieldValue(_T("pregnant_week"), sValue);
			lvi.iSubItem = 9;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);

			rs.GetFieldValue(_T("create_date"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				sValue.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),
					(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day,
					(var.m_pdate)->hour, (var.m_pdate)->minute, (var.m_pdate)->second);
			}
			lvi.iSubItem = 10;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);

			rs.GetFieldValue(_T("mrid"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				sValue.Format(_T("%d"), var.m_lVal);
			}
			lvi.iSubItem = 11;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstRecord.SetItem(&lvi);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::UpdateRecordList第4个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			m_nListValidCount = i;
			goto fini;
		}
		rs.MoveNext();
	}
	//记录列表中有效内容的行数
	m_nListValidCount = i;

	if (rs.IsOpen()) {
		rs.Close();
	}

fini:

	//设置页数和页码
	CString sInfo;
	sInfo.Format(_T("%d条，%d页(%d)"),
		m_nRecordCount, //总条数
		(m_nRecordCount + MONITOR_LIST_LINAGE - 1) / MONITOR_LIST_LINAGE, //计算占用的页数
		m_nPageStartIndex / MONITOR_LIST_LINAGE + 1);//当前页
	m_edtPageInfo.SetWindowText(sInfo);
}

void CMonitorRecordDlg::SetNaviButtonState()
{
	if (-1 == m_nListCurrentIndex) {
		m_btnSelectMedicalNum.EnableWindow(FALSE);
		m_btnDel.EnableWindow(FALSE);
		m_btnECGWaveform.EnableWindow(FALSE);
		m_btnFHRWaveform.EnableWindow(FALSE);
		m_btnTendWaveform.EnableWindow(FALSE);
		m_btnTendTable.EnableWindow(FALSE);
	}
	else {
		m_btnSelectMedicalNum.EnableWindow(TRUE);
		m_btnDel.EnableWindow(TRUE);
		m_btnECGWaveform.EnableWindow(TRUE);
		m_btnFHRWaveform.EnableWindow(TRUE);
		m_btnTendWaveform.EnableWindow(TRUE);
		m_btnTendTable.EnableWindow(TRUE);
	}

	if (m_nRecordCount < MONITOR_LIST_LINAGE) {
		//内容不够一页
		m_btnPrev.EnableWindow(FALSE);
		m_btnFirst.EnableWindow(FALSE);
		m_btnNext.EnableWindow(FALSE);
		m_btnLast.EnableWindow(FALSE);
	}

	if (m_nPageStartIndex > 0) {
		m_btnPrev.EnableWindow(TRUE);
		m_btnFirst.EnableWindow(TRUE);
	}
	else {
		m_btnPrev.EnableWindow(FALSE);
		m_btnFirst.EnableWindow(FALSE);
	}

	if ((m_nPageStartIndex + MONITOR_LIST_LINAGE) < m_nRecordCount) {
		m_btnNext.EnableWindow(TRUE);
		m_btnLast.EnableWindow(TRUE);
	}
	else {
		m_btnNext.EnableWindow(FALSE);
		m_btnLast.EnableWindow(FALSE);
	}
}

BOOL CMonitorRecordDlg::MoreThan24Hour(CString mrid)
{
	CTime tmCreate;
	CTime tmNow = CTime::GetCurrentTime();

	CRecordset rs(&m_db);
	CString sql;

	// 构造SQL语句
	sql.Format(
		_T("SELECT create_date FROM monitor_record ")
		_T("WHERE mrid='%s'"),
		mrid);

	OutputDebugString(sql + _T("\n"));

	CSmartLock lock(m_dbLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::GetRecordFileList第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return FALSE;
	}

	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::GetRecordFileList第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return FALSE;
	}

	while (! rs.IsEOF()) {
		try {
			CDBVariant var;
			rs.GetFieldValue(_T("create_date"), var);
			CTime tm(
				(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day,
				(var.m_pdate)->hour, (var.m_pdate)->minute, (var.m_pdate)->second);

			tmCreate = tm;
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CFMCenterApp::GetRecordFileList第3个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return FALSE;
		}
		rs.MoveNext();
	}

	if (rs.IsOpen()) {
		rs.Close();
	}

	CTimeSpan ts = tmNow - tmCreate;
	int nHours = ts.GetTotalHours();
	return (nHours >= 24);
}

// CMonitorRecordDlg 消息处理程序
void CMonitorRecordDlg::OnBnClickedBtnSearchAll()
{
	ClearSearchString();
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSelectMedicalNum()
{
	if (-1 == m_nListCurrentIndex) {
		return;
	}

	CString sMRID = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_MRID);
	if (MoreThan24Hour(sMRID)) {
		MessageBox(_T("该记录已经超过24小时，您已经不能再修改了！"));
		return;
	}

	CMedicalRecordListDlg dlg(this, TYPE_OP_SELECT);
	if (IDOK == dlg.DoModal()) {
		ClearSearchString();
		CString sMRID = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_MRID);
		CString sMedNum = dlg.GetSelectedMedicalNum();
		MonitorRecordSetMedicalNum(sMRID, sMedNum);
		UpdateRecordList();
		SetNaviButtonState();
	}
}

void CMonitorRecordDlg::MonitorRecordSetMedicalNum(CString& sMRID, CString& sMedNum)
{
	CString sql;
	sql.Format(
		_T("UPDATE monitor_record SET med_num='%s',operator='%s' ")
		_T("WHERE mrid='%s'"),
		sMedNum, theApp.m_sAdminName, sMRID);

	//执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::MonitorRecordSetMedicalNum第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
		return;
	}

	//如果该监护窗口还没有关闭，则更新相对应的窗口信息
	CFMRecordUnit* pu = theApp.FindRecordUnitByMRID(sMRID);
	if (pu) {
		pu->UpdatePatientInfo(sMedNum);
	}
}

void CMonitorRecordDlg::MonitorRecordSetPregnantWeek(CString& sMRID, CString& sPregnantWeek)
{
	CString sql;
	sql.Format(
		_T("UPDATE monitor_record SET pregnant_week='%s',operator='%s' ")
		_T("WHERE mrid='%s'"),
		sPregnantWeek, theApp.m_sAdminName, sMRID);

	//执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::MonitorRecordSetMedicalNum第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
		return;
	}

	//如果该监护窗口还没有关闭，则更新相对应的窗口信息
	CFMRecordUnit* pu = theApp.FindRecordUnitByMRID(sMRID);
	if (pu) {
		pu->SetPregnantWeek(sPregnantWeek);
	}
}

void CMonitorRecordDlg::OnBnClickedBtnFirst()
{
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnPrev()
{
	if (m_nPageStartIndex > 0) {
		m_nPageStartIndex -= MONITOR_LIST_LINAGE;
	}
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnNext()
{
	if ((m_nPageStartIndex + MONITOR_LIST_LINAGE) < m_nRecordCount) {
		m_nPageStartIndex += MONITOR_LIST_LINAGE;
	}
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnLast()
{
	m_nPageStartIndex = ((m_nRecordCount - 1) / MONITOR_LIST_LINAGE) * MONITOR_LIST_LINAGE;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnDelete()
{
	if (-1 == m_nListCurrentIndex) {
		return;
	}

	if (1 != theApp.m_nAdminType) {
		MessageBox(_T("只有 高级管理员 才能删除记录！"), _T("操作提示"), MB_ICONEXCLAMATION | MB_OK);
		return;
	}

	CString sMRID = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_MRID);
	CString sName = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_PATIENT_NAME);
	CString sBedNum = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_BED_NUM);
	CString sTime = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_MONITOR_DATE);

	//首先检查要删除的记录是不是正在监护。不能删除正在监护中的记录
	CFMRecordUnit* pu = theApp.FindRecordUnitByMRID(sMRID);
	if (pu) {
		MessageBox(_T("不能删除正在监护中的记录！"), _T("操作提示"), MB_ICONEXCLAMATION | MB_OK);
		return;
	}

	CString sMsgInfo;
	sMsgInfo.Format(_T("准备删除的记录：\n\n姓名：%s\n床号：%s\n监护时间：%s\n\n您确认要删除该病历吗？"), 
		sName, sBedNum, sTime);
	if (IDOK != MessageBox(sMsgInfo, _T("确认删除操作"), MB_ICONEXCLAMATION | MB_OKCANCEL)) {
		return;
	}

	//删除关联的记录文件
	DeleteRecordFiles(sMRID);

	//删除关联的file_record
	//注意，必须首先删除file_record，否则monitor_record会因为外键关联的原因而无法删除
	{
		CString sql;
		sql.Format(
			_T("DELETE FROM file_record WHERE mrid='%s'"), sMRID);

		//执行SQL语句
		CSmartLock lock(m_dbLock);
		try {
			m_db.ExecuteSQL(sql);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::OnBnClickedBtnDelete第1个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
			return;
		}
	}

	//执行删除monitor_record操作
	{
		CString sql;
		sql.Format(
			_T("DELETE FROM monitor_record WHERE mrid='%s'"), sMRID);

		//执行SQL语句
		CSmartLock lock(m_dbLock);
		try {
			m_db.ExecuteSQL(sql);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::OnBnClickedBtnDelete第2个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
			return;
		}
	}

	//记录条计数减一
	DecreaseRecordCounter();
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSearchByMedicalNum()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchMedicalNum.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchMedicalNum = _T("med_num IS NULL");
	}
	else {
		m_sSearchMedicalNum.Format(_T("med_num LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSearchByPatientName()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchPatientName.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchPatientName = _T("patient_name=''");
	}
	else {
		m_sSearchPatientName.Format(_T("patient_name LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSearchByPatientType()
{
	int nIndex;
	ClearSearchString();
	nIndex = m_cmbSearchPatientType.GetCurSel();
	m_sSearchPatientType.Format(_T("patient_type='%d'"), nIndex);
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSearchByGender()
{
	int nIndex;
	ClearSearchString();
	nIndex = m_cmbSearchGender.GetCurSel();
	m_sSearchGender.Format(_T("gender='%d'"), nIndex);
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSearchByPhoneNumber()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchPhoneNumber.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchPhoneNumber = _T("phone_number=''");
	}
	else {
		m_sSearchPhoneNumber.Format(_T("phone_number LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSearchByClinic()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchClinic.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchClinic = _T("clinic=''");
	}
	else {
		m_sSearchClinic.Format(_T("clinic LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSearchByDoctor()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchDoctor.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchDoctor = _T("doctor=''");
	}
	else {
		m_sSearchDoctor.Format(_T("doctor LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSearchByBedNum()
{
	CString sValue;
	ClearSearchString();
	m_cmbSearchBedNum.GetWindowText(sValue);
	m_sSearchBedNum.Format(_T("bed_num='%s'"), sValue);
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSearchByMonitorTime()
{
	CTime tm;
	CString sTime;
	ClearSearchString();
	m_datSearchMonitorDate.GetTime(tm);
	sTime = tm.Format(_T("%Y-%m-%d %H:%M:%S"));
	m_sSearchMonitorDate.Format(_T("create_date >= '%s'"), sTime);
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnSearchByMonitorNum()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchMonitorNum.GetWindowText(sValue);
	m_sSearchMonitorNum.Format(_T("mrid='%s'"), sValue);
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMonitorRecordDlg::OnBnClickedBtnListUnconnectedItems()
{
	ClearSearchString();
	m_sSearchMedicalNum = _T("med_num IS NULL");
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

BOOL CMonitorRecordDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 初始化LIST的表头
	int i;
	m_lstRecord.RemoveAllGroups();
	m_lstRecord.ModifyStyle(0, LVS_REPORT | LVS_SHOWSELALWAYS, 0);
	m_lstRecord.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	//设定一个用于存取column的结构lvc
	LVCOLUMN lvc;
	//设定存取模式
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	//用InSertColumn函数向窗口中插入柱
	for (i=0 ; i<MONITOR_LIST_COLUMN_COUNT; i++) {
		lvc.iSubItem = i;
		lvc.pszText = l_arColumnLabel[i];
		lvc.cx = l_arColumnWidth[i];
		lvc.fmt = l_arColumnFmt[i];
		m_lstRecord.InsertColumn(i,&lvc);
	}

	// 设置选项
	m_cmbSearchGender.ResetContent();
	for (i=0; i<CFMDict::GetGenderCount(); i++) {
		m_cmbSearchGender.AddString(CFMDict::Gender_itos(i));
	}
	m_cmbSearchPatientType.ResetContent();
	for (i=0; i<CFMDict::GetPatientTypeCount(); i++) {
		m_cmbSearchPatientType.AddString(CFMDict::PatientType_itos(i));
	}
	for (i=1; i<CLIENT_COUNT+1; i++) {
		CString sBedNum;
		sBedNum.Format(_T("%02d"), i);
		m_cmbSearchBedNum.AddString(sBedNum);
	}
	CTime tmNow = CTime::GetCurrentTime();
	m_datSearchMonitorDate.SetTime(&tmNow);
	m_datSearchMonitorDate.SetFormat(_T("yyyy-MM-dd"));

	// 设置输入框长度限制
	m_edtSearchMedicalNum.SetLimitText(24);
	m_edtSearchPatientName.SetLimitText(24);
	m_edtSearchPhoneNumber.SetLimitText(24);
	m_edtSearchClinic.SetLimitText(24);
	m_edtSearchDoctor.SetLimitText(24);
	m_edtSearchMonitorNum.SetLimitText(24);
	m_edtPregnantWeek.SetLimitText(10);

	// 初始查询并设置按钮状态
	m_nPageStartIndex = 0;
	m_nListValidCount = 0;
	UpdateRecordList(TRUE);
	SetNaviButtonState();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CMonitorRecordDlg::OnItemclickLstRecord(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	*pResult = 0;

	// 得到选中的行数
	int nIndex = phdr->iItem;
    if (nIndex < 0 || nIndex >= m_nListValidCount) {
		m_nListCurrentIndex = -1;
		SetNaviButtonState();
        return;
    }
	m_nListCurrentIndex = nIndex;
	SetNaviButtonState();

	CString sMRID = m_lstRecord.GetItemText(nIndex, MONITOR_LIST_INDEX_OF_MRID);
	UpdateDetialPanel(sMRID);
}

void CMonitorRecordDlg::UpdateDetialPanel(CString& sMRID)
{
	//读出medno，并查询它对应的记录集
	CRecordset rs(&m_db);
	CString sql;
	sql.Format(
		_T("SELECT med_num,patient_name,patient_type,gender,phone_number,admission_date,clinic,doctor,bed_num,pregnant_week,a.create_date,mrid,a.operator ")
		_T("FROM monitor_record a LEFT JOIN medical_record b ON a.med_num=b.medical_num ")
		_T("WHERE mrid = '%s'"), sMRID);

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::UpdateDetialPanel第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}

	//将内容填入右侧控件
	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::UpdateDetialPanel第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}
	if (!rs.IsEOF()) {
		try {
			CString sValue;
			CDBVariant var;
			
			rs.GetFieldValue(_T("med_num"), sValue);
			m_edtMedicalNum.SetWindowText(sValue);

			rs.GetFieldValue(_T("patient_name"), sValue);
			m_edtPatientName.SetWindowText(sValue);

			rs.GetFieldValue(_T("patient_type"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				sValue = CFMDict::PatientType_itos(var.m_lVal);
			}
			m_edtPatientType.SetWindowText(sValue);

			rs.GetFieldValue(_T("gender"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				sValue = CFMDict::Gender_itos(var.m_lVal);
			}
			m_edtGender.SetWindowText(sValue);

			rs.GetFieldValue(_T("phone_number"), sValue);
			m_edtPhoneNumber.SetWindowText(sValue);

			rs.GetFieldValue(_T("admission_date"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				sValue.Format(_T("%04d-%02d-%02d"),
					(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day);
			}
			m_edtAdmissionDate.SetWindowText(sValue);

			rs.GetFieldValue(_T("clinic"), sValue);
			m_edtClinic.SetWindowText(sValue);

			rs.GetFieldValue(_T("doctor"), sValue);
			m_edtDoctor.SetWindowText(sValue);

			rs.GetFieldValue(_T("bed_num"), sValue);
			m_edtBedNum.SetWindowText(sValue);

			rs.GetFieldValue(_T("pregnant_week"), sValue);
			m_edtPregnantWeek.SetWindowText(sValue);

			rs.GetFieldValue(_T("create_date"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				sValue.Format(_T("%04d-%02d-%02d"),
					(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day);
			}
			m_edtMonitorDate.SetWindowText(sValue);

			// 超过24小时之后，不能再编辑孕周
			CTime tmNow = CTime::GetCurrentTime();
			CTime tm(
				(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day,
				(var.m_pdate)->hour, (var.m_pdate)->minute, (var.m_pdate)->second);
			CTimeSpan ts = tmNow - tm;
			m_edtPregnantWeek.EnableWindow(ts.GetTotalHours() < 24);

			rs.GetFieldValue(_T("mrid"), var);
			if (DBVT_NULL == var.m_dwType) {
				sValue = _T("");
			}
			else {
				sValue.Format(_T("%d"), var.m_lVal);
			}
			m_edtMonitorNum.SetWindowText(sValue);

			rs.GetFieldValue(_T("operator"), sValue);
			m_edtAdminName.SetWindowText(sValue);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMonitorRecordDlg::UpdateDetialPanel第3个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return;
		}
	}

	if (rs.IsOpen()) {
		rs.Close();
	}
}

void CMonitorRecordDlg::DeleteRecordFiles(CString mrid)
{
	CStringArray sa;
	int i;
	int count = theApp.GetRecordFileList(sa, mrid);
	for (i=0; i<count; i++) {
		//MessageBox(sa[i]);
		DeleteFile(sa[i]);
	}
}

void CMonitorRecordDlg::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}

void CMonitorRecordDlg::OnBnClickedBtnEcgWaveform()
{
	if (-1 == m_nListCurrentIndex) {
		return;
	}
	ShowDataPreviewDlg(REVIEW_ECG_WAVEFORM);
}

void CMonitorRecordDlg::OnBnClickedBtnTendWaveform()
{
	if (-1 == m_nListCurrentIndex) {
		return;
	}
	ShowDataPreviewDlg(REVIEW_TEND_WAVEFORM);
}

void CMonitorRecordDlg::OnBnClickedBtnTendTable()
{
	if (-1 == m_nListCurrentIndex) {
		return;
	}
	ShowDataPreviewDlg(REVIEW_TEND_TABLE);
}

void CMonitorRecordDlg::OnBnClickedBtnFhrWaveform()
{
	if (-1 == m_nListCurrentIndex) {
		return;
	}
	ShowDataPreviewDlg(REVIEW_FHR_WAVEFORM);
}

void CMonitorRecordDlg::ShowDataPreviewDlg(int type)
{
	CString sMRID = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_MRID);
	CString sTime = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_MONITOR_DATE);
	CString sBedNum = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_BED_NUM);
	CString sMedNum = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_MEDICAL_NUM);
	CString sName = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_PATIENT_NAME);
	CString sGender = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_GENDER);
	CString sPatientType = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_PATIENT_TYPE);
	CString sAge = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_PATIENT_AGE);
	CString sPregWeek = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_PREG_WEEK);
	CReviewDlg dlg(this, sMRID, type, sTime, sBedNum, sMedNum, sName, sGender, sPatientType, sAge, sPregWeek);
	dlg.DoModal();
}


void CMonitorRecordDlg::OnBnClickedBtnSavePregnantWeek()
{
	if (-1 == m_nListCurrentIndex) {
		return;
	}

	CString sMRID = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_MRID);
	if (MoreThan24Hour(sMRID)) {
		MessageBox(_T("该记录已经超过24小时，您已经不能再修改了！"));
		return;
	}

	CString sPregWeekInList = m_lstRecord.GetItemText(m_nListCurrentIndex, MONITOR_LIST_INDEX_OF_PREG_WEEK);
	CString sPregnantWeek;
	m_edtPregnantWeek.GetWindowText(sPregnantWeek);
	if (sPregnantWeek != sPregWeekInList) {
		MonitorRecordSetPregnantWeek(sMRID, sPregnantWeek);
		UpdateRecordList();
		SetNaviButtonState();
	}
}
