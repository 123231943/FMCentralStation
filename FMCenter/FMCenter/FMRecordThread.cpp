#include "stdafx.h"
#include "FMCenter.h"
#include "FMRecordUnit.h"
#include "FMRecordThread.h"

static DWORD WINAPI ThreadFuncSaveA(LPVOID lpParam);
static DWORD WINAPI ThreadFuncSaveB(LPVOID lpParam);

CFMRecordThread::CFMRecordThread(void)
{
	m_hShutdownEvent = NULL;
	m_hSaveThreadA = NULL;
	m_hSaveThreadB = NULL;
	ZeroMemory(m_arUnitGroup, sizeof(m_arUnitGroup));
	ZeroMemory(m_arUnitEventGroup, sizeof(m_arUnitEventGroup));
}


CFMRecordThread::~CFMRecordThread(void)
{
	int i;

	//关闭记录组
	//记录组关闭时，首先要保存数据。
	//若先关闭存盘线程，会导致析构函数死锁
	for (i=0; i<CLIENT_COUNT; i++) {
		if (m_arUnitGroup[i]) {
			delete m_arUnitGroup[i];
			m_arUnitGroup[i] = NULL;
		}
	}

	//关闭存盘线程
	SetEvent(m_hShutdownEvent);
	HANDLE threads[] = {m_hSaveThreadA, m_hSaveThreadB};
	WaitForMultipleObjects(2, threads, TRUE, INFINITE);
	m_hSaveThreadA = NULL;
	m_hSaveThreadB = NULL;

	CloseHandle(m_hShutdownEvent);
	m_hShutdownEvent = NULL;

	//关闭事件组
	for (i=0; i<CLIENT_COUNT; i++) {
		CloseHandle(m_arUnitEventGroup[i]);
		m_arUnitEventGroup[i] = NULL;
	}
}

BOOL CFMRecordThread::Start()
{
	//创建记录组，用于记录床旁机数据
	int i;
	m_hShutdownEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	for (i=0; i<CLIENT_COUNT; i++) {
		HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_arUnitEventGroup[i] = hEvent;
		m_arUnitGroup[i] = new CFMRecordUnit(i, hEvent);
	}

	//创建工作线程，用于将已经存满的数据单元写入文件
	DWORD nThreadID;
	m_hSaveThreadA = CreateThread(NULL, 4096, (LPTHREAD_START_ROUTINE)&ThreadFuncSaveA, this, 0, (LPDWORD)&nThreadID);
	if (m_hSaveThreadA == NULL) {
		return FALSE;
	}
	m_hSaveThreadB = CreateThread(NULL, 4096, (LPTHREAD_START_ROUTINE)&ThreadFuncSaveB, this, 0, (LPDWORD)&nThreadID);
	if (m_hSaveThreadB == NULL) {
		return FALSE;
	}
	return TRUE;
}

CFMRecordUnit* CFMRecordThread::GetUnit(int nIndex)
{
	return m_arUnitGroup[nIndex];
}

static DWORD WINAPI ThreadFuncSaveA(LPVOID lpParam)
{
	int nIndex;
	CFMRecordThread* pr = (CFMRecordThread*)lpParam;

	//Worker thread will be around to process requests, until a Shutdown event is not Signaled.
	BOOL bRun = TRUE;
	HANDLE events[CLIENT_COUNT/2 + 1];
	events[0] = pr->GetShutdownEvent();
	memcpy(&(events[1]), pr->GetUnitEventGroup(), sizeof(HANDLE) * (CLIENT_COUNT/2));
	while (bRun) {

		DWORD dwRet = WaitForMultipleObjects(CLIENT_COUNT/2+1, events, FALSE, INFINITE);
		if (dwRet != 258) {//258是超时
			CString info;
			info.Format(_T("ThreadFuncSave被触发，事件：%d\n"), dwRet);
			theApp.WriteLog(info);
		}
		switch (dwRet) {
		case WAIT_FAILED:
		case WAIT_OBJECT_0://收到退出事件
			ResetEvent(events[0]);
			bRun = FALSE;
			break;

		case WAIT_TIMEOUT:
			//超时不用处理
			break;

		default:
			ResetEvent(events[dwRet - WAIT_OBJECT_0]);
			nIndex = dwRet - WAIT_OBJECT_0 - 1;
			CFMRecordUnit* pu = pr->GetUnit(nIndex);
			pu->ResetSaveEvent();
			pu->SaveUnitProcess();
			break;
		}

	} // while

	return 0;
}

static DWORD WINAPI ThreadFuncSaveB(LPVOID lpParam)
{
	int nIndex;
	CFMRecordThread* pr = (CFMRecordThread*)lpParam;

	//Worker thread will be around to process requests, until a Shutdown event is not Signaled.
	BOOL bRun = TRUE;
	HANDLE events[CLIENT_COUNT/2 + 1];
	events[0] = pr->GetShutdownEvent();
	HANDLE* arEventGroup = pr->GetUnitEventGroup();
	memcpy(&(events[1]), &(arEventGroup[CLIENT_COUNT/2]), sizeof(HANDLE) * (CLIENT_COUNT/2));
	while (bRun) {

		DWORD dwRet = WaitForMultipleObjects(CLIENT_COUNT/2+1, events, FALSE, INFINITE);
		if (dwRet != 258) {//258是超时
			CString info;
			info.Format(_T("ThreadFuncSave被触发，事件：%d\n"), dwRet);
			theApp.WriteLog(info);
		}
		switch (dwRet) {
		case WAIT_FAILED:
		case WAIT_OBJECT_0://收到退出事件
			ResetEvent(events[0]);
			bRun = FALSE;
			break;

		case WAIT_TIMEOUT:
			//超时不用处理
			break;

		default:
			ResetEvent(events[dwRet - WAIT_OBJECT_0]);
			nIndex = CLIENT_COUNT/2 + dwRet - WAIT_OBJECT_0 - 1;
			CFMRecordUnit* pu = pr->GetUnit(nIndex);
			pu->ResetSaveEvent();
			pu->SaveUnitProcess();
			break;
		}

	} // while

	return 0;
}
