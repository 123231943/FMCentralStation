#pragma once
#include "afxwin.h"

#define REVIEW_ECG_WAVEFORM            (1)
#define REVIEW_FHR_WAVEFORM            (2)
#define REVIEW_TEND_TABLE              (3)
#define REVIEW_TEND_WAVEFORM           (4)

// CReviewDlg 对话框
class CPrintFrame;
class CReviewDlg;
class CReviewTool
{
public:
	CReviewTool(CReviewDlg* pdlg) { m_pReviewDlg = pdlg; };
	virtual ~CReviewTool() {};

protected:
	CReviewDlg* m_pReviewDlg;

friend class CReviewDlg;
protected:
	virtual int  GetPageSpan() = 0;
	virtual void EnterReview() = 0;
	virtual void LeaveReview() = 0;
	virtual void Draw(CDC* pdc) = 0;
};

class CPrintTool
{
public:
	CPrintTool(CReviewDlg* pdlg) { m_pReviewDlg = pdlg; };
	virtual ~CPrintTool() {};

protected:
	CReviewDlg* m_pReviewDlg;

public:
	virtual void InitSetup(int type=0) = 0;
	virtual void Print(CDC* pdc,  CPrintInfo* pInfo) = 0;
};

class CReviewDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CReviewDlg)

private:
	int     m_nType;
	int     m_nOldType;
	BOOL    m_bInitBkDC;
	CDC*    m_pMemDC;
	CPrintFrame* m_pPrintFrame;
	int     m_nPrintType;
	int     m_nZoomScale;

private:
	CLIENT_DATA* m_arBuffer;
	CString m_sStartTime;
	CTime   m_tmStart;
	int     m_nTotalSize;//本记录的总长度
	int     m_nPageSpan;
	int     m_nCurPos;

public:
	CString m_sMRID;
	CString m_sBedNum;
	CString m_sMedicalNum;
	CString m_sPatientName;
	CString m_sPatientGender;
	CString m_sPatientType;
	CString m_sPatientAge;
	CString m_sPregnantWeek;

	BOOL m_bPrintHR;
	BOOL m_bPrintSYS;
	BOOL m_bPrintMEA;
	BOOL m_bPrintDIA;
	BOOL m_bPrintRR;
	BOOL m_bPrintSPO2;
	BOOL m_bPrintT1;
	BOOL m_bPrintT2;

	inline int GetTotalSize() { return m_nTotalSize; };
	inline int GetZoomScale() { return m_nZoomScale; };
	inline int GetNIBPUnitOpt() { return m_cmbSelectNibpUnit.GetCurSel(); };
	inline int GetTEMPUnitOpt() { return m_cmbSelectTempUnit.GetCurSel(); };
	CRect GetTableRect();
	void InitBkgd(CDC* pdc, COLORREF cr);
	CLIENT_DATA* GetCurrentBufferPointer(int* pnValidLen);
	CTime GetCurrentDataTime(int* pnValidLen);
	void OnPrint(CDC * pDC, CPrintInfo* pInfo);

private:
	CReviewTool* m_pECGReviewTool;
	CReviewTool* m_pFHRReviewTool;
	CReviewTool* m_pTendTableTool;
	CReviewTool* m_pTendWaveTool;

	CPrintTool* m_pECGPrintTool;
	CPrintTool* m_pFHRPrintTool;
	CPrintTool* m_pTendTablePrintTool;
	CPrintTool* m_pTendWavePrintTool;

private:
	void SetBtnState();
	void SwitchReview();
	void SetZoomInfoAndButton();
	int  ReadRecordFiles(CStringArray& sa, CUIntArray& uaOffset, CUIntArray& uaSize);
	int  GetPageSpan();
	void RenewScrollBar();

public:
	CReviewDlg(CWnd* pParent, CString mrid, int type, CString sStartTime,
		CString sBedNum, CString sMedicalNum, CString sPatientName, CString sPatientGender,
		CString sPatientType, CString sPatientAge, CString sPregnantWeek);
	virtual ~CReviewDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_REVIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedBtnEcgWaveform();
	afx_msg void OnBnClickedBtnFhrWaveform();
	afx_msg void OnBnClickedBtnTendTable();
	afx_msg void OnBnClickedBtnTendWaveform();
	afx_msg void OnBnClickedBtnPrint();
	afx_msg void OnPaint();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnBnClickedBtnZoomOut();
	afx_msg void OnBnClickedBtnZoomIn();
	afx_msg void OnCbnSelchangeComboSelectNibpUnit();
	afx_msg void OnCbnSelchangeComboSelectTempUnit();
	afx_msg void OnBnClickedBtnSelectParam();
	CButton m_btnECGWaveform;
	CButton m_btnFHRWaveform;
	CButton m_btnTendTable;
	CButton m_btnTendWaveform;
	CScrollBar m_sclBar;
	CEdit m_edtTimeSpan;
	CEdit m_edtStartTime;
	CEdit m_edtPatientName;
	CStatic m_staTableArea;
	CButton m_btnZoomOut;
	CButton m_btnZoomIn;
	CEdit m_edtZoomInfo;
	CComboBox m_cmbSelectFormat;
	CStatic m_staSelectFormat;
	CStatic m_staSelectNibpUnit;
	CComboBox m_cmbSelectNibpUnit;
	CStatic m_staSelectTempUnit;
	CComboBox m_cmbSelectTempUnit;
	CButton m_btnSelectParam;
	CComboBox m_cmbSelectChannel;
	CStatic m_staDoctorDiagno;
	CComboBox m_cmbDoctorDiagno;
};
