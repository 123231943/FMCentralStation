#pragma once


// CSetupFMDlg 对话框

class CSetupFMDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSetupFMDlg)

public:
	CSetupFMDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSetupFMDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_SETUP_FM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
