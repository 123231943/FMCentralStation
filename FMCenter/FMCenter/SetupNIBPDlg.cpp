// SetupNIBPDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "SetupNIBPDlg.h"
#include "afxdialogex.h"
#include "FMSocketThread.h"
#include "FMSocketUnit.h"
#include "FMRecordUnit.h"

// CSetupNIBPDlg 对话框

IMPLEMENT_DYNAMIC(CSetupNIBPDlg, CDialogEx)

CSetupNIBPDlg::CSetupNIBPDlg(CWnd* pParent, CFMRecordUnit* pru, int nNIBPUnit)
	: CDialogEx(CSetupNIBPDlg::IDD, pParent)
	, m_pru(pru)
{
	m_nNIBPUnitOpt = nNIBPUnit;
}

CSetupNIBPDlg::~CSetupNIBPDlg()
{
}

void CSetupNIBPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_SYS_HIGH, m_edtSysHigh);
	DDX_Control(pDX, IDC_EDIT_SYS_LOW, m_edtSysLow);
	DDX_Control(pDX, IDC_EDIT_MEA_HIGH, m_edtMeaHigh);
	DDX_Control(pDX, IDC_EDIT_MEA_LOW, m_edtMeaLow);
	DDX_Control(pDX, IDC_EDIT_DIA_HIGH, m_edtDiaHigh);
	DDX_Control(pDX, IDC_EDIT_DIA_LOW, m_edtDiaLow);
	DDX_Control(pDX, IDC_COMBO_MODE, m_cmbMode);
	DDX_Control(pDX, IDC_EDIT_PUMP_INFO, m_edtPumpInfo);
	DDX_Control(pDX, IDC_COMBO_NIBP_UNIT, m_cmbNIBPUnit);
	DDX_Control(pDX, IDC_STA_SYS_RANGE1, m_staSysRange1);
	DDX_Control(pDX, IDC_STA_SYS_RANGE2, m_staSysRange2);
	DDX_Control(pDX, IDC_STA_MEA_RANGE1, m_staMeaRange1);
	DDX_Control(pDX, IDC_STA_MEA_RANGE2, m_staMeaRange2);
	DDX_Control(pDX, IDC_STA_DIA_RANGE1, m_staDiaRange1);
	DDX_Control(pDX, IDC_STA_DIA_RANGE2, m_staDiaRange2);
}

BEGIN_MESSAGE_MAP(CSetupNIBPDlg, CDialogEx)
	ON_BN_CLICKED(ID_PUMP_START, &CSetupNIBPDlg::OnBnClickedPumpStart)
	ON_BN_CLICKED(ID_PUMP_STOP, &CSetupNIBPDlg::OnBnClickedPumpStop)
	ON_BN_CLICKED(IDOK, &CSetupNIBPDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSetupNIBPDlg::OnBnClickedCancel)
	ON_BN_CLICKED(ID_BTN_SEND, &CSetupNIBPDlg::OnBnClickedBtnSend)
END_MESSAGE_MAP()

// CSetupNIBPDlg 消息处理程序
void CSetupNIBPDlg::OnBnClickedPumpStart()
{
	m_edtPumpInfo.SetWindowText(_T(""));
	CFMSocketUnit* psu = theApp.m_pSocketThread->GetUnit(m_pru->GetIndex());
	if (psu->SendNIBPPumpCmd(TRUE)) { //启动血压泵
		m_edtPumpInfo.SetWindowText(_T("启动 发送成功。"));
	}
	else {
		m_edtPumpInfo.SetWindowText(_T("启动 发送失败！"));
	}
}

void CSetupNIBPDlg::OnBnClickedPumpStop()
{
	m_edtPumpInfo.SetWindowText(_T(""));
	CFMSocketUnit* psu = theApp.m_pSocketThread->GetUnit(m_pru->GetIndex());
	if (psu->SendNIBPPumpCmd(FALSE)) { //停止血压泵
		m_edtPumpInfo.SetWindowText(_T("停止 发送成功。"));
	}
	else {
		m_edtPumpInfo.SetWindowText(_T("停止 发送失败！"));
	}
}

void CSetupNIBPDlg::OnBnClickedBtnSend()
{
	// 发送设置
	CString sValue;
	m_edtSysHigh.GetWindowText(sValue); //统一转换成mmHg进行处理
	int sys_h = (int)(1 == m_nNIBPUnitOpt ? 7.5f * _ttof(sValue) : _ttof(sValue));
	m_edtSysLow.GetWindowText(sValue);
	int sys_l = (int)(1 == m_nNIBPUnitOpt ? 7.5f * _ttof(sValue) : _ttof(sValue));
	m_edtMeaHigh.GetWindowText(sValue);
	int mea_h = (int)(1 == m_nNIBPUnitOpt ? 7.5f * _ttof(sValue) : _ttof(sValue));
	m_edtMeaLow.GetWindowText(sValue);
	int mea_l = (int)(1 == m_nNIBPUnitOpt ? 7.5f * _ttof(sValue) : _ttof(sValue));
	m_edtDiaHigh.GetWindowText(sValue);
	int dia_h = (int)(1 == m_nNIBPUnitOpt ? 7.5f * _ttof(sValue) : _ttof(sValue));
	m_edtDiaLow.GetWindowText(sValue);
	int dia_l = (int)(1 == m_nNIBPUnitOpt ? 7.5f * _ttof(sValue) : _ttof(sValue));

	if (sys_h < 40 || sys_h > 270) {
		MessageBox(_T("收缩压上限超出范围！"));
		m_edtSysHigh.SetFocus();
		return;
	}
	if (sys_l < 40 || sys_l > 270) {
		MessageBox(_T("收缩压下限超出范围！"));
		m_edtSysLow.SetFocus();
		return;
	}
	if (sys_l >= sys_h) {
		MessageBox(_T("收缩压下限不能超越上限设定！"));
		m_edtSysLow.SetFocus();
		return;
	}
	if (mea_h < 20 || mea_h > 230) {
		MessageBox(_T("平均压上限超出范围！"));
		m_edtMeaHigh.SetFocus();
		return;
	}
	if (mea_l < 20 || mea_l > 230) {
		MessageBox(_T("平均压下限超出范围！"));
		m_edtMeaLow.SetFocus();
		return;
	}
	if (mea_l >= mea_h) {
		MessageBox(_T("平均压下限不能超越上限设定！"));
		m_edtMeaLow.SetFocus();
		return;
	}
	if (dia_h < 10 || dia_h > 210) {
		MessageBox(_T("舒张压上限超出范围！"));
		m_edtDiaHigh.SetFocus();
		return;
	}
	if (dia_l < 10 || dia_l > 210) {
		MessageBox(_T("舒张压下限超出范围！"));
		m_edtDiaLow.SetFocus();
		return;
	}
	if (dia_l >= dia_h) {
		MessageBox(_T("舒张压下限不能超越上限设定！"));
		m_edtDiaLow.SetFocus();
		return;
	}

	int nibp_mode = m_cmbMode.GetCurSel();
	CFMSocketUnit* psu = theApp.m_pSocketThread->GetUnit(m_pru->GetIndex());
	psu->SendSetupNIBP(sys_h, sys_l, mea_h, mea_l, dia_h, dia_l, nibp_mode);
}

void CSetupNIBPDlg::OnBnClickedOk()
{
	m_nNIBPUnitOpt = m_cmbNIBPUnit.GetCurSel();
	CDialogEx::OnOK();
}

void CSetupNIBPDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}

BOOL CSetupNIBPDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_cmbMode.ResetContent();
	m_cmbNIBPUnit.ResetContent();
	int i;
	int count = CFMDict::GetNIBPModeCount();
	for (i=0; i<count; i++) {
		m_cmbMode.AddString(CFMDict::NIBPMode_itos(i));
	}
	count = CFMDict::GetNIBPUnitCount();
	for (i=0; i<count; i++) {
		m_cmbNIBPUnit.AddString(CFMDict::NIBPUnit_itos(i));
	}
	m_cmbNIBPUnit.SetCurSel(m_nNIBPUnitOpt);

	DISPLAY_DATA* pdd = m_pru->GetLatestNumbData();
	if (pdd) {
		CString sValue;
		if (1 == m_nNIBPUnitOpt) {
			//转化为千帕
			sValue.Format(_T("%.1f"), 0.1333f * (float)pdd->sys_h);
			m_edtSysHigh.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), 0.1333f * (float)pdd->sys_l);
			m_edtSysLow.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), 0.1333f * (float)pdd->mea_h);
			m_edtMeaHigh.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), 0.1333f * (float)pdd->mea_l);
			m_edtMeaLow.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), 0.1333f * (float)pdd->dia_h);
			m_edtDiaHigh.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), 0.1333f * (float)pdd->dia_l);
			m_edtDiaLow.SetWindowText(sValue);
		}
		else {
			sValue.Format(_T("%d"), pdd->sys_h);
			m_edtSysHigh.SetWindowText(sValue);
			sValue.Format(_T("%d"), pdd->sys_l);
			m_edtSysLow.SetWindowText(sValue);
			sValue.Format(_T("%d"), pdd->mea_h);
			m_edtMeaHigh.SetWindowText(sValue);
			sValue.Format(_T("%d"), pdd->mea_l);
			m_edtMeaLow.SetWindowText(sValue);
			sValue.Format(_T("%d"), pdd->dia_h);
			m_edtDiaHigh.SetWindowText(sValue);
			sValue.Format(_T("%d"), pdd->dia_l);
			m_edtDiaLow.SetWindowText(sValue);
		}
		m_cmbMode.SetCurSel(pdd->nibp_mode);
	}

	if (1 == m_nNIBPUnitOpt) {
		//提示信息转化为千帕
		m_staSysRange1.SetWindowText(_T("（12~21.3 kPa）"));
		m_staSysRange2.SetWindowText(_T("（12~21.3 kPa）"));
		m_staMeaRange1.SetWindowText(_T("（8~14.7 kPa）"));
		m_staMeaRange2.SetWindowText(_T("（8~14.7 kPa）"));
		m_staDiaRange1.SetWindowText(_T("（6.7~12 kPa）"));
		m_staDiaRange2.SetWindowText(_T("（6.7~12 kPa）"));
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

