#include "stdafx.h"
#include "FMCenter.h"
#include "FMMonitorUnit.h"
#include "FMMonitorView.h"
#include "FMRecordUnit.h"
#include "FMRecordThread.h"
#include "FMSocketUnit.h"
#include "FMSocketThread.h"
#include "MonitorViewToolbar.h"
#include "MedicalRecordListDlg.h"
#include "FMDisplayDataBuffer.h"
#include "SetupECGDlg.h"
#include "SetupRESPDlg.h"
#include "SetupSPO2Dlg.h"
#include "SetupNIBPDlg.h"
#include "SetupTEMPDlg.h"
#include "SetupFHRDlg.h"

static const int nSmlTextH = 12;
static const int nSmlTextW = 12;
static const int nTextH = 16;
static const int nTextW = 16;
static const int nBigTextH = 24;
static const int nBigTextW = 16;
static const COLORREF cr_icon_text = RGB(197, 209, 163);

CFMMonitorUnit::CFMMonitorUnit(CFMMonitorView* pView)
	: m_rcECG (-1, -1, -1, -1)
	, m_rcRESP(-1, -1, -1, -1)
	, m_rcSPO2(-1, -1, -1, -1)
	, m_rcNIBP(-1, -1, -1, -1)
	, m_rcTEMP(-1, -1, -1, -1)
	, m_rcFHR (-1, -1, -1, -1)
	, m_nFHRPaperSpeedOpt(theApp.m_nFHRSpeedOption)
	, m_nNIBPUnit(theApp.m_nNIBPUnitOption)
	, m_nTEMPUnit(theApp.m_nTEMPUnitOption)
{
	m_pView = pView;
	m_x = 0;
	m_y = 0;
	m_w = 0;
	m_h = 0;
	m_nAreaType = MONITOR_AREA_1;
	m_nOldType = -1;
	m_nOldAreaType = -1;
	m_bVisible = FALSE;
	m_ppBkDC = NULL;
	m_bRedrawBkgd = TRUE;
	m_pUnit = NULL;
	m_nTypeDebug = MONITOR_TYPE_FETUS;

	//数字恢复为默认值
	ResetNumber();

	m_pWaveECG1 = NULL;
	m_pWaveECG2 = NULL;
	m_pWaveECGv = NULL;
	m_pWaveSPO2 = NULL;
	m_pWaveRESP = NULL;
	m_pWaveFHR1 = NULL;
	m_pWaveFHR2 = NULL;
	m_pWaveTOCO = NULL;
	m_pWaveFMMARK = NULL;
	m_pWaveTOCOReset = NULL;
	m_nWaveECG_StartPos = 0;
	m_nWaveSPO2_StartPos = 0;
	m_nWaveRESP_StartPos = 0;
	m_nWaveFHR_StartPos = 0;
	m_nLastGetDataTimeSpan = 0;
	m_nCurErrorIndex = 0;

#ifdef DECIMUS_VERSION
	m_bDrawWaveDecimus = FALSE;
	m_nWaveECG_DecimusDataPos = 0;
	m_nWaveSPO2_DecimusDataPos = 0;
	m_nWaveRESP_DecimusDataPos = 0;
#endif
}

CFMMonitorUnit::~CFMMonitorUnit(void)
{
	m_pUnit = NULL;
	m_pView = NULL;
}

void CFMMonitorUnit::ResetNumber()
{
	int nDefaultFM    = 0;
	int nDefaultValue = INVALID_VALUE;
	int nDefaultIcon  = 0;

	if (theApp.DebugUI()) {
		nDefaultFM    = 12;
		nDefaultValue = 123;
		nDefaultIcon  = 0;
	}

	m_nHRFlag       = nDefaultIcon;
	m_nHR           = nDefaultValue;
	m_nHRLimitH     = nDefaultValue;
	m_nHRLimitL     = nDefaultValue;
	m_nECGGainOpt   = nDefaultIcon;
	m_nECG1LeadOpt  = nDefaultIcon;
	m_nECG2LeadOpt  = nDefaultIcon;
	m_nECGFilter    = nDefaultIcon;
	m_nNIBP_S       = nDefaultValue;
	m_nNIBP_M       = nDefaultValue;
	m_nNIBP_D       = nDefaultValue;
	m_nNIBPLimitH   = nDefaultValue;
	m_nNIBPLimitL   = nDefaultValue;
	m_nNIBPPumpPressure = nDefaultValue;
	m_nNIBPPumpMode = nDefaultIcon;
	m_nT1           = nDefaultValue;
	m_nT1LimitH     = nDefaultValue;
	m_nT1LimitL     = nDefaultValue;
	m_nT2           = nDefaultValue;
	m_nT2LimitH     = nDefaultValue;
	m_nT2LimitL     = nDefaultValue;
	m_nSPO2         = nDefaultValue;
	m_nSPO2LimitH   = nDefaultValue;
	m_nSPO2LimitL   = nDefaultValue;
	m_nPR           = nDefaultValue;
	m_nPRLimitH     = nDefaultValue;
	m_nPRLimitL     = nDefaultValue;
	m_nRR           = nDefaultValue;
	m_nRRLimitH     = nDefaultValue;
	m_nRRLimitL     = nDefaultValue;
	m_nFHR1         = nDefaultValue;
	m_nFHR2         = nDefaultValue;
	m_nFHRLimitH    = nDefaultValue;
	m_nFHRLimitL    = nDefaultValue;
	m_nTOCO         = nDefaultValue;
	m_nTOCOLimitH   = nDefaultValue;
	m_nTOCOLimitL   = nDefaultValue;
	m_nFM           = nDefaultFM;
	m_nECGError     = 0;
	m_nRESPError    = 0;
	m_nSPO2Error    = 0;
	m_nNIBPError    = 0;
	m_nTEMPError    = 0;
	m_nFHRError     = 0;
	m_nBioAlarmType = 0;
	m_nBioAlarmLevel = 0;
	m_nOldBioAlarmLevel = 0;
	m_nFHRBaseline1 = 0;
	m_nFHRBaseline2 = 0;
}

void CFMMonitorUnit::DrawBkgd(CDC* pdc)
{
	if (*m_ppBkDC) {
		pdc->BitBlt(m_x, m_y, m_w, m_h, *m_ppBkDC, 0, 0, SRCCOPY);
	}

	if (theApp.DebugArea()) {
		DrawCheckArea(pdc);
	}
}

void CFMMonitorUnit::DrawInfo(CDC* pdc)
{
	const int nTimeToRight = 250;

	CRect rc(m_nInfoBarX, m_nInfoBarY, m_nInfoBarX + m_nInfoBarW, m_nInfoBarY + m_nInfoBarH);
	CString sInfo;
	CString sTime;
	CString sName = _T("姓名：----");
	CString sGender = _T("性别：----");
	CString sType = _T("类型：----");
	CString sAge = _T("年龄：----");
	CString sPregnantWeek = _T("");
	if (m_pUnit) {
		CString name;
		int gender;
		int type;
		int age;
		if (m_pUnit->GetPatientInfo(name, gender, type, age)) {
			sName.Format(_T("姓名：%s"), name);
			sGender.Format(_T("性别：%s"), CFMDict::Gender_itos(gender));
			sType.Format(_T("类型：%s"), CFMDict::PatientType_itos(type));
			sAge.Format(_T("年龄：%d"), age);
		}
		CString preg_week = m_pUnit->GetPregnantWeek();
		if (preg_week.GetLength() > 0) {
			sPregnantWeek.Format(_T("孕周：%s"), preg_week);
		}
	}
	sInfo.Format(_T("%10s%10s%10s%10s%10s"), sName, sGender, sType, sAge, sPregnantWeek);
	
	CTimeSpan ts(0, 0, 0, m_pUnit->GetDisplayLenOfSecond());
	CTime tmStart = m_pUnit->GetStartTime();
	sTime = tmStart.Format(_T("开始时间：%H:%M:%S"));
	sTime += ts.Format(_T("  时长：%H:%M:%S"));

	if (*m_ppBkDC) {
		pdc->BitBlt(m_x + rc.left, m_y + rc.top, rc.Width(), rc.Height(), *m_ppBkDC, rc.left, rc.top, SRCCOPY);
	}
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(RGB(255,255,255));
	m_pView->SelectInfoFont();
	pdc->TextOut(m_x + rc.left, m_y + rc.top + (m_nInfoBarH - nTextH) / 2, sInfo);
	pdc->TextOut(m_x + rc.right - nTimeToRight, m_y + rc.top + (m_nInfoBarH - nTextH) / 2, sTime);
	m_pView->ResetFont();
}

void CFMMonitorUnit::DrawNumber(CDC* pdc)
{
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(cr_icon_text);
	m_pView->SelectNumberFont();
	m_pView->DrawNumber(pdc, m_x + m_stBedNum.x, m_y + m_stBedNum.y, StringBedNum(), m_stBedNum.type);

	if (MONITOR_TYPE_ADULT == GetType() ||
		MONITOR_TYPE_BOTH == GetType()) {
		if (m_stHRFlag.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stHRFlag.x, m_y + m_stHRFlag.y, StringHRFlag(), m_stHRFlag.type); }
		if (0 == m_nHRFlag) {
			if (m_stHR.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stHR.x, m_y + m_stHR.y, StringHR(), m_stHR.type); }
			if (m_stHRLimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stHRLimitH.x, m_y + m_stHRLimitH.y, StringHRLimitH(), m_stHRLimitH.type); }
			if (m_stHRLimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stHRLimitL.x, m_y + m_stHRLimitL.y, StringHRLimitL(), m_stHRLimitL.type); }
		}
		else {
			if (m_stHR.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stHR.x, m_y + m_stHR.y, StringPR(), m_stHR.type); }
			if (m_stHRLimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stHRLimitH.x, m_y + m_stHRLimitH.y, StringPRLimitH(), m_stHRLimitH.type); }
			if (m_stHRLimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stHRLimitL.x, m_y + m_stHRLimitL.y, StringPRLimitL(), m_stHRLimitL.type); }
		}
		if (m_stECGGainOpt.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stECGGainOpt.x, m_y + m_stECGGainOpt.y, StringECGGainOpt(), m_stECGGainOpt.type); }
		if (m_stECG1LeadOpt.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stECG1LeadOpt.x, m_y + m_stECG1LeadOpt.y, StringECG1LeadOpt(), m_stECG1LeadOpt.type); }
		if (m_stECG2LeadOpt.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stECG2LeadOpt.x, m_y + m_stECG2LeadOpt.y, StringECG2LeadOpt(), m_stECG2LeadOpt.type); }

		if (m_stNIBP_S.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBP_S.x, m_y + m_stNIBP_S.y, StringNIBP_S(), m_stNIBP_S.type, m_stNIBP_S.sub_type); }
		if (m_stNIBP_M.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBP_M.x, m_y + m_stNIBP_M.y, StringNIBP_M(), m_stNIBP_M.type, m_stNIBP_M.sub_type); }
		if (m_stNIBP_D.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBP_D.x, m_y + m_stNIBP_D.y, StringNIBP_D(), m_stNIBP_D.type, m_stNIBP_D.sub_type); }
		if (m_stNIBPLimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBPLimitH.x, m_y + m_stNIBPLimitH.y, StringNIBPLimitH(), m_stNIBPLimitH.type); }
		if (m_stNIBPLimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBPLimitL.x, m_y + m_stNIBPLimitL.y, StringNIBPLimitL(), m_stNIBPLimitL.type); }
		if (m_stNIBPUnit.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBPUnit.x, m_y + m_stNIBPUnit.y, StringNIBPUnit(), m_stNIBPUnit.type); }

		if (m_stT1.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT1.x, m_y + m_stT1.y, StringT1(), m_stT1.type, m_stT1.sub_type); }
		if (m_stT1LimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT1LimitH.x, m_y + m_stT1LimitH.y, StringT1LimitH(), m_stT1LimitH.type); }
		if (m_stT1LimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT1LimitL.x, m_y + m_stT1LimitL.y, StringT1LimitL(), m_stT1LimitL.type); }
		if (m_stT2.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT2.x, m_y + m_stT2.y, StringT2(), m_stT2.type, m_stT2.sub_type); }
		if (m_stT2LimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT2LimitH.x, m_y + m_stT2LimitH.y, StringT2LimitH(), m_stT2LimitH.type); }
		if (m_stT2LimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT2LimitL.x, m_y + m_stT2LimitL.y, StringT2LimitL(), m_stT2LimitL.type); }
		if (m_stTEMPUnit.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stTEMPUnit.x, m_y + m_stTEMPUnit.y, StringTEMPUnit(), m_stTEMPUnit.type); }

		if (m_stSPO2.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stSPO2.x, m_y + m_stSPO2.y, StringSPO2(), m_stSPO2.type); }
		if (m_stSPO2LimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stSPO2LimitH.x, m_y + m_stSPO2LimitH.y, StringSPO2LimitH(), m_stSPO2LimitH.type); }
		if (m_stSPO2LimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stSPO2LimitL.x, m_y + m_stSPO2LimitL.y, StringSPO2LimitL(), m_stSPO2LimitL.type); }

		if (m_stRR.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stRR.x, m_y + m_stRR.y, StringRR(), m_stRR.type); }
		if (m_stRRLimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stRRLimitH.x, m_y + m_stRRLimitH.y, StringRRLimitH(), m_stRRLimitH.type); }
		if (m_stRRLimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stRRLimitL.x, m_y + m_stRRLimitL.y, StringRRLimitL(), m_stRRLimitL.type); }

		//绘制其他文字信息
		if (0 != m_stECGSpeed.x && 0 != m_stECGSpeed.y) {
			if (*m_ppBkDC) {
				pdc->BitBlt(m_x + m_stECGSpeed.x, m_y + m_stECGSpeed.y, nBigTextW * 10, nBigTextH,
					*m_ppBkDC, m_stECGSpeed.x, m_stECGSpeed.y, SRCCOPY);
			}
			pdc->TextOut(m_x + m_stECGSpeed.x, m_y + m_stECGSpeed.y, _T("速度：") + StringECGSpeed());
		}
		if (0 != m_stECGFilter.x && 0 != m_stECGFilter.y) {
			if (*m_ppBkDC) {
				pdc->BitBlt(m_x + m_stECGFilter.x, m_y + m_stECGFilter.y, nBigTextW * 10, nBigTextH,
					*m_ppBkDC, m_stECGFilter.x, m_stECGFilter.y, SRCCOPY);
			}
			pdc->TextOut(m_x + m_stECGFilter.x, m_y + m_stECGFilter.y, _T("滤波：") + StringECGFilter());
		}
		if (0 != m_stNIBPPumpMode.x && 0 != m_stNIBPPumpMode.y) {
			if (*m_ppBkDC) {
				pdc->BitBlt(m_x + m_stNIBPPumpMode.x, m_y + m_stNIBPPumpMode.y, nBigTextW * 10, nBigTextH,
					*m_ppBkDC, m_stNIBPPumpMode.x, m_stNIBPPumpMode.y, SRCCOPY);
			}
			pdc->TextOut(m_x + m_stNIBPPumpMode.x, m_y + m_stNIBPPumpMode.y, _T("模式：") + StringNIBPPumpMode());
		}
		if (0 != m_stNIBPPumpPressure.x && 0 != m_stNIBPPumpPressure.y) {
			if (*m_ppBkDC) {
				pdc->BitBlt(m_x + m_stNIBPPumpPressure.x, m_y + m_stNIBPPumpPressure.y, nBigTextW * 7, nBigTextH,
					*m_ppBkDC, m_stNIBPPumpPressure.x, m_stNIBPPumpPressure.y, SRCCOPY);
			}
			pdc->TextOut(m_x + m_stNIBPPumpPressure.x, m_y + m_stNIBPPumpPressure.y, _T("袖带压：") + StringNIBPPumpPressure());
		}
	}
	if (MONITOR_TYPE_FETUS == GetType() ||
		MONITOR_TYPE_BOTH == GetType()) {
		if (m_stFHR1.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stFHR1.x, m_y + m_stFHR1.y, StringFHR1(), m_stFHR1.type); }
		if (m_stFHR2.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stFHR2.x, m_y + m_stFHR2.y, StringFHR2(), m_stFHR2.type); }
		if (m_stFHRLimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stFHRLimitH.x, m_y + m_stFHRLimitH.y, StringFHRLimitH(), m_stFHRLimitH.type); }
		if (m_stFHRLimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stFHRLimitL.x, m_y + m_stFHRLimitL.y, StringFHRLimitL(), m_stFHRLimitL.type); }

		if (m_stTOCO.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stTOCO.x, m_y + m_stTOCO.y, StringTOCO(), m_stTOCO.type); }
		if (m_stTOCOLimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stTOCOLimitH.x, m_y + m_stTOCOLimitH.y, StringTOCOLimitH(), m_stTOCOLimitH.type); }
		if (m_stTOCOLimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stTOCOLimitL.x, m_y + m_stTOCOLimitL.y, StringTOCOLimitL(), m_stTOCOLimitL.type); }

		if (m_stFM.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stFM.x, m_y + m_stFM.y, StringFM(), m_stFM.type); }
	}
	m_pView->ResetFont();
}

void CFMMonitorUnit::RedrawNibpBkgd(CDC* pdc)
{
	if (MONITOR_TYPE_ADULT == GetType() ||
	MONITOR_TYPE_BOTH == GetType()) {
		if (m_stNIBP_S.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBP_S.x, m_y + m_stNIBP_S.y, _T("    "), m_stNIBP_S.type, m_stNIBP_S.sub_type); }
		if (m_stNIBP_M.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBP_M.x, m_y + m_stNIBP_M.y, _T("    "), m_stNIBP_M.type, m_stNIBP_M.sub_type); }
		if (m_stNIBP_D.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBP_D.x, m_y + m_stNIBP_D.y, _T("    "), m_stNIBP_D.type, m_stNIBP_D.sub_type); }
		if (m_stNIBPLimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBPLimitH.x, m_y + m_stNIBPLimitH.y, _T("    "), m_stNIBPLimitH.type); }
		if (m_stNIBPLimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stNIBPLimitL.x, m_y + m_stNIBPLimitL.y, _T("    "), m_stNIBPLimitL.type); }
	}
}

void CFMMonitorUnit::RedrawTempBkgd(CDC* pdc)
{
	if (MONITOR_TYPE_ADULT == GetType() ||
	MONITOR_TYPE_BOTH == GetType()) {
		if (m_stT1.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT1.x, m_y + m_stT1.y, _T("    "), m_stT1.type, m_stT1.sub_type); }
		if (m_stT1LimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT1LimitH.x, m_y + m_stT1LimitH.y, _T("    "), m_stT1LimitH.type); }
		if (m_stT1LimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT1LimitL.x, m_y + m_stT1LimitL.y, _T("    "), m_stT1LimitL.type); }
		if (m_stT2.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT2.x, m_y + m_stT2.y, _T("    "), m_stT2.type, m_stT2.sub_type); }
		if (m_stT2LimitH.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT2LimitH.x, m_y + m_stT2LimitH.y, _T("    "), m_stT2LimitH.type); }
		if (m_stT2LimitL.IsValid()) { m_pView->DrawNumber(pdc, m_x + m_stT2LimitL.x, m_y + m_stT2LimitL.y, _T("    "), m_stT2LimitL.type); }
	}
}

void CFMMonitorUnit::DrawWave(CDC* pdc)
{
	if (MONITOR_TYPE_ADULT == GetType() ||
		MONITOR_TYPE_BOTH == GetType()) {
		DrawECGWave(pdc);
		DrawSPO2Wave(pdc);
		DrawRESPWave(pdc);
	}
}

#ifdef DECIMUS_VERSION
void CFMMonitorUnit::DrawWaveDecimus(CDC* pdc, int nDecimusCounter)
{
	if (! m_bDrawWaveDecimus) {
		return;
	}

	if (! m_pUnit) {
		return;
	}

	CString s;
	s.Format(_T("GetType = %d\n"), GetType());
	theApp.WriteLog(s);

	if (MONITOR_TYPE_ADULT == GetType() ||
		MONITOR_TYPE_BOTH == GetType()) {
		DrawECGWaveDecimus(pdc, nDecimusCounter);
		DrawSPO2WaveDecimus(pdc, nDecimusCounter);
		DrawRESPWaveDecimus(pdc, nDecimusCounter);
	}
}
#endif

void CFMMonitorUnit::DrawFHRWave(CDC* pdc)
{
	if (MONITOR_TYPE_FETUS == GetType() ||
		MONITOR_TYPE_BOTH == GetType()) {
		switch (m_nFHRPaperSpeedOpt) {
		case FHR_PAPER_SPEED_3CM:
			DrawFHRWave3(pdc);
			break;
		case FHR_PAPER_SPEED_1CM:
			DrawFHRWave1(pdc);
			break;
		}
	}
}

void CFMMonitorUnit::DrawECGWave(CDC* pdc)
{
	if (! m_pWaveECG1 ||
		! m_pWaveECG2) {
		return;
	}

	if (0 == m_nWaveECG1_Y &&
		0 == m_nWaveECG2_Y) {
		return;
	}

	int nDrawLen = min(DISPLAY_ECG_UNIT_SIZE, m_nWaveECG_W);
	if (0 != m_nWaveECG1_Y) {
		if (*m_ppBkDC) {
			int x = m_nWaveECG_X + m_nWaveECG_StartPos + 1;
			int y = m_nWaveECG1_Y;
			int w1 = min(nDrawLen, m_nWaveECG_W - m_nWaveECG_StartPos - 1);
			int w2 = nDrawLen - w1;
			int h = m_nWaveECG_H;
			pdc->BitBlt(m_x+x, m_y+y, w1, h, *m_ppBkDC, x, y, SRCCOPY);
			if (w2 > 0) {
				pdc->BitBlt(m_x+m_nWaveECG_X, m_y+y, w2, h, *m_ppBkDC, m_nWaveECG_X, y, SRCCOPY);
			}
		}
	}
	if (0 != m_nWaveECG2_Y) {
		if (*m_ppBkDC) {
			int x = m_nWaveECG_X + m_nWaveECG_StartPos + 1;
			int y = m_nWaveECG2_Y;
			int w1 = min(nDrawLen, m_nWaveECG_W - m_nWaveECG_StartPos - 1);
			int w2 = nDrawLen - w1;
			int h = m_nWaveECG_H;
			pdc->BitBlt(m_x+x, m_y+y, w1, h, *m_ppBkDC, x, y, SRCCOPY);
			if (w2 > 0) {
				pdc->BitBlt(m_x+m_nWaveECG_X, m_y+y, w2, h, *m_ppBkDC, m_nWaveECG_X, y, SRCCOPY);
			}
		}
	}

	int nHalfH = m_nWaveECG_H / 2;
	int i;
	int x = m_nWaveECG_X + m_nWaveECG_StartPos;
	for (i = 0; i < nDrawLen - 1; i ++) {
		//绘制第一通道
		int mid_y1 = m_nWaveECG1_Y + nHalfH;
		int mid_y2 = m_nWaveECG1_Y + nHalfH;
		int v1 = min(ECG_MID_VALUE + nHalfH - 1, max(ECG_MID_VALUE - nHalfH + 1, m_pWaveECG1[i]));
		int v2 = min(ECG_MID_VALUE + nHalfH - 1, max(ECG_MID_VALUE - nHalfH + 1, m_pWaveECG1[i+1]));
		if (0 != m_nWaveECG1_Y) {
			DrawLineAA(pdc, m_crWaveECG1,
				m_x + x,
				m_y + mid_y1 + ECG_MID_VALUE - v1,
				m_y + mid_y1 + ECG_MID_VALUE - v2);
		}
		//绘制第二通道
		mid_y1 = m_nWaveECG2_Y + nHalfH;
		mid_y2 = m_nWaveECG2_Y + nHalfH;
		v1 = min(ECG_MID_VALUE + nHalfH - 1, max(ECG_MID_VALUE - nHalfH + 1, m_pWaveECG2[i]));
		v2 = min(ECG_MID_VALUE + nHalfH - 1, max(ECG_MID_VALUE - nHalfH + 1, m_pWaveECG2[i+1]));
		if (0 != m_nWaveECG2_Y) {
			DrawLineAA(pdc, m_crWaveECG2,
				m_x + x,
				m_y + mid_y2 + ECG_MID_VALUE - v1,
				m_y + mid_y2 + ECG_MID_VALUE - v2);
		}
		x ++;
		m_nWaveECG_StartPos ++;
		if (m_nWaveECG_StartPos >= m_nWaveECG_W - 1) {
			//绘制到达画布最右端时，跳转到画布的最左端
			m_nWaveECG_StartPos = 0;
			x = m_nWaveECG_X;
		}
	}

	//绘制终结线
	if (0 != m_nWaveECG1_Y) {
		CBrush br(m_crWaveECG1);
		CRect rc2(x+1, m_nWaveECG1_Y,
			min(x+3, m_nWaveECG_X + m_nWaveECG_W), m_nWaveECG1_Y + m_nWaveECG_H);
		CRect rc1(x+3, m_nWaveECG1_Y,
			min(x+15, m_nWaveECG_X + m_nWaveECG_W), m_nWaveECG1_Y + m_nWaveECG_H);
		if (*m_ppBkDC) {
			pdc->BitBlt(m_x + rc1.left, m_y + rc1.top, rc1.Width(), rc1.Height(), *m_ppBkDC, rc1.left, rc1.top, SRCCOPY);
		}
		rc2.OffsetRect(m_x, m_y);
		pdc->FillRect(&rc2, &br);
	}
	if (0 != m_nWaveECG2_Y) {
		CBrush br(m_crWaveECG2);
		CRect rc2(x+1, m_nWaveECG2_Y, 
			min(x+3, m_nWaveECG_X + m_nWaveECG_W), m_nWaveECG2_Y + m_nWaveECG_H);
		CRect rc1(x+3, m_nWaveECG2_Y, 
			min(x+15, m_nWaveECG_X + m_nWaveECG_W), m_nWaveECG2_Y + m_nWaveECG_H);
		if (*m_ppBkDC) {
			pdc->BitBlt(m_x + rc1.left, m_y + rc1.top, rc1.Width(), rc1.Height(), *m_ppBkDC, rc1.left, rc1.top, SRCCOPY);
		}
		rc2.OffsetRect(m_x, m_y);
		pdc->FillRect(&rc2, &br);
	}
}

void CFMMonitorUnit::DrawSPO2Wave(CDC* pdc)
{
	if (! m_pWaveSPO2) {
		return;
	}

	if (0 == m_nWaveSPO2_X) {
		return;
	}

	//压缩数据
	float scaleY = float(m_nWaveSPO2_H) / 256;
	BYTE arDispBuf[2000];
	int dispCount = min(2000, (int)(DISPLAY_SPO2_UNIT_SIZE * scaleY));
	int srcCount  = DISPLAY_SPO2_UNIT_SIZE;
	CompressData(arDispBuf, m_pWaveSPO2, dispCount, srcCount, scaleY);

	int nDrawLen = min(dispCount, m_nWaveSPO2_W);
	if (*m_ppBkDC) {
		int x = m_nWaveSPO2_X + m_nWaveSPO2_StartPos + 1;
		int y = m_nWaveSPO2_Y;
		int w1 = min(nDrawLen, m_nWaveSPO2_W - m_nWaveSPO2_StartPos - 1);
		int w2 = nDrawLen - w1;
		int h = m_nWaveSPO2_H;
		pdc->BitBlt(m_x+x, m_y+y, w1, h, *m_ppBkDC, x, y, SRCCOPY);
		if (w2 > 0) {
			pdc->BitBlt(m_x+m_nWaveSPO2_X, m_y+y, w2, h, *m_ppBkDC, m_nWaveSPO2_X, y, SRCCOPY);
		}
	}
	int i;
	int x = m_nWaveSPO2_X + m_nWaveSPO2_StartPos;
	for (i = 0; i < nDrawLen - 1; i ++) {
		int v1 = min(m_nWaveSPO2_H - 1, max(1, arDispBuf[i]));
		int v2 = min(m_nWaveSPO2_H - 1, max(1, arDispBuf[i+1]));
		DrawLineAA(pdc, m_crWaveSPO2,
			m_x + x,
			m_y + m_nWaveSPO2_Y + m_nWaveSPO2_H - v1,
			m_y + m_nWaveSPO2_Y + m_nWaveSPO2_H - v2);
		x ++;
		m_nWaveSPO2_StartPos ++;
		if (m_nWaveSPO2_StartPos >= m_nWaveSPO2_W - 1) {
			//绘制到达画布最右端时，跳转到画布的最左端
			m_nWaveSPO2_StartPos = 0;
			x = m_nWaveSPO2_X;
		}
	}

	//绘制终结线
	CBrush br(m_crWaveSPO2);
	CRect rc2(x+1, m_nWaveSPO2_Y,
		min(x+3, m_nWaveSPO2_X + m_nWaveSPO2_W), m_nWaveSPO2_Y + m_nWaveSPO2_H);
	CRect rc1(x+3, m_nWaveSPO2_Y, 
		min(x+15, m_nWaveSPO2_X + m_nWaveSPO2_W), m_nWaveSPO2_Y + m_nWaveSPO2_H);
	if (*m_ppBkDC) {
		pdc->BitBlt(m_x + rc1.left, m_y + rc1.top, rc1.Width(), rc1.Height(), *m_ppBkDC, rc1.left, rc1.top, SRCCOPY);
	}
	rc2.OffsetRect(m_x, m_y);
	pdc->FillRect(&rc2, &br);
}

void CFMMonitorUnit::DrawRESPWave(CDC* pdc)
{
	if (! m_pWaveRESP) {
		return;
	}

	if (0 == m_nWaveRESP_X) {
		return;
	}
	
	//压缩数据
	float scaleY = float(m_nWaveRESP_H) / 256;
	BYTE arDispBuf[2000];
	int dispCount = min(2000, (int)(DISPLAY_RESP_UNIT_SIZE * scaleY));
	int srcCount  = DISPLAY_RESP_UNIT_SIZE;
	CompressData(arDispBuf, m_pWaveRESP, dispCount, srcCount, scaleY);

	int nDrawLen = min (dispCount, m_nWaveRESP_W);
	if (*m_ppBkDC) {
		int x = m_nWaveRESP_X + m_nWaveRESP_StartPos + 1;
		int y = m_nWaveRESP_Y;
		int w1 = min(nDrawLen, m_nWaveRESP_W - m_nWaveRESP_StartPos - 1);
		int w2 = nDrawLen - w1;
		int h = m_nWaveRESP_H;
		pdc->BitBlt(m_x+x, m_y+y, w1, h, *m_ppBkDC, x, y, SRCCOPY);
		if (w2 > 0) {
			pdc->BitBlt(m_x+m_nWaveRESP_X, m_y+y, w2, h, *m_ppBkDC, m_nWaveRESP_X, y, SRCCOPY);
		}
	}
	int i;
	int x = m_nWaveRESP_X + m_nWaveRESP_StartPos;
	for (i = 0; i < dispCount - 1; i ++) {
		int v1 = min(m_nWaveRESP_H - 1, max(1, arDispBuf[i]));
		int v2 = min(m_nWaveRESP_H - 1, max(1, arDispBuf[i+1]));
		DrawLineAA(pdc, m_crWaveRESP,
			m_x + x,
			m_y + m_nWaveRESP_Y + m_nWaveRESP_H - v1,
			m_y + m_nWaveRESP_Y + m_nWaveRESP_H - v2);
		x ++;
		m_nWaveRESP_StartPos ++;
		if (m_nWaveRESP_StartPos >= m_nWaveRESP_W - 1) {
			//绘制到达画布最右端时，跳转到画布的最左端
			m_nWaveRESP_StartPos = 0;
			x = m_nWaveRESP_X;
		}
	}

	//绘制终结线
	CBrush br(m_crWaveRESP);
	CRect rc2(x+1, m_nWaveRESP_Y, 
		min(x+3, m_nWaveRESP_X + m_nWaveRESP_W), m_nWaveRESP_Y + m_nWaveRESP_H);
	CRect rc1(x+3, m_nWaveRESP_Y, 
		min(x+15, m_nWaveRESP_X + m_nWaveRESP_W), m_nWaveRESP_Y + m_nWaveRESP_H);
	if (*m_ppBkDC) {
		pdc->BitBlt(m_x + rc1.left, m_y + rc1.top, rc1.Width(), rc1.Height(), *m_ppBkDC, rc1.left, rc1.top, SRCCOPY);
	}
	rc2.OffsetRect(m_x, m_y);
	pdc->FillRect(&rc2, &br);
}

void CFMMonitorUnit::DrawFHRWave3(CDC* pdc)
{
	if (! m_pWaveFHR1 && ! m_pWaveFHR2 && ! m_pWaveTOCO) {
		return;
	}

	if (0 == m_nWaveFHR_X) {
		return;
	}
	
	// FHR表格的宽度，要按照半分钟对齐(60像素)
	int fhrW = (m_nWaveFHR_W + 1) / 60 * 60;

	//1秒钟2个点，x轴不用压缩
	int i, v1, v2;
	int arDispFHR1[DISPLAY_FHR_UNIT_SIZE];
	int arDispFHR2[DISPLAY_FHR_UNIT_SIZE];
	int arDispTOCO[DISPLAY_FHR_UNIT_SIZE];
	DISPLAY_FLAG FHR1Flag;
	DISPLAY_FLAG FHR2Flag;
	for (i = 0; i< DISPLAY_FHR_UNIT_SIZE; i ++) {
		if (INVALID_VALUE == m_pWaveFHR1[i]) {
			arDispFHR1[i] = 0;
		}
		else {
			arDispFHR1[i] = (int)((m_pWaveFHR1[i] - 30) * float(m_nWaveFHR_H) / 210);
		}

		if (INVALID_VALUE == m_pWaveFHR2[i]) {
			arDispFHR2[i] = 0;
		}
		else {
			arDispFHR2[i] = (int)((m_pWaveFHR2[i] - 30 - FHR1_FHR2_WAVE_GAP) * float(m_nWaveFHR_H) / 210);
		}

		arDispTOCO[i] = (int)(m_pWaveTOCO[i] * float(m_nWaveTOCO_H) / 100);
	}
	FHR1Flag.baseline = (0 == m_pDiagno1[0].baseline) ?
		0 : (int)((m_pDiagno1[0].baseline - 30) * float(m_nWaveFHR_H) / 210);
	FHR1Flag.flag_type = m_pDiagno1[0].flag_type;
	FHR2Flag.baseline = (0 == m_pDiagno2[0].baseline) ?
		0 : (int)((m_pDiagno2[0].baseline - 30 - FHR1_FHR2_WAVE_GAP) * float(m_nWaveFHR_H) / 210);
	FHR2Flag.flag_type = m_pDiagno2[0].flag_type;

	int nDrawLen = min(DISPLAY_FHR_UNIT_SIZE, fhrW);
	if (*m_ppBkDC) {
		int x = m_nWaveFHR_X + m_nWaveFHR_StartPos + 1;
		int fhrY = m_nWaveFHR_Y;
		int tocoY = m_nWaveTOCO_Y;
		int w1 = min(nDrawLen, fhrW - m_nWaveFHR_StartPos - 1);
		int w2 = nDrawLen - w1;
		int fhrH = m_nWaveFHR_H;
		int tocoH = m_nWaveTOCO_H;
		pdc->BitBlt(m_x+x, m_y+fhrY,  w1, fhrH,  *m_ppBkDC, x, fhrY,  SRCCOPY);
		pdc->BitBlt(m_x+x, m_y+tocoY, w1, tocoH, *m_ppBkDC, x, tocoY, SRCCOPY);
		if (w2 > 0) {
			pdc->BitBlt(m_x+m_nWaveFHR_X, m_y+fhrY,  w2, fhrH,  *m_ppBkDC, m_nWaveFHR_X, fhrY,  SRCCOPY);
			pdc->BitBlt(m_x+m_nWaveFHR_X, m_y+tocoY, w2, tocoH, *m_ppBkDC, m_nWaveFHR_X, tocoY, SRCCOPY);
		}
	}

	int nBreakGap = 30 * float(m_nWaveFHR_H) / 210;
	int x = m_nWaveFHR_X + m_nWaveFHR_StartPos;
	int nFhrX = m_x + x;
	int nFhr1Y;
	int nFhr2Y;
	for (i = 0; i < nDrawLen - 1; i ++) {

		if (0 != arDispFHR1[i] && 0 != arDispFHR1[i+1]) {
			//FHR1曲线
			v1 = min(m_nWaveFHR_H - 1, max(1, arDispFHR1[i]));
			v2 = min(m_nWaveFHR_H - 1, max(1, arDispFHR1[i+1]));
			nFhr1Y = m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v1;
			if (abs(v1 - v2) < nBreakGap) {
				//超过30就断线了
				DrawLine(pdc, m_crWaveFHR1,
					nFhrX + i,
					m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v1,
					m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v2);
			}
		}
		//绘制基线
		if (FHR1Flag.baseline > 0) {
			DrawLine(pdc, m_crFlagFHR1,
				nFhrX + i,
				m_y + m_nWaveFHR_Y + m_nWaveFHR_H - FHR1Flag.baseline,
				m_y + m_nWaveFHR_Y + m_nWaveFHR_H - FHR1Flag.baseline);
		}

		if (0 != arDispFHR2[i] && 0 != arDispFHR2[i+1]) {
			//FHR2曲线
			v1 = min(m_nWaveFHR_H - 1, max(1, arDispFHR2[i]));
			v2 = min(m_nWaveFHR_H - 1, max(1, arDispFHR2[i+1]));
			nFhr2Y = m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v1;
			if (abs(v1 - v2) < nBreakGap) {
				//超过30就断线了
				DrawLine(pdc, m_crWaveFHR2,
					nFhrX + i,
					m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v1,
					m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v2);
			}
		}
		//绘制基线
		if (FHR2Flag.baseline > 0) {
			DrawLine(pdc, m_crFlagFHR2,
				nFhrX + i,
				m_y + m_nWaveFHR_Y + m_nWaveFHR_H - FHR2Flag.baseline,
				m_y + m_nWaveFHR_Y + m_nWaveFHR_H - FHR2Flag.baseline);
		}

		//TOCO曲线
		v1 = min(m_nWaveTOCO_H - 1, max(1, arDispTOCO[i]));
		v2 = min(m_nWaveTOCO_H - 1, max(1, arDispTOCO[i+1]));
		DrawLine(pdc, m_crWaveTOCO,
			nFhrX + i,
			m_y + m_nWaveTOCO_Y + m_nWaveTOCO_H - v1,
			m_y + m_nWaveTOCO_Y + m_nWaveTOCO_H - v2);

		//绘制胎动标记
		if (m_pWaveFMMARK[0]) {
			DrawFMMark(pdc, x);
		}

		//绘制宫压归零标记
		if (m_pWaveTOCOReset[0]) {
			DrawTOCOResetMark(pdc, x);
		}

		//绘制自动诊断标记
		if (0 == i) {
			if (0 != FHR1Flag.flag_type) {
				DrawDiagnosisFlag(pdc, m_crFlagFHR1, nFhrX, nFhr1Y, FHR1Flag.flag_type);
			}
			if (0 != FHR2Flag.flag_type) {
				DrawDiagnosisFlag(pdc, m_crFlagFHR2, nFhrX, nFhr2Y, FHR2Flag.flag_type);
			}
		}

		x ++;
		m_nWaveFHR_StartPos ++;
		if (m_nWaveFHR_StartPos >= fhrW - 1) {
			//绘制到达画布最右端时，跳转到画布的最左端
			m_nWaveFHR_StartPos = 0;
			x = m_nWaveFHR_X;
			//此时把最左端的背景更新一下。（因为自动诊断的信息可能会画到边条上）
			if (*m_ppBkDC) {
				pdc->BitBlt(m_x, m_y + m_nWaveFHR_Y, m_nWaveFHR_X, m_nWaveFHR_H, *m_ppBkDC, 0, m_nWaveFHR_Y, SRCCOPY);
			}
		}
	}

	//绘制终结线
	if (*m_ppBkDC) {
		CRect rc1(x+3, m_nWaveFHR_Y,  min(x+15, m_nWaveFHR_X + m_nWaveFHR_W), m_nWaveFHR_Y + m_nWaveFHR_H);
		CRect rc2(x+3, m_nWaveTOCO_Y, min(x+15, m_nWaveFHR_X + m_nWaveFHR_W), m_nWaveTOCO_Y + m_nWaveTOCO_H);
		pdc->BitBlt(m_x + rc1.left, m_y + rc1.top, rc1.Width(), rc1.Height(), *m_ppBkDC, rc1.left, rc1.top, SRCCOPY);
		pdc->BitBlt(m_x + rc2.left, m_y + rc2.top, rc2.Width(), rc2.Height(), *m_ppBkDC, rc2.left, rc2.top, SRCCOPY);
	}
	CBrush br1(m_crWaveFHR1);
	CBrush br2(m_crWaveFHR2);
	CBrush br3(m_crWaveTOCO);
	CRect rc3(x+1, m_nWaveFHR_Y + m_nWaveFHR_H * 8 / 21, 
		min(x+3, m_nWaveFHR_X + m_nWaveFHR_W), m_nWaveFHR_Y + m_nWaveFHR_H * 12 / 21);
	CRect rc4(x+1, m_nWaveFHR_Y + m_nWaveFHR_H * (80  + FHR1_FHR2_WAVE_GAP) / 210,
		min(x+3, m_nWaveFHR_X + m_nWaveFHR_W), m_nWaveFHR_Y + m_nWaveFHR_H * (120 + FHR1_FHR2_WAVE_GAP) / 210);
	CRect rc5(x+1, m_nWaveTOCO_Y,
		min(x+3, m_nWaveFHR_X + m_nWaveFHR_W), m_nWaveTOCO_Y + m_nWaveTOCO_H);	
	rc3.OffsetRect(m_x, m_y);
	rc4.OffsetRect(m_x, m_y);
	rc5.OffsetRect(m_x, m_y);
	pdc->FillRect(&rc3, &br1);
	pdc->FillRect(&rc4, &br2);
	pdc->FillRect(&rc5, &br3);
}

void CFMMonitorUnit::DrawFHRWave1(CDC* pdc)
{
	if (! m_pWaveFHR1 && ! m_pWaveFHR2 && ! m_pWaveTOCO) {
		return;
	}

	if (0 == m_nWaveFHR_X) {
		return;
	}
	
	//速度为1cm/min时，每3秒钟才绘制一次
	m_nWaveFHR_Speed1Counter ++;
	if (m_nWaveFHR_Speed1Counter < 3) {
		return;
	}
	m_nWaveFHR_Speed1Counter = 0;

	// FHR表格的宽度，要按照半分钟对齐(60像素)
	int fhrW = (m_nWaveFHR_W + 1) / 60 * 60;

	//3秒钟2个点，需要
	int i, v1, v2;
	BYTE arDispFHR1[3];
	BYTE arDispFHR2[3];
	BYTE arDispTOCO[3];
	DISPLAY_FLAG FHR1Flag;
	DISPLAY_FLAG FHR2Flag;
	if (INVALID_VALUE == m_pWaveFHR1[-6]) {
		arDispFHR1[0] = 0;
	}
	else {
		arDispFHR1[0] = (int)((m_pWaveFHR1[-6] - 30) * float(m_nWaveFHR_H) / 210);
	}
	if (INVALID_VALUE == m_pWaveFHR2[-6]) {
		arDispFHR2[0] = 0;
	}
	else {
		arDispFHR2[0] = (int)((m_pWaveFHR2[-6] - 30 - FHR1_FHR2_WAVE_GAP) * float(m_nWaveFHR_H) / 210);
	}
	arDispTOCO[0] = (int)(m_pWaveTOCO[-6] * float(m_nWaveTOCO_H) / 100);

	if (INVALID_VALUE == m_pWaveFHR1[-2]) {
		arDispFHR1[1] = 0;
	}
	else {
		arDispFHR1[1] = (int)((m_pWaveFHR1[-2] - 30) * float(m_nWaveFHR_H) / 210);
	}
	if (INVALID_VALUE == m_pWaveFHR2[-2]) {
		arDispFHR2[1] = 0;
	}
	else {
		arDispFHR2[1] = (int)((m_pWaveFHR2[-2] - 30 - FHR1_FHR2_WAVE_GAP) * float(m_nWaveFHR_H) / 210);
	}
	arDispTOCO[1] = (int)(m_pWaveTOCO[-2] * float(m_nWaveTOCO_H) / 100);

	if (INVALID_VALUE == m_pWaveFHR1[2]) {
		arDispFHR1[2] = 0;
	}
	else {
		arDispFHR1[2] = (int)((m_pWaveFHR1[2] - 30) * float(m_nWaveFHR_H) / 210);
	}
	if (INVALID_VALUE == m_pWaveFHR2[2]) {
		arDispFHR2[2] = 0;
	}
	else {
		arDispFHR2[2] = (int)((m_pWaveFHR2[2] - 30 - FHR1_FHR2_WAVE_GAP) * float(m_nWaveFHR_H) / 210);
	}
	arDispTOCO[2] = (int)(m_pWaveTOCO[2] * float(m_nWaveTOCO_H) / 100);

	FHR1Flag.baseline = (0 == m_pDiagno1[0].baseline) ?
		0 : (int)((m_pDiagno1[0].baseline - 30) * float(m_nWaveFHR_H) / 210);
	FHR1Flag.flag_type = m_pDiagno1[0].flag_type;
	if (0 == FHR1Flag.flag_type) { FHR1Flag.flag_type = m_pDiagno1[-1].flag_type; }
	if (0 == FHR1Flag.flag_type) { FHR1Flag.flag_type = m_pDiagno1[-2].flag_type; }
	FHR2Flag.baseline = (0 == m_pDiagno2[0].baseline) ?
		0 : (int)((m_pDiagno2[0].baseline - 30 - FHR1_FHR2_WAVE_GAP) * float(m_nWaveFHR_H) / 210);
	FHR2Flag.flag_type = m_pDiagno2[0].flag_type;
	if (0 == FHR2Flag.flag_type) { FHR2Flag.flag_type = m_pDiagno2[-1].flag_type; }
	if (0 == FHR2Flag.flag_type) { FHR2Flag.flag_type = m_pDiagno2[-2].flag_type; }

	int nDrawLen = min(DISPLAY_FHR_UNIT_SIZE, fhrW);
	if (*m_ppBkDC) {
		int x = m_nWaveFHR_X + m_nWaveFHR_StartPos + 1;
		int fhrY = m_nWaveFHR_Y;
		int tocoY = m_nWaveTOCO_Y;
		int w1 = min(nDrawLen, fhrW - m_nWaveFHR_StartPos - 1);
		int w2 = nDrawLen - w1;
		int fhrH = m_nWaveFHR_H;
		int tocoH = m_nWaveTOCO_H;
		pdc->BitBlt(m_x+x, m_y+fhrY,  w1, fhrH,  *m_ppBkDC, x, fhrY,  SRCCOPY);
		pdc->BitBlt(m_x+x, m_y+tocoY, w1, tocoH, *m_ppBkDC, x, tocoY, SRCCOPY);
		if (w2 > 0) {
			pdc->BitBlt(m_x+m_nWaveFHR_X, m_y+fhrY,  w2, fhrH,  *m_ppBkDC, m_nWaveFHR_X, fhrY,  SRCCOPY);
			pdc->BitBlt(m_x+m_nWaveFHR_X, m_y+tocoY, w2, tocoH, *m_ppBkDC, m_nWaveFHR_X, tocoY, SRCCOPY);
		}
	}

	int nBreakGap = 30 * float(m_nWaveFHR_H) / 210;
	int x = m_nWaveFHR_X + m_nWaveFHR_StartPos;
	int nFhrX = m_x + x;
	int nFhr1Y;
	int nFhr2Y;
	for (i = 0; i < nDrawLen - 1; i ++) {

		if (0 != arDispFHR1[i] && 0 != arDispFHR1[i+1]) {
			//FHR1曲线
			v1 = min(m_nWaveFHR_H - 1, max(1, arDispFHR1[i]));
			v2 = min(m_nWaveFHR_H - 1, max(1, arDispFHR1[i+1]));
			nFhr1Y = m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v1;
			if (abs(v1 - v2) < nBreakGap) {
				//超过30就断线了
				DrawLine(pdc, m_crWaveFHR1,
					nFhrX + i,
					m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v1,
					m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v2);
			}
		}
		//绘制基线
		if (FHR1Flag.baseline > 0) {
			DrawLine(pdc, m_crFlagFHR1,
				nFhrX + i,
				m_y + m_nWaveFHR_Y + m_nWaveFHR_H - FHR1Flag.baseline,
				m_y + m_nWaveFHR_Y + m_nWaveFHR_H - FHR1Flag.baseline);
		}

		if (0 != arDispFHR2[i] && 0 != arDispFHR2[i+1]) {
			//FHR2曲线
			v1 = min(m_nWaveFHR_H - 1, max(1, arDispFHR2[i]));
			v2 = min(m_nWaveFHR_H - 1, max(1, arDispFHR2[i+1]));
			nFhr2Y = m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v1;
			if (abs(v1 - v2) < nBreakGap) {
				//超过30就断线了
				DrawLine(pdc, m_crWaveFHR2,
					nFhrX + i,
					m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v1,
					m_y + m_nWaveFHR_Y + m_nWaveFHR_H - v2);
			}
		}
		//绘制基线
		if (FHR2Flag.baseline > 0) {
			DrawLine(pdc, m_crFlagFHR2,
				nFhrX + i,
				m_y + m_nWaveFHR_Y + m_nWaveFHR_H - FHR2Flag.baseline,
				m_y + m_nWaveFHR_Y + m_nWaveFHR_H - FHR2Flag.baseline);
		}

		//TOCO曲线
		v1 = min(m_nWaveTOCO_H - 1, max(1, arDispTOCO[i]));
		v2 = min(m_nWaveTOCO_H - 1, max(1, arDispTOCO[i+1]));
		DrawLine(pdc, m_crWaveTOCO,
			nFhrX + i,
			m_y + m_nWaveTOCO_Y + m_nWaveTOCO_H - v1,
			m_y + m_nWaveTOCO_Y + m_nWaveTOCO_H - v2);

		//绘制胎动标记(相邻3秒钟只要发生1次胎动，就算有胎动)
		if (m_pWaveFMMARK[-2] ||
			m_pWaveFMMARK[-1] ||
			m_pWaveFMMARK[0] ) {
			DrawFMMark(pdc, x);
		}

		//绘制胎动标记(相邻3秒钟只要发生1次胎动，就算有胎动)
		if (m_pWaveTOCOReset[-2] ||
			m_pWaveTOCOReset[-1] ||
			m_pWaveTOCOReset[0] ) {
			DrawTOCOResetMark(pdc, x);
		}

		//绘制自动诊断标记
		if (0 == i) {
			if (0 != FHR1Flag.flag_type) {
				DrawDiagnosisFlag(pdc, m_crFlagFHR1, nFhrX, nFhr1Y, FHR1Flag.flag_type);
			}
			if (0 != FHR2Flag.flag_type) {
				DrawDiagnosisFlag(pdc, m_crFlagFHR2, nFhrX, nFhr2Y, FHR2Flag.flag_type);
			}
		}

		x ++;
		m_nWaveFHR_StartPos ++;
		if (m_nWaveFHR_StartPos >= fhrW - 1) {
			//绘制到达画布最右端时，跳转到画布的最左端
			m_nWaveFHR_StartPos = 0;
			x = m_nWaveFHR_X;
			//此时把最左端的背景更新一下。（因为自动诊断的信息可能会画到边条上）
			if (*m_ppBkDC) {
				pdc->BitBlt(m_x, m_y + m_nWaveFHR_Y, m_nWaveFHR_X, m_nWaveFHR_H, *m_ppBkDC, 0, m_nWaveFHR_Y, SRCCOPY);
			}
		}
	}

	//绘制终结线
	if (*m_ppBkDC) {
		CRect rc1(x+3, m_nWaveFHR_Y,  min(x+15, m_nWaveFHR_X + m_nWaveFHR_W), m_nWaveFHR_Y + m_nWaveFHR_H);
		CRect rc2(x+3, m_nWaveTOCO_Y, min(x+15, m_nWaveFHR_X + m_nWaveFHR_W), m_nWaveTOCO_Y + m_nWaveTOCO_H);
		pdc->BitBlt(m_x + rc1.left, m_y + rc1.top, rc1.Width(), rc1.Height(), *m_ppBkDC, rc1.left, rc1.top, SRCCOPY);
		pdc->BitBlt(m_x + rc2.left, m_y + rc2.top, rc2.Width(), rc2.Height(), *m_ppBkDC, rc2.left, rc2.top, SRCCOPY);
	}
	CBrush br1(m_crWaveFHR1);
	CBrush br2(m_crWaveFHR2);
	CBrush br3(m_crWaveTOCO);
	CRect rc3(x+1, m_nWaveFHR_Y + m_nWaveFHR_H * 8 / 21, 
		min(x+3, m_nWaveFHR_X + m_nWaveFHR_W), m_nWaveFHR_Y + m_nWaveFHR_H * 12 / 21);
	CRect rc4(x+1, m_nWaveFHR_Y + m_nWaveFHR_H * (80  + FHR1_FHR2_WAVE_GAP) / 210,
		min(x+3, m_nWaveFHR_X + m_nWaveFHR_W), m_nWaveFHR_Y + m_nWaveFHR_H * (120 + FHR1_FHR2_WAVE_GAP) / 210);
	CRect rc5(x+1, m_nWaveTOCO_Y,
		min(x+3, m_nWaveFHR_X + m_nWaveFHR_W), m_nWaveTOCO_Y + m_nWaveTOCO_H);	
	rc3.OffsetRect(m_x, m_y);
	rc4.OffsetRect(m_x, m_y);
	rc5.OffsetRect(m_x, m_y);
	pdc->FillRect(&rc3, &br1);
	pdc->FillRect(&rc4, &br2);
	pdc->FillRect(&rc5, &br3);

	//绘制胎心率基线
	if (m_nFHRBaseline1 > 30 && m_nFHRBaseline1 < 240) {
		CPen pen(PS_SOLID, 1, m_crFlagFHR1);
		CPen* pOldPen = pdc->SelectObject(&pen);
		int y = min(m_nWaveFHR_H - 1, max(1, (int)((m_pWaveFHR2[i] - 30) * float(m_nWaveFHR_H) / 210)));
		pdc->MoveTo(m_x, y);
		pdc->LineTo(m_x + fhrW, y);
		pdc->SelectObject(pOldPen);
	}
	if (m_nFHRBaseline2 > 30 && m_nFHRBaseline2 < 240) {
		CPen pen(PS_SOLID, 1, m_crFlagFHR2);
		CPen* pOldPen = pdc->SelectObject(&pen);
		int y = min(m_nWaveFHR_H - 1, max(1, (int)((m_pWaveFHR2[i] - 30 - FHR1_FHR2_WAVE_GAP) * float(m_nWaveFHR_H) / 210)));
		pdc->MoveTo(m_x, y);
		pdc->LineTo(m_x + fhrW, y);
		pdc->SelectObject(pOldPen);
	}
}

void CFMMonitorUnit::DrawFMMark(CDC* pdc, int x)
{
	const int nMarkWidth = 2;
	const int nArrawSize = 7;
	int i, j;
	for (i = 0; i < nArrawSize; i ++) {
		for (j = nMarkWidth - i; j <= i + nMarkWidth; j ++) {
			pdc->SetPixel(m_x + x + j, m_y + m_nWaveTOCO_Y + i + 1, m_crWaveMARK);
		}
	}
	CBrush br(m_crWaveMARK);
	CRect rc(x, m_nWaveTOCO_Y + nArrawSize + 1, x + nMarkWidth, m_nWaveTOCO_Y + m_nWaveTOCO_H / 4);
	rc.OffsetRect(m_x, m_y);
	pdc->FillRect(&rc, &br);
}

void CFMMonitorUnit::DrawTOCOResetMark(CDC* pdc, int x)
{
	const int nMarkWidth = 2;
	const int nArrawSize = 9;
	int i, j;
	int nMarkPosY = m_y + m_nWaveTOCO_Y + m_nWaveTOCO_H - nArrawSize - 1;
	for (i = 0; i < nArrawSize; i ++) {
		for (j = nMarkWidth - i; j <= i + nMarkWidth; j ++) {
			pdc->SetPixel(m_x + x + j - nArrawSize, nMarkPosY + i + 1, m_crWaveMARK);
			pdc->SetPixel(m_x + x + j - nArrawSize, nMarkPosY - i - 1, m_crWaveMARK);
		}
	}
}

void CFMMonitorUnit::DrawTechAlarm(CDC* pdc)
{
	CRect rc(m_nTechAlarmX, m_nTechAlarmY, m_nTechAlarmX + m_nTechAlarmW, m_nTechAlarmY + m_nTechAlarmH);
	if (0 == m_nECGError &&
		0 == m_nRESPError &&
		0 == m_nSPO2Error &&
		0 == m_nNIBPError &&
		0 == m_nTEMPError &&
		0 == m_nFHRError) {
		if (0 != m_nCurErrorIndex) {
			//上次有报警，这次没报警，则擦除报警区域
			if (*m_ppBkDC) {
				pdc->BitBlt(m_x + rc.left, m_y + rc.top, rc.Width(), rc.Height(),
					(*m_ppBkDC), rc.left, rc.top, SRCCOPY);
			}
			m_nCurErrorIndex = 0;
		}
		return;
	}

	//背景
	rc.OffsetRect(m_x, m_y);
	//技术报警是黄色的
	CBrush br(RGB(220,220,0));
	pdc->FillRect(&rc, &br);

	//构造文字
newSearch:
	switch (m_nCurErrorIndex) {
	case 0:
		if (m_nECGError  > 0) { m_nCurErrorIndex = 1; break; }
	case 1:
		if (m_nRESPError > 0) { m_nCurErrorIndex = 2; break; }
	case 2:
		if (m_nSPO2Error > 0) { m_nCurErrorIndex = 3; break; }
	case 3:
		if (m_nNIBPError > 0) { m_nCurErrorIndex = 4; break; }
	case 4:
		if (m_nTEMPError > 0) { m_nCurErrorIndex = 5; break; }
	case 5:
		if (m_nFHRError  > 0) { m_nCurErrorIndex = 6; break; }
	case 6:
		m_nCurErrorIndex = 0; goto newSearch;
	}
	CString sErrorInfo = GetTechErrorString();

	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(RGB(0,0,0));
	pdc->TextOut(rc.left + nTextW*3, rc.top + (rc.Height() - nTextH) / 2, sErrorInfo);
}

CString CFMMonitorUnit::GetTechErrorString()
{
	switch (m_nCurErrorIndex) {
	case 1:
		return CFMDict::ErrECGLead_itos(m_nECGError);
	case 2:
		return CFMDict::ErrRESP_itos(m_nRESPError);
	case 3:
		return CFMDict::ErrSPO2_itos(m_nSPO2Error);
	case 4:
		return CFMDict::ErrNIBP_itos(m_nNIBPError);
	case 5:
		return CFMDict::ErrTEMP_itos(m_nTEMPError);
	case 6:
		return CFMDict::ErrFHR_itos(m_nFHRError);
	default:
		break;
	}
	return _T("");
}

void CFMMonitorUnit::DrawBioAlarm(CDC* pdc)
{
	CRect rc(m_nBioAlarmX, m_nBioAlarmY, m_nBioAlarmX + m_nBioAlarmW, m_nBioAlarmY + m_nBioAlarmH);
	//生理报警要根据等级选择背景颜色
	if (0 == m_nBioAlarmLevel) {
		if (0 != m_nOldBioAlarmLevel) {
			if (*m_ppBkDC) {
				pdc->BitBlt(m_x + rc.left, m_y + rc.top, rc.Width(), rc.Height(),
					(*m_ppBkDC), rc.left, rc.top, SRCCOPY);
			}
			m_nOldBioAlarmLevel = 0;
		}
		return;
	}
	m_nOldBioAlarmLevel = m_nBioAlarmLevel;

	rc.OffsetRect(m_x, m_y);
	if (m_nBioAlarmLevel < 3) {
		CBrush br(RGB(220,220,0));
		pdc->FillRect(&rc, &br);
	}
	else {
		CBrush br(RGB(255,0,0));
		pdc->FillRect(&rc, &br);
	}
	CBrush brBlack(RGB(0,0,0));
	CRect rcBlack(rc);
	rcBlack.right = rcBlack.left + 2;
	pdc->FillRect(&rcBlack, &brBlack);

	CString sLevelStarts = _T("***");
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(RGB(0,0,0));
	pdc->TextOut(rc.left + 4, rc.top + (rc.Height() - nTextH) / 2, sLevelStarts.Left(m_nBioAlarmLevel));

	CString sErrorInfo = GetBioAlarmString();
	pdc->TextOut(rc.left + nTextW*3, rc.top + (rc.Height() - nTextH) / 2, sErrorInfo);
}

CString CFMMonitorUnit::GetBioAlarmString()
{
	switch (m_nBioAlarmType) {

	case BIO_ALARM_HR:
		if (m_nHR > m_nHRLimitH) { return TEXT_HR_TOO_HIGH; }
		if (m_nHR < m_nHRLimitL) { return TEXT_HR_TOO_LOW; }
		break;

	case BIO_ALARM_ARR:
		return CFMDict::ECGArrMode_itos(m_nBioAlarmLevel);

	case BIO_ALARM_PVC:
		if (m_nPVC > m_nPVCLimitH) { return TEXT_PVC_TOO_HIGH; }
		break;

	case BIO_ALARM_ST:
		if (m_nST1 > m_nST1LimitH) { return TEXT_ST1_TOO_HIGH; }
		if (m_nST1 < m_nST1LimitL) { return TEXT_ST1_TOO_LOW; }
		if (m_nST2 > m_nST2LimitH) { return TEXT_ST2_TOO_HIGH; }
		if (m_nST2 < m_nST2LimitL) { return TEXT_ST2_TOO_LOW; }
		break;

	case BIO_ALARM_RR:
		if (m_nRR > m_nRRLimitH) { return TEXT_RR_TOO_HIGH; }
		if (m_nRR < m_nRRLimitL) { return TEXT_RR_TOO_LOW; }
		break;

	case BIO_ALARM_PR:
		if (m_nPR > m_nPRLimitH) { return TEXT_PR_TOO_HIGH; }
		if (m_nPR < m_nPRLimitL) { return TEXT_PR_TOO_LOW; }
		break;

	case BIO_ALARM_SPO2:
		if (m_nSPO2 > m_nSPO2LimitH) { return TEXT_SPO2_TOO_HIGH; }
		if (m_nSPO2 < m_nSPO2LimitL) { return TEXT_SPO2_TOO_LOW; }
		break;

	case BIO_ALARM_NIBP:
		if (m_nNIBP_S > m_nNIBPLimitH) { return TEXT_SYS_TOO_HIGH; }
		if (m_nNIBP_S < m_nNIBPLimitL) { return TEXT_SYS_TOO_LOW; }
		if (m_nNIBP_M > m_nNIBP_M_LimitH) { return TEXT_MEA_TOO_HIGH; }
		if (m_nNIBP_M < m_nNIBP_M_LimitL) { return TEXT_MEA_TOO_LOW; }
		if (m_nNIBP_D > m_nNIBP_D_LimitH) { return TEXT_DIA_TOO_HIGH; }
		if (m_nNIBP_D < m_nNIBP_D_LimitL) { return TEXT_DIA_TOO_LOW; }
		break;

	case BIO_ALARM_TEMP:
		if (m_nT1 > m_nT1LimitH) { return TEXT_T1_TOO_HIGH; }
		if (m_nT1 < m_nT1LimitL) { return TEXT_T1_TOO_LOW; }
		if (m_nT2 > m_nT2LimitH) { return TEXT_T2_TOO_HIGH; }
		if (m_nT2 < m_nT2LimitL) { return TEXT_T2_TOO_LOW; }
		break;

	case BIO_ALARM_FHR:
		if (m_nFHR1 > m_nFHRLimitH) { return TEXT_FHR1_TOO_HIGH; }
		if (m_nFHR1 < m_nFHRLimitL) { return TEXT_FHR1_TOO_LOW; }
		if (m_nFHR1 > m_nFHRLimitH) { return TEXT_FHR2_TOO_HIGH; }
		if (m_nFHR1 < m_nFHRLimitL) { return TEXT_FHR2_TOO_LOW; }
		break;
	}

	return TEXT_ALARM_UNKNOWN;
}

void CFMMonitorUnit::FillUpFHRWave(CDC* pdc)
{
	if (MONITOR_TYPE_ADULT == GetType()) {
		return;
	}

	// FHR表格的宽度，要按照半分钟对齐(60像素)
	int fhrW = (m_nWaveFHR_W + 1) / 60 * 60;

	//根据走纸速度和表格宽度推算一屏的时间跨度(秒数)
	int nSecondsPerScreen = 0;
	switch (m_nFHRPaperSpeedOpt) {
	case FHR_PAPER_SPEED_1CM:
		nSecondsPerScreen = fhrW / 2 * 3;
		break;
	case FHR_PAPER_SPEED_3CM:
		nSecondsPerScreen = fhrW / 2;
		break;
	}

	m_nLastGetDataTimeSpan = m_pUnit->GetDisplayLenOfSecond();
	if (m_nLastGetDataTimeSpan < 1) {
		return;
	}
	int nDrawLenSeconds = min(m_nLastGetDataTimeSpan, nSecondsPerScreen);
	if (m_nLastGetDataTimeSpan <= nSecondsPerScreen) {
		//不到一屏的情况
		m_nWaveFHR_StartPos = 0;
		m_nWaveFHR_Speed1Counter = 0;

		int i;
		for (i = 0; i < m_nLastGetDataTimeSpan; i ++) {
			RenewFHRWave(i);
			DrawFHRWave(pdc);
			//不是每次绘制都会向后推m_nWaveFHR_StartPos
			//当纸速为3cm/min时，每3次之中只绘制1次
		}
	}
	else {
		//超过一屏
		int nTimeSpan = m_nLastGetDataTimeSpan - nSecondsPerScreen;
		m_nWaveFHR_StartPos = 0;
		switch (m_nFHRPaperSpeedOpt) {
		case FHR_PAPER_SPEED_1CM:
			m_nWaveFHR_StartPos = (m_nLastGetDataTimeSpan % nSecondsPerScreen) * 2 / 3;
			break;
		case FHR_PAPER_SPEED_3CM:
			m_nWaveFHR_StartPos = (m_nLastGetDataTimeSpan % nSecondsPerScreen) * 2;
			break;
		}
		m_nWaveFHR_Speed1Counter = m_nLastGetDataTimeSpan % 3;

		int i;
		for (i = 0; i < nSecondsPerScreen; i ++) {
			RenewFHRWave(nTimeSpan + i);
			DrawFHRWave(pdc);
			//不是每次绘制都会向后推m_nWaveFHR_StartPos
			//当纸速为3cm/min时，每3次之中只绘制1次
		}
	}
}

void CFMMonitorUnit::DrawFHRTimeRuler(CDC* pdc)
{
	if (MONITOR_TYPE_ADULT == GetType()) {
		return;
	}

	// FHR表格的宽度，要按照半分钟对齐(60像素)
	int fhrW = (m_nWaveFHR_W + 1) / 60 * 60;
	int limitH = m_nWaveTOCO_Y - (m_nWaveFHR_Y + m_nWaveFHR_H);
	CTime tmStart = m_pUnit->GetStartTime();
	int nLenSecond = m_pUnit->GetDisplayLenOfSecond();

	//根据走纸速度和表格宽度推算一屏的时间跨度(秒数)
	int nSecondsPerScreen = 0;
	int nSecondsPerScale = 0;
	switch (m_nFHRPaperSpeedOpt) {
	case FHR_PAPER_SPEED_1CM:
		nSecondsPerScreen = fhrW / 2 * 3;
		nSecondsPerScale = 180;//秒，两个刻度之间距离3分钟
		break;
	case FHR_PAPER_SPEED_3CM:
		nSecondsPerScreen = fhrW / 2;
		nSecondsPerScale = 60;//秒，两个刻度之间距离1分钟
		break;
	}

	m_pView->SelectTimeRulerFont();
	if (nLenSecond <= nSecondsPerScreen) {
		//不到一屏的情况
		int i;
		int count = fhrW / 120;
		for (i = 0; i <= count; i ++) {
			CTimeSpan ts(0, 0, 0, nSecondsPerScale * i);
			CTime tm = tmStart + ts;
			DrawTimeMark(pdc, m_crTimeMark, tm, //时间值
				m_nWaveFHR_X + 120 * i, m_nWaveFHR_Y + m_nWaveFHR_H,//x和y值
				limitH);//允许的绘制高度
		}
	}
	else {
		//超过一屏，要分两部分绘制时间轴
		int nStartTm = nLenSecond / nSecondsPerScreen * nSecondsPerScreen; //当前屏起始时间
		int nCurrent = nLenSecond % nSecondsPerScreen; //当前位置相对于起始时间的距离
		int i;
		int count = fhrW / 120;
		for (i = 0; i <= count; i ++) {
			CTime tm;
			int nMarkPos = nSecondsPerScale * i;
			if (nMarkPos <= nCurrent) {
				//当前点之前的时间轴
				CTimeSpan ts(0, 0, 0, nStartTm + nMarkPos);
				tm = tmStart + ts;
				DrawTimeMark(pdc, m_crTimeMark, tm, //时间值
					m_nWaveFHR_X + 120 * i, m_nWaveFHR_Y + m_nWaveFHR_H,//x和y值
					limitH);//允许的绘制高度
			}
			else {
				//当前点之后的时间轴（采用前一屏的时间）
				CTimeSpan ts(0, 0, 0, nStartTm + nMarkPos - nSecondsPerScreen);
				tm = tmStart + ts;
				DrawTimeMark(pdc, m_crTimeMarkDark, tm, //时间值
					m_nWaveFHR_X + 120 * i, m_nWaveFHR_Y + m_nWaveFHR_H,//x和y值
					limitH);//允许的绘制高度
			}
		}
	}
	m_pView->ResetFont();
}

void CFMMonitorUnit::DrawTimeMark(CDC* pdc, COLORREF cr, CTime& tm, int x, int y, int limitH)
{
	const int fontW = 9;
	const int fontH = 16;
	int i;

	if (*m_ppBkDC) {
		CRect rc(x, y, x + fontW * 5, y + limitH);
		pdc->BitBlt(m_x + rc.left, m_y + rc.top, rc.Width(), rc.Height(), *m_ppBkDC, rc.left, rc.top, SRCCOPY);
	}
	for (i = 0; i < limitH - 2; i ++) {
		pdc->SetPixel(m_x + x, m_y + y + i + 1, cr);
	}
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(cr);
	pdc->TextOut(m_x + x + 1, m_y + y + (limitH - fontH) / 2, tm.Format(_T("%H:%M")));
}

void CFMMonitorUnit::DrawFocus(CDC* pdc)
{
	if (m_bVisible) {
		int w = FRAME_WIDTH_FOCUS;
		int h = FRAME_WIDTH_FOCUS;
		CRect rc1(CPoint(m_x, m_y), CSize(m_w, h));
		CRect rc2(CPoint(m_x, m_y + h), CSize(w, m_h - h - h));
		CRect rc3(CPoint(m_x + m_w - w, m_y + h), CSize(w, m_h - h - h));
		CRect rc4(CPoint(m_x, m_y + m_h - h), CSize(m_w, h));
		CBrush br(RGB(192, 255, 128));
		pdc->FillRect(&rc1, &br);
		pdc->FillRect(&rc2, &br);
		pdc->FillRect(&rc3, &br);
		pdc->FillRect(&rc4, &br);
	}
}

void CFMMonitorUnit::DrawFrame(CDC* pdc)
{
	if (m_bVisible) {
		int w = FRAME_WIDTH_NORMAL;
		int h = FRAME_WIDTH_NORMAL;
		CRect rc1(CPoint(m_x, m_y), CSize(m_w, h));
		CRect rc2(CPoint(m_x, m_y + h), CSize(w, m_h - h - h));
		CRect rc3(CPoint(m_x + m_w - w, m_y + h), CSize(w, m_h - h - h));
		CRect rc4(CPoint(m_x, m_y + m_h - h), CSize(m_w, h));
		CBrush br(RGB(92, 128, 255));
		pdc->FillRect(&rc1, &br);
		pdc->FillRect(&rc2, &br);
		pdc->FillRect(&rc3, &br);
		pdc->FillRect(&rc4, &br);
	}
}

void CFMMonitorUnit::DrawCheckArea(CDC* pdc)
{
	CRect rc;
	CBrush br(RGB(0, 92, 255));
	if (MONITOR_TYPE_ADULT == GetType() ||
	MONITOR_TYPE_BOTH == GetType()) {
		rc = m_rcECG;	rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		rc = m_rcRESP;	rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		rc = m_rcSPO2;	rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		rc = m_rcNIBP;	rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		rc = m_rcTEMP;	rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		//检查曲线绘制区域
		if (0 != m_nWaveECG1_Y) {
			CBrush br(m_crWaveECG1);
			CRect rc(m_nWaveECG_X, m_nWaveECG1_Y, m_nWaveECG_X + m_nWaveECG_W, m_nWaveECG1_Y + m_nWaveECG_H);
			rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		}
		if (0 != m_nWaveECG2_Y) {
			CBrush br(m_crWaveECG2);
			CRect rc(m_nWaveECG_X, m_nWaveECG2_Y, m_nWaveECG_X + m_nWaveECG_W, m_nWaveECG2_Y + m_nWaveECG_H);
			rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		}
		if (0 != m_nWaveSPO2_Y) {
			CBrush br(m_crWaveSPO2);
			CRect rc(m_nWaveSPO2_X, m_nWaveSPO2_Y, m_nWaveSPO2_X + m_nWaveSPO2_W, m_nWaveSPO2_Y + m_nWaveSPO2_H);
			rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		}
		if (0 != m_nWaveRESP_Y) {
			CBrush br(m_crWaveRESP);
			CRect rc(m_nWaveRESP_X, m_nWaveRESP_Y, m_nWaveRESP_X + m_nWaveRESP_W, m_nWaveRESP_Y + m_nWaveRESP_H);
			rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		}
	}
	if (MONITOR_TYPE_FETUS == GetType() ||
		MONITOR_TYPE_BOTH == GetType()) {
		rc = m_rcFHR;	rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		//检查曲线绘制区域
		if (0 != m_nWaveFHR_Y) {
			CBrush br(m_crWaveFHR1);
			CRect rc(m_nWaveFHR_X, m_nWaveFHR_Y, m_nWaveFHR_X + m_nWaveFHR_W, m_nWaveFHR_Y + m_nWaveFHR_H);
			rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		}
		if (0 != m_nWaveTOCO_Y) {
			CBrush br(m_crWaveTOCO);
			CRect rc(m_nWaveFHR_X, m_nWaveTOCO_Y, m_nWaveFHR_X + m_nWaveFHR_W, m_nWaveTOCO_Y + m_nWaveTOCO_H);
			rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
		}
	}
	//检查Info区
	{
		CBrush br(m_crInfoText);
		CRect rc(m_nInfoBarX, m_nInfoBarY, m_nInfoBarX + m_nInfoBarW, m_nInfoBarY + m_nInfoBarH);
		rc.OffsetRect(m_x, m_y); pdc->FrameRect(&rc, &br);
	}
	//检查报警区域
	{
		CBrush br(RGB(0,0,255));
		CRect rc(m_nTechAlarmX, m_nTechAlarmY, m_nTechAlarmX + m_nTechAlarmW, m_nTechAlarmY + m_nTechAlarmH);
		rc.OffsetRect(m_x, m_y); pdc->FillRect(&rc, &br);
	}
	{
		CBrush br(RGB(255,0,0));
		CRect rc(m_nBioAlarmX, m_nBioAlarmY, m_nBioAlarmX + m_nBioAlarmW, m_nBioAlarmY + m_nBioAlarmH);
		rc.OffsetRect(m_x, m_y); pdc->FillRect(&rc, &br);
	}
}

void CFMMonitorUnit::DrawDiagnosisFlag(CDC* pdc, COLORREF cr, int x, int y, int type)
{
	if (0 == type) {
		return;
	}
	pdc->SetTextColor(cr);
	int nTextY;
	int nTextX;
	CString sText;
	if (type > 0) {
		nTextY = y - nSmlTextH * 1.5;
		sText = CFMDict::FHRAutodiagnoAccel_itos(type);
	}
	else {
		nTextY = y + nSmlTextH * 0.5;
		sText = CFMDict::FHRAutodiagnoDecel_itos(-type);
	}

	//需要往文字的前方绘制，因为当前位置后方会被新绘曲线擦掉
	nTextX = max(m_x, x - nTextW * sText.GetLength());
	nTextY = max(m_y + m_nWaveFHR_Y - nSmlTextH, min(nTextY, m_y + m_nWaveFHR_Y + m_nWaveFHR_H - nSmlTextH));
	pdc->TextOut(nTextX, nTextY, sText);
}

void CFMMonitorUnit::SetPos(int x, int y, int w, int h, int nAreaType, BOOL bVisible)
{
	m_x = x;
	m_y = y;
	m_w = w;
	m_h = h;
	m_bVisible = bVisible;
	m_nAreaType = nAreaType;
}

void CFMMonitorUnit::ResetWavePos()
{
	m_nWaveECG_StartPos = 0;
	m_nWaveSPO2_StartPos = 0;
	m_nWaveRESP_StartPos = 0;
	m_nWaveFHR_StartPos = 0;
}

BOOL CFMMonitorUnit::PtInArea(CPoint pt)
{
	CRect rc(CPoint(m_x, m_y), CSize(m_w, m_h));
	return PtInRect(&rc, pt);
}

void CFMMonitorUnit::SetType(int type)
{
	if (m_pUnit) {
		m_pUnit->m_type = type;
	}
	m_nTypeDebug = type;
	m_bRedrawBkgd = TRUE;
}

int CFMMonitorUnit::GetType()
{
	if (m_pUnit) {
		return m_pUnit->m_type;
	}
	else {
		return m_nTypeDebug;
	}
}
void CFMMonitorUnit::RenewBkConfig()
{
	//非全屏模式没有混合显示方式，切换为胎儿监护
	if (MONITOR_AREA_1 != m_nAreaType &&
		MONITOR_TYPE_BOTH == GetType()) {
			SetType(MONITOR_TYPE_FETUS);
	}

	if (m_nOldType != GetType() || m_nAreaType != m_nOldAreaType) {
		//根据type修改背景图以及各种数字的显示位置等信息
		int nIndex;
		switch (GetType()) {
		case MONITOR_TYPE_ADULT:
			switch (m_nAreaType) {
			case MONITOR_AREA_1: nIndex = INDEX_A1; break;
			case MONITOR_AREA_2: nIndex = INDEX_A2; break;
			case MONITOR_AREA_3: nIndex = INDEX_A3; break;
			case MONITOR_AREA_4: nIndex = INDEX_A4; break;
			case MONITOR_AREA_5: nIndex = INDEX_A5; break;
			}
			m_ppBkDC = &(m_pView->m_arBkDC[nIndex]);
			SetNumberResForAdult(nIndex);
			break;
		case MONITOR_TYPE_FETUS:
			switch (m_nAreaType) {
			case MONITOR_AREA_1: nIndex = INDEX_F1; break;
			case MONITOR_AREA_2: nIndex = INDEX_F2; break;
			case MONITOR_AREA_3: nIndex = INDEX_F3; break;
			case MONITOR_AREA_4: nIndex = INDEX_F4; break;
			case MONITOR_AREA_5: nIndex = INDEX_F5; break;
			}
			m_ppBkDC = &(m_pView->m_arBkDC[nIndex]);
			SetNumberResForFetus(nIndex);
			break;
		case MONITOR_TYPE_BOTH:
			nIndex = INDEX_FA;
			m_ppBkDC = &(m_pView->m_arBkDC[nIndex]);
			SetNumberResForAdult(nIndex);
			SetNumberResForFetus(nIndex);
			break;
		}
		m_nOldType = GetType();
		m_nOldAreaType = m_nAreaType;
	}

	//刷新背景
	m_bRedrawBkgd = TRUE;

	//绘制曲线的位置清零
	//add:20150403；解决了切换显示模式后曲线残留的问题
	ResetWavePos();
}

void CFMMonitorUnit::SetNumberResForAdult(int nIndex)
{
	int R, G, B;
	CIniInt* pi = m_pView->m_arBkIni[nIndex];

	// 数值的绘图位置
	m_stBedNum.type = pi->Get(_T("BED_NUM_FONT"));
	m_stBedNum.x = pi->Get(_T("BED_NUM_X"));
	m_stBedNum.y = pi->Get(_T("BED_NUM_Y"));
	m_nInfoBarX = pi->Get(_T("INFO_BAR_X"));
	m_nInfoBarY = pi->Get(_T("INFO_BAR_Y"));
	m_nInfoBarW = pi->Get(_T("INFO_BAR_W"));
	m_nInfoBarH = pi->Get(_T("INFO_BAR_H"));
	R = pi->Get(_T("INFO_TEXT_COLOR_R"));
	G = pi->Get(_T("INFO_TEXT_COLOR_G"));
	B = pi->Get(_T("INFO_TEXT_COLOR_B"));
	m_crInfoText = RGB(R, G, B);

	m_stBedNum.type = pi->Get(_T("BED_NUM_FONT"));
	m_stBedNum.x = pi->Get(_T("BED_NUM_X"));
	m_stBedNum.y = pi->Get(_T("BED_NUM_Y"));
	m_stHRFlag.type = pi->Get(_T("HR_FLAG_FONT"));
	m_stHRFlag.x = pi->Get(_T("HR_FLAG_X"));
	m_stHRFlag.y = pi->Get(_T("HR_FLAG_Y"));
	m_stHR.type = pi->Get(_T("HR_NUM_FONT"));
	m_stHR.x = pi->Get(_T("HR_NUM_X"));
	m_stHR.y = pi->Get(_T("HR_NUM_Y"));
	m_stHRLimitH.type = pi->Get(_T("HR_LIMIT_FONT"));
	m_stHRLimitH.x = pi->Get(_T("HR_LIMIT_H_X"));
	m_stHRLimitH.y = pi->Get(_T("HR_LIMIT_H_Y"));
	m_stHRLimitL.type = pi->Get(_T("HR_LIMIT_FONT"));
	m_stHRLimitL.x = pi->Get(_T("HR_LIMIT_L_X"));
	m_stHRLimitL.y = pi->Get(_T("HR_LIMIT_L_Y"));
	m_stECGGainOpt.type = pi->Get(_T("ECG_GAIN_FONT"));
	m_stECGGainOpt.x = pi->Get(_T("ECG_GAIN_X"));
	m_stECGGainOpt.y = pi->Get(_T("ECG_GAIN_Y"));
	m_stECG1LeadOpt.type = pi->Get(_T("ECG_LEAD_FONT"));
	m_stECG1LeadOpt.x = pi->Get(_T("ECG1_LEAD_X"));
	m_stECG1LeadOpt.y = pi->Get(_T("ECG1_LEAD_Y"));
	m_stECG2LeadOpt.type = pi->Get(_T("ECG_LEAD_FONT"));
	m_stECG2LeadOpt.x = pi->Get(_T("ECG2_LEAD_X"));
	m_stECG2LeadOpt.y = pi->Get(_T("ECG2_LEAD_Y"));
	m_stECGFilter.x = pi->Get(_T("ECG_FILTER_X"));
	m_stECGFilter.y = pi->Get(_T("ECG_FILTER_Y"));
	m_stECGSpeed.x = pi->Get(_T("ECG_SPEED_X"));
	m_stECGSpeed.y = pi->Get(_T("ECG_SPEED_Y"));

	m_stNIBP_S.type = pi->Get(_T("NIBP_NUM_S_FONT"));
	m_stNIBP_S.sub_type = pi->Get(_T("NIBP_NUM_S_FONT_SMALL"));
	m_stNIBP_S.x = pi->Get(_T("NIBP_NUM_S_X"));
	m_stNIBP_S.y = pi->Get(_T("NIBP_NUM_S_Y"));
	m_stNIBP_M.type = pi->Get(_T("NIBP_NUM_M_FONT"));
	m_stNIBP_M.sub_type = pi->Get(_T("NIBP_NUM_M_FONT_SMALL"));
	m_stNIBP_M.x = pi->Get(_T("NIBP_NUM_M_X"));
	m_stNIBP_M.y = pi->Get(_T("NIBP_NUM_M_Y"));
	m_stNIBP_D.type = pi->Get(_T("NIBP_NUM_D_FONT"));
	m_stNIBP_D.sub_type = pi->Get(_T("NIBP_NUM_D_FONT_SMALL"));
	m_stNIBP_D.x = pi->Get(_T("NIBP_NUM_D_X"));
	m_stNIBP_D.y = pi->Get(_T("NIBP_NUM_D_Y"));
	m_stNIBPLimitH.type = pi->Get(_T("NIBP_LIMIT_FONT"));
	m_stNIBPLimitH.x = pi->Get(_T("NIBP_LIMIT_H_X"));
	m_stNIBPLimitH.y = pi->Get(_T("NIBP_LIMIT_H_Y"));
	m_stNIBPLimitL.type = pi->Get(_T("NIBP_LIMIT_FONT"));
	m_stNIBPLimitL.x = pi->Get(_T("NIBP_LIMIT_L_X"));
	m_stNIBPLimitL.y = pi->Get(_T("NIBP_LIMIT_L_Y"));
	m_stNIBPUnit.type = pi->Get(_T("NIBP_UNIT_FONT"));
	m_stNIBPUnit.x = pi->Get(_T("NIBP_UNIT_X"));
	m_stNIBPUnit.y = pi->Get(_T("NIBP_UNIT_Y"));
	m_stNIBPPumpPressure.x = pi->Get(_T("PUMP_PRESSURE_X"));
	m_stNIBPPumpPressure.y = pi->Get(_T("PUMP_PRESSURE_Y"));
	m_stNIBPPumpMode.x = pi->Get(_T("PUMP_MODE_X"));
	m_stNIBPPumpMode.y = pi->Get(_T("PUMP_MODE_Y"));

	m_stT1.type = pi->Get(_T("TEMP_NUM_FONT"));
	m_stT1.sub_type = pi->Get(_T("TEMP_NUM_FONT_SMALL"));
	m_stT1.x = pi->Get(_T("T1_NUM_X"));
	m_stT1.y = pi->Get(_T("T1_NUM_Y"));
	m_stT1LimitH.type = pi->Get(_T("T1_LIMIT_FONT"));
	m_stT1LimitH.x = pi->Get(_T("T1_LIMIT_H_X"));
	m_stT1LimitH.y = pi->Get(_T("T1_LIMIT_H_Y"));
	m_stT1LimitL.type = pi->Get(_T("T1_LIMIT_FONT"));
	m_stT1LimitL.x = pi->Get(_T("T1_LIMIT_L_X"));
	m_stT1LimitL.y = pi->Get(_T("T1_LIMIT_L_Y"));
	m_stT2.type = pi->Get(_T("TEMP_NUM_FONT"));
	m_stT2.sub_type = pi->Get(_T("TEMP_NUM_FONT_SMALL"));
	m_stT2.x = pi->Get(_T("T2_NUM_X"));
	m_stT2.y = pi->Get(_T("T2_NUM_Y"));
	m_stT2LimitH.type = pi->Get(_T("T2_LIMIT_FONT"));
	m_stT2LimitH.x = pi->Get(_T("T2_LIMIT_H_X"));
	m_stT2LimitH.y = pi->Get(_T("T2_LIMIT_H_Y"));
	m_stT2LimitL.type = pi->Get(_T("T2_LIMIT_FONT"));
	m_stT2LimitL.x = pi->Get(_T("T2_LIMIT_L_X"));
	m_stT2LimitL.y = pi->Get(_T("T2_LIMIT_L_Y"));
	m_stTEMPUnit.type = pi->Get(_T("TEMP_UNIT_FONT"));
	m_stTEMPUnit.x = pi->Get(_T("TEMP_UNIT_X"));
	m_stTEMPUnit.y = pi->Get(_T("TEMP_UNIT_Y"));

	m_stSPO2.type = pi->Get(_T("SPO2_NUM_FONT"));
	m_stSPO2.x = pi->Get(_T("SPO2_NUM_X"));
	m_stSPO2.y = pi->Get(_T("SPO2_NUM_Y"));
	m_stSPO2LimitH.type = pi->Get(_T("SPO2_LIMIT_FONT"));
	m_stSPO2LimitH.x = pi->Get(_T("SPO2_LIMIT_H_X"));
	m_stSPO2LimitH.y = pi->Get(_T("SPO2_LIMIT_H_Y"));
	m_stSPO2LimitL.type = pi->Get(_T("SPO2_LIMIT_FONT"));
	m_stSPO2LimitL.x = pi->Get(_T("SPO2_LIMIT_L_X"));
	m_stSPO2LimitL.y = pi->Get(_T("SPO2_LIMIT_L_Y"));

	m_stRR.type = pi->Get(_T("RR_NUM_FONT"));
	m_stRR.x = pi->Get(_T("RR_NUM_X"));
	m_stRR.y = pi->Get(_T("RR_NUM_Y"));
	m_stRRLimitH.type = pi->Get(_T("RR_LIMIT_FONT"));
	m_stRRLimitH.x = pi->Get(_T("RR_LIMIT_H_X"));
	m_stRRLimitH.y = pi->Get(_T("RR_LIMIT_H_Y"));
	m_stRRLimitL.type = pi->Get(_T("RR_LIMIT_FONT"));
	m_stRRLimitL.x = pi->Get(_T("RR_LIMIT_L_X"));
	m_stRRLimitL.y = pi->Get(_T("RR_LIMIT_L_Y"));

	// 各种区域的位置
	m_rcECG.top = pi->Get(_T("ECG_AREA_TOP"));
	m_rcECG.bottom = pi->Get(_T("ECG_AREA_BOT"));
	m_rcECG.left = pi->Get(_T("ECG_AREA_LFT"));
	m_rcECG.right = pi->Get(_T("ECG_AREA_RHT"));
	m_rcRESP.top = pi->Get(_T("RESP_AREA_TOP"));
	m_rcRESP.bottom = pi->Get(_T("RESP_AREA_BOT"));
	m_rcRESP.left = pi->Get(_T("RESP_AREA_LFT"));
	m_rcRESP.right = pi->Get(_T("RESP_AREA_RHT"));
	m_rcSPO2.top = pi->Get(_T("SPO2_AREA_TOP"));
	m_rcSPO2.bottom = pi->Get(_T("SPO2_AREA_BOT"));
	m_rcSPO2.left = pi->Get(_T("SPO2_AREA_LFT"));
	m_rcSPO2.right = pi->Get(_T("SPO2_AREA_RHT"));
	m_rcNIBP.top = pi->Get(_T("NIBP_AREA_TOP"));
	m_rcNIBP.bottom = pi->Get(_T("NIBP_AREA_BOT"));
	m_rcNIBP.left = pi->Get(_T("NIBP_AREA_LFT"));
	m_rcNIBP.right = pi->Get(_T("NIBP_AREA_RHT"));
	m_rcTEMP.top = pi->Get(_T("TEMP_AREA_TOP"));
	m_rcTEMP.bottom = pi->Get(_T("TEMP_AREA_BOT"));
	m_rcTEMP.left = pi->Get(_T("TEMP_AREA_LFT"));
	m_rcTEMP.right = pi->Get(_T("TEMP_AREA_RHT"));

	// 波形的绘图位置
	m_nWaveECG_X = pi->Get(_T("ECG_TABLE_X"));
	m_nWaveECG1_Y = pi->Get(_T("ECG1_TABLE_Y"));
	m_nWaveECG2_Y = pi->Get(_T("ECG2_TABLE_Y"));
	m_nWaveECG_W = pi->Get(_T("ECG_TABLE_W"));
	m_nWaveECG_H = pi->Get(_T("ECG_TABLE_H"));
	R = pi->Get(_T("ECG1_WAVE_COLOR_R"));
	G = pi->Get(_T("ECG1_WAVE_COLOR_G"));
	B = pi->Get(_T("ECG1_WAVE_COLOR_B"));
	m_crWaveECG1 = RGB(R, G, B);
	R = pi->Get(_T("ECG2_WAVE_COLOR_R"));
	G = pi->Get(_T("ECG2_WAVE_COLOR_G"));
	B = pi->Get(_T("ECG2_WAVE_COLOR_B"));
	m_crWaveECG2 = RGB(R, G, B);

	m_nWaveSPO2_X = pi->Get(_T("SPO2_TABLE_X"));
	m_nWaveSPO2_Y = pi->Get(_T("SPO2_TABLE_Y"));
	m_nWaveSPO2_W = pi->Get(_T("SPO2_TABLE_W"));
	m_nWaveSPO2_H = pi->Get(_T("SPO2_TABLE_H"));
	R = pi->Get(_T("SPO2_WAVE_COLOR_R"));
	G = pi->Get(_T("SPO2_WAVE_COLOR_G"));
	B = pi->Get(_T("SPO2_WAVE_COLOR_B"));
	m_crWaveSPO2 = RGB(R, G, B);

	m_nWaveRESP_X = pi->Get(_T("RESP_TABLE_X"));
	m_nWaveRESP_Y = pi->Get(_T("RESP_TABLE_Y"));
	m_nWaveRESP_W = pi->Get(_T("RESP_TABLE_W"));
	m_nWaveRESP_H = pi->Get(_T("RESP_TABLE_H"));
	R = pi->Get(_T("RESP_WAVE_COLOR_R"));
	G = pi->Get(_T("RESP_WAVE_COLOR_G"));
	B = pi->Get(_T("RESP_WAVE_COLOR_B"));
	m_crWaveRESP = RGB(R, G, B);

	m_nTechAlarmX = pi->Get(_T("TECH_ALARM_X"));
	m_nTechAlarmY = pi->Get(_T("TECH_ALARM_Y"));
	m_nTechAlarmW = pi->Get(_T("TECH_ALARM_W"));
	m_nTechAlarmH = pi->Get(_T("TECH_ALARM_H"));
	m_nBioAlarmX = pi->Get(_T("BIO_ALARM_X"));
	m_nBioAlarmY = pi->Get(_T("BIO_ALARM_Y"));
	m_nBioAlarmW = pi->Get(_T("BIO_ALARM_W"));
	m_nBioAlarmH = pi->Get(_T("BIO_ALARM_H"));
}

void CFMMonitorUnit::SetNumberResForFetus(int nIndex)
{
	int R, G, B;
	CIniInt* pi = m_pView->m_arBkIni[nIndex];

	// 数值的绘制位置
	m_stBedNum.type = pi->Get(_T("BED_NUM_FONT"));
	m_stBedNum.x = pi->Get(_T("BED_NUM_X"));
	m_stBedNum.y = pi->Get(_T("BED_NUM_Y"));
	m_nInfoBarX = pi->Get(_T("INFO_BAR_X"));
	m_nInfoBarY = pi->Get(_T("INFO_BAR_Y"));
	m_nInfoBarW = pi->Get(_T("INFO_BAR_W"));
	m_nInfoBarH = pi->Get(_T("INFO_BAR_H"));
	R = pi->Get(_T("INFO_TEXT_COLOR_R"));
	G = pi->Get(_T("INFO_TEXT_COLOR_G"));
	B = pi->Get(_T("INFO_TEXT_COLOR_B"));
	m_crInfoText = RGB(R, G, B);

	m_stFHR1.type = pi->Get(_T("FHR_NUM_FONT"));
	m_stFHR1.x = pi->Get(_T("FHR1_NUM_X"));
	m_stFHR1.y = pi->Get(_T("FHR1_NUM_Y"));
	m_stFHR2.type = pi->Get(_T("FHR_NUM_FONT"));
	m_stFHR2.x = pi->Get(_T("FHR2_NUM_X"));
	m_stFHR2.y = pi->Get(_T("FHR2_NUM_Y"));
	m_stFHRLimitH.type = pi->Get(_T("FHR_LIMIT_FONT"));
	m_stFHRLimitH.x = pi->Get(_T("FHR_LIMIT_H_X"));
	m_stFHRLimitH.y = pi->Get(_T("FHR_LIMIT_H_Y"));
	m_stFHRLimitL.type = pi->Get(_T("FHR_LIMIT_FONT"));
	m_stFHRLimitL.x = pi->Get(_T("FHR_LIMIT_L_X"));
	m_stFHRLimitL.y = pi->Get(_T("FHR_LIMIT_L_Y"));

	m_stTOCO.type = pi->Get(_T("TOCO_FONT"));
	m_stTOCO.x = pi->Get(_T("TOCO_NUM_X"));
	m_stTOCO.y = pi->Get(_T("TOCO_NUM_Y"));
	m_stTOCOLimitH.type = pi->Get(_T("TOCO_LIMIT_FONT"));
	m_stTOCOLimitH.x = pi->Get(_T("TOCO_LIMIT_H_X"));
	m_stTOCOLimitH.y = pi->Get(_T("TOCO_LIMIT_H_Y"));
	m_stTOCOLimitL.type = pi->Get(_T("TOCO_LIMIT_FONT"));
	m_stTOCOLimitL.x = pi->Get(_T("TOCO_LIMIT_L_X"));
	m_stTOCOLimitL.y = pi->Get(_T("TOCO_LIMIT_L_Y"));

	m_stFM.type = pi->Get(_T("FM_FONT"));
	m_stFM.x = pi->Get(_T("FM_NUM_X"));
	m_stFM.y = pi->Get(_T("FM_NUM_Y"));

	//区域的位置
	m_rcFHR.top = pi->Get(_T("FHR_AREA_TOP"));
	m_rcFHR.bottom = pi->Get(_T("FHR_AREA_BOT"));
	m_rcFHR.left = pi->Get(_T("FHR_AREA_LFT"));
	m_rcFHR.right = pi->Get(_T("FHR_AREA_RHT"));

	// 波形的绘制位置
	m_nWaveFHR_X = pi->Get(_T("FHR_TABLE_X"));
	m_nWaveFHR_Y = pi->Get(_T("FHR_TABLE_Y"));
	m_nWaveFHR_W = pi->Get(_T("FHR_TABLE_W"));
	m_nWaveFHR_H = pi->Get(_T("FHR_TABLE_H"));
	R = pi->Get(_T("FHR1_WAVE_COLOR_R"));
	G = pi->Get(_T("FHR1_WAVE_COLOR_G"));
	B = pi->Get(_T("FHR1_WAVE_COLOR_B"));
	m_crWaveFHR1 = RGB(R, G, B);
	R = pi->Get(_T("FHR2_WAVE_COLOR_R"));
	G = pi->Get(_T("FHR2_WAVE_COLOR_G"));
	B = pi->Get(_T("FHR2_WAVE_COLOR_B"));
	m_crWaveFHR2 = RGB(R, G, B);
	R = pi->Get(_T("FHR1_FLAG_COLOR_R"));
	G = pi->Get(_T("FHR1_FLAG_COLOR_G"));
	B = pi->Get(_T("FHR1_FLAG_COLOR_B"));
	m_crFlagFHR1 = RGB(R, G, B);
	R = pi->Get(_T("FHR2_FLAG_COLOR_R"));
	G = pi->Get(_T("FHR2_FLAG_COLOR_G"));
	B = pi->Get(_T("FHR2_FLAG_COLOR_B"));
	m_crFlagFHR2 = RGB(R, G, B);
	R = pi->Get(_T("TIME_MARK_COLOR_R"));
	G = pi->Get(_T("TIME_MARK_COLOR_G"));
	B = pi->Get(_T("TIME_MARK_COLOR_B"));
	m_crTimeMark = RGB(R, G, B);
	m_crTimeMarkDark = RGB(R*0.6, G*0.6, B);

	m_nWaveTOCO_Y = pi->Get(_T("TOCO_TABLE_Y"));
	m_nWaveTOCO_H = pi->Get(_T("TOCO_TABLE_H"));
	R = pi->Get(_T("TOCO_WAVE_COLOR_R"));
	G = pi->Get(_T("TOCO_WAVE_COLOR_G"));
	B = pi->Get(_T("TOCO_WAVE_COLOR_B"));
	m_crWaveTOCO = RGB(R, G, B);
	R = pi->Get(_T("FM_MARK_COLOR_R"));
	G = pi->Get(_T("FM_MARK_COLOR_G"));
	B = pi->Get(_T("FM_MARK_COLOR_B"));
	m_crWaveMARK = RGB(R, G, B);

	m_nTechAlarmX = pi->Get(_T("TECH_ALARM_X"));
	m_nTechAlarmY = pi->Get(_T("TECH_ALARM_Y"));
	m_nTechAlarmW = pi->Get(_T("TECH_ALARM_W"));
	m_nTechAlarmH = pi->Get(_T("TECH_ALARM_H"));
	m_nBioAlarmX = pi->Get(_T("BIO_ALARM_X"));
	m_nBioAlarmY = pi->Get(_T("BIO_ALARM_Y"));
	m_nBioAlarmW = pi->Get(_T("BIO_ALARM_W"));
	m_nBioAlarmH = pi->Get(_T("BIO_ALARM_H"));
}

void CFMMonitorUnit::OnGetFocus()
{
	RenewToolbar();
}

void CFMMonitorUnit::OnLoseFocus()
{
}

void CFMMonitorUnit::RenewData(CDC* pdc)
{
	if (! m_bVisible) {
		return;
	}

	if (! m_pUnit) {
		return;
	}

	//更改布局之后，第一次绘制背景；背景并非每次都重绘
	if (m_bRedrawBkgd) {
		DrawBkgd(pdc);
		m_bRedrawBkgd = FALSE;
		//填补FHR曲线
		FillUpFHRWave(pdc);

		if (theApp.DebugUI()) {
			DrawNumber(pdc);
		}
	}
	else {
		if (m_bRedrawNibpBkgd) {
			RedrawNibpBkgd(pdc);
			m_bRedrawNibpBkgd = FALSE;
		}
		if (m_bRedrawTempBkgd) {
			RedrawTempBkgd(pdc);
			m_bRedrawTempBkgd = FALSE;
		}
	}

	int nTimeSpan = m_pUnit->GetDisplayLenOfSecond();
	if (0 == nTimeSpan) {
		return;
	}

	if (nTimeSpan <= m_nLastGetDataTimeSpan) {
		return;
	}

	//胎儿监护模式，从第10分钟开始，每1分钟启动一次自动诊断，
	//并在下一次显示时强制刷新
	if (nTimeSpan >= 600 && nTimeSpan % 60 == 0) {
		m_pUnit->Autodiagnostics();
		if (MONITOR_TYPE_FETUS == GetType() ||
			MONITOR_TYPE_BOTH == GetType()) {
			m_bRedrawBkgd = TRUE;
		}
	}

	RenewNumb(nTimeSpan); //数值采用最新的数值
	DrawNumber(pdc);

#ifdef DECIMUS_VERSION
	//每1/10秒推进动画的绘制有些复杂，其逻辑是这样的：
	//1、没有新的数据，那么就不需要绘制了。
	//2、有新的数据，而且刚刚够绘制1次，那么按照1/10秒动画推进；
	//3、新的数据超过1次绘制所能完成的数量，那么只有最后一次按照1/10秒推进，
	//   前面调用DrawWave直接画完。
	if (m_nLastGetDataTimeSpan < nTimeSpan) {
		
		while (m_nLastGetDataTimeSpan < (nTimeSpan - 1)) {
			RenewWave(m_nLastGetDataTimeSpan);
			DrawWave(pdc);
			RenewFHRWave(m_nLastGetDataTimeSpan);
			DrawFHRWave(pdc);
			m_nLastGetDataTimeSpan ++;
		}

		//按照正常的1/10秒动画进行绘制
		RenewWave(m_nLastGetDataTimeSpan);
		m_bDrawWaveDecimus = TRUE;
		m_nWaveECG_DecimusDataPos = 0;
		m_nWaveSPO2_DecimusDataPos = 0;
		m_nWaveRESP_DecimusDataPos = 0;

		RenewFHRWave(m_nLastGetDataTimeSpan);
		DrawFHRWave(pdc);
		m_nLastGetDataTimeSpan ++;
	}
	else {
		//这种情况下不需要再绘制什么了
		m_bDrawWaveDecimus = FALSE;
	}

#else
	while (m_nLastGetDataTimeSpan < nTimeSpan) {
		RenewWave(m_nLastGetDataTimeSpan);
		DrawWave(pdc);
		RenewFHRWave(m_nLastGetDataTimeSpan);
		DrawFHRWave(pdc);
		m_nLastGetDataTimeSpan ++;
	}
#endif

	//更新FHR标尺显示
	DrawFHRTimeRuler(pdc);

	//绘制信息，更新监护时间
	DrawInfo(pdc);

	//绘制报警信息
	m_pView->SelectInfoFont();
	DrawTechAlarm(pdc);
	DrawBioAlarm(pdc);
	m_pView->ResetFont();
}

void CFMMonitorUnit::RenewNumb(int nTimeSpan)
{
	DISPLAY_DATA* pdd = m_pUnit->GetNumbData(nTimeSpan);
	if (! pdd) {
		pdd = m_pUnit->GetLatestNumbData();
	}

	if (pdd) {
		m_nHRFlag         = (0 == pdd->hr_src) ? 0 : 1;
		m_nHR             = pdd->hr;
		m_nHRLimitH       = pdd->hr_h;
		m_nHRLimitL       = pdd->hr_l;
		m_nST1            = pdd->st1;
		m_nST1LimitH      = pdd->st1_h;
		m_nST1LimitL      = pdd->st1_l;
		m_nST2            = pdd->st2;
		m_nST2LimitH      = pdd->st2_h;
		m_nST2LimitL      = pdd->st2_l;
		m_nPVC            = pdd->pvc;
		m_nPVCLimitH      = pdd->pvc_h;
		m_nECG1LeadOpt    = pdd->ecg1_lead;
		m_nECG2LeadOpt    = pdd->ecg2_lead;
		m_nECGGainOpt     = pdd->ecg_gain;
		m_nECGFilter      = pdd->ecg_filter;
		m_nNIBP_S         = pdd->sys;
		m_nNIBP_M         = pdd->mea;
		m_nNIBP_D         = pdd->dia;
		m_nNIBPLimitH     = pdd->sys_h;
		m_nNIBPLimitL     = pdd->sys_l;
		m_nNIBP_M_LimitH  = pdd->mea_h;
		m_nNIBP_M_LimitL  = pdd->mea_l;
		m_nNIBP_D_LimitH  = pdd->dia_h;
		m_nNIBP_D_LimitL  = pdd->dia_l;
		m_nNIBPPumpPressure = pdd->nibp_pump;
		m_nNIBPPumpMode   = pdd->nibp_mode;
		m_nT1             = pdd->t1;
		m_nT1LimitH       = pdd->t1_h;
		m_nT1LimitL       = pdd->t1_l;
		m_nT2             = pdd->t2;
		m_nT2LimitH       = pdd->t2_h;
		m_nT2LimitL       = pdd->t2_l;
		m_nSPO2           = pdd->spo2;
		m_nSPO2LimitH     = pdd->spo2_h;
		m_nSPO2LimitL     = pdd->spo2_l;
		m_nRR             = pdd->rr;
		m_nRRLimitH       = pdd->rr_h;
		m_nRRLimitL       = pdd->rr_l;
		m_nPR             = pdd->pr;
		m_nPRLimitH       = pdd->pr_h;
		m_nPRLimitL       = pdd->pr_l;
		m_nFHR1           = pdd->fhr1;
		m_nFHR2           = pdd->fhr2;
		m_nFHRLimitH      = pdd->fhr_h;
		m_nFHRLimitL      = pdd->fhr_l;
		m_nTOCO           = pdd->toco;
		m_nFM             = pdd->fm;
		m_nECGError       = pdd->ecg_error;
		m_nRESPError      = pdd->resp_error;
		m_nSPO2Error      = pdd->spo2_error;
		m_nNIBPError      = pdd->nibp_error;
		//m_nTEMPError      = pdd->temp_error;
		m_nTEMPError      = 0; //体温探头一般不接，所以体温的技术报警不报。（20141004修改）
		m_nFHRError       = pdd->fhr_error;
		m_nBioAlarmType   = pdd->bio_alarm_type;
		m_nBioAlarmLevel  = pdd->bio_alarm_level;
	}
	else {
		//显示成未赋值状态
		ResetNumber();
	}
}

void CFMMonitorUnit::RenewWave(int nTimeSpan)
{
	int nRet = m_pUnit->GetWaveData(nTimeSpan,
		&m_pWaveECG1,
		&m_pWaveECG2,
		&m_pWaveECGv,
		&m_pWaveSPO2,
		&m_pWaveRESP);
}

void CFMMonitorUnit::RenewFHRWave(int nTimeSpan)
{
	int nRet = m_pUnit->GetFHRWaveData(nTimeSpan,
		&m_pWaveFHR1,
		&m_pWaveFHR2,
		&m_pWaveTOCO,
		&m_pWaveFMMARK,
		&m_pWaveTOCOReset,
		&m_pDiagno1,
		&m_pDiagno2);
}

void CFMMonitorUnit::OnSwitchType()
{
	if (MONITOR_AREA_1 == m_nAreaType) {
		switch (GetType()) {
			case MONITOR_TYPE_ADULT: SetType(MONITOR_TYPE_FETUS); break;
			case MONITOR_TYPE_FETUS: SetType(MONITOR_TYPE_BOTH);  break;
			case MONITOR_TYPE_BOTH:  SetType(MONITOR_TYPE_ADULT); break;
			default: SetType(MONITOR_TYPE_FETUS); break;
		}
	}
	else {
		switch (GetType()) {
			case MONITOR_TYPE_BOTH:  SetType(MONITOR_TYPE_ADULT); break;
			case MONITOR_TYPE_ADULT: SetType(MONITOR_TYPE_FETUS); break;
			case MONITOR_TYPE_FETUS: SetType(MONITOR_TYPE_ADULT); break;
			default: SetType(MONITOR_TYPE_FETUS); break;
		}
	}
	RenewToolbar();
	//在画面切换过来之前就不再画线了
	m_bDrawWaveDecimus = FALSE;
}

void CFMMonitorUnit::OnMedicalRecord()
{
	CMedicalRecordListDlg dlg(m_pView, TYPE_OP_SELECT);
	if (IDOK == dlg.DoModal()) {
		CString sMedNum = dlg.GetSelectedMedicalNum();
		m_pUnit->SetMedicalNum(m_pView, sMedNum);
	}
}

void CFMMonitorUnit::SetRecordUnit(int nUnitIndex)
{
	if (-1 == nUnitIndex) {
		m_pUnit = NULL;
		return;
	}
	m_pUnit = theApp.GetRecordUnit(nUnitIndex);
	if (m_pUnit->IsFirstUse()) {
		ResetWavePos();
	}
}

CString CFMMonitorUnit::StringBedNum() {
	CString s;
	if (m_pUnit) {
		s.Format(_T("%02d"), m_pUnit->GetBedNum());
	}
	else {
		s = _T("--"); 
	}
	return s;
};

void CFMMonitorUnit::RenewToolbar()
{
	CString sBtnText = _T("");
	if (MONITOR_AREA_1 == m_nAreaType) {
		switch (GetType()) {
			case MONITOR_TYPE_ADULT: sBtnText = TOOLBAR_TEXT_FETUS; break;
			case MONITOR_TYPE_FETUS: sBtnText = TOOLBAR_TEXT_BOTH;  break;
			case MONITOR_TYPE_BOTH:  sBtnText = TOOLBAR_TEXT_ADULT; break;
			default: sBtnText = TOOLBAR_TEXT_FETUS; break;
		}
	}
	else {
		switch (GetType()) {
			case MONITOR_TYPE_ADULT: sBtnText = TOOLBAR_TEXT_FETUS; break;
			case MONITOR_TYPE_FETUS: sBtnText = TOOLBAR_TEXT_ADULT; break;
			default: sBtnText = TOOLBAR_TEXT_FETUS; break;
		}
	}
	m_pView->SetToolbarStatus(m_x+35, m_y+2, sBtnText);
}

BOOL CFMMonitorUnit::OnDbClick(CPoint& pt)
{
	if (! m_pUnit) {
		return FALSE;
	}
	if (! m_pUnit->IsAlive()) {
		return FALSE;
	}
	
	CRect rc;
	if (MONITOR_TYPE_ADULT == GetType() ||
		MONITOR_TYPE_BOTH == GetType()) {
		rc = m_rcECG; rc.OffsetRect(m_x, m_y);
		if (rc.PtInRect(pt)) {
			CSetupECGDlg dlg(m_pView, m_pUnit);
			dlg.DoModal();
			return TRUE;
		}
		rc = m_rcRESP; rc.OffsetRect(m_x, m_y);
		if (rc.PtInRect(pt)) {
			CSetupRESPDlg dlg(m_pView, m_pUnit);
			dlg.DoModal();
			return TRUE;
		}
		rc = m_rcSPO2; rc.OffsetRect(m_x, m_y);
		if (rc.PtInRect(pt)) {
			CSetupSPO2Dlg dlg(m_pView, m_pUnit);
			dlg.DoModal();
			return TRUE;
		}
		rc = m_rcNIBP; rc.OffsetRect(m_x, m_y);
		if (rc.PtInRect(pt)) {
			CSetupNIBPDlg dlg(m_pView, m_pUnit, m_nNIBPUnit);
			if (IDOK == dlg.DoModal()) {
				m_nNIBPUnit = dlg.GetNIBPUnitOpt();
				m_bRedrawNibpBkgd = TRUE;
			}
			return TRUE;
		}
		rc = m_rcTEMP; rc.OffsetRect(m_x, m_y);
		if (rc.PtInRect(pt)) {
			CSetupTEMPDlg dlg(m_pView, m_pUnit, m_nTEMPUnit);
			if (IDOK == dlg.DoModal()) {
				m_nTEMPUnit = dlg.GetTEMPUnitOpt();
				m_bRedrawTempBkgd = TRUE;
			}
			return TRUE;
		}
	}
	if (MONITOR_TYPE_FETUS == GetType() ||
		MONITOR_TYPE_BOTH == GetType()) {
		rc = m_rcFHR; rc.OffsetRect(m_x, m_y);
		if (rc.PtInRect(pt)) {
			CSetupFHRDlg dlg(m_pView, m_pUnit, m_nFHRPaperSpeedOpt);
			if (IDOK == dlg.DoModal()) {
				m_nFHRPaperSpeedOpt = dlg.GetFHRPaperSpeedOpt();
				// 刷新FHR的波形显示
				m_bRedrawBkgd = TRUE;
			}
			return TRUE;
		}
	}

	return FALSE;
}

#ifdef DECIMUS_VERSION
void CFMMonitorUnit::DrawECGWaveDecimus(CDC* pdc, int nDecimusCounter)
{
	if (! m_pWaveECG1 ||
		! m_pWaveECG2) {
		return;
	}

	if (0 == m_nWaveECG1_Y &&
		0 == m_nWaveECG2_Y) {
		return;
	}

	int nDrawLen = min(DISPLAY_ECG_UNIT_SIZE, m_nWaveECG_W);
	int nDecimusLen = (nDrawLen-1) * (nDecimusCounter + 1) / 10
					- (nDrawLen-1) * nDecimusCounter / 10;

	//CString s;
	//s.Format(_T("nDecimusCounter = %d\n"), nDecimusCounter);
	//theApp.WriteLog(s);
	//s.Format(_T("nDecimusLen = %d\n"), nDecimusLen);
	//theApp.WriteLog(s);

	if (0 != m_nWaveECG1_Y) {
		if (*m_ppBkDC) {
			int x = m_nWaveECG_X + m_nWaveECG_StartPos + 1;
			int y = m_nWaveECG1_Y;
			int w1 = min(nDecimusLen+1, m_nWaveECG_W - m_nWaveECG_StartPos - 1);
			int w2 = nDecimusLen+1 - w1;
			int h = m_nWaveECG_H;
			pdc->BitBlt(m_x+x, m_y+y, w1, h, *m_ppBkDC, x, y, SRCCOPY);
			if (w2 > 0) {
				pdc->BitBlt(m_x+m_nWaveECG_X, m_y+y, w2, h, *m_ppBkDC, m_nWaveECG_X, y, SRCCOPY);
			}
		}
	}
	if (0 != m_nWaveECG2_Y) {
		if (*m_ppBkDC) {
			int x = m_nWaveECG_X + m_nWaveECG_StartPos + 1;
			int y = m_nWaveECG2_Y;
			int w1 = min(nDecimusLen+1, m_nWaveECG_W - m_nWaveECG_StartPos - 1);
			int w2 = nDecimusLen+1 - w1;
			int h = m_nWaveECG_H;
			pdc->BitBlt(m_x+x, m_y+y, w1, h, *m_ppBkDC, x, y, SRCCOPY);
			if (w2 > 0) {
				pdc->BitBlt(m_x+m_nWaveECG_X, m_y+y, w2, h, *m_ppBkDC, m_nWaveECG_X, y, SRCCOPY);
			}
		}
	}

	int nHalfH = m_nWaveECG_H / 2;
	int i;
	int x = m_nWaveECG_X + m_nWaveECG_StartPos;
	for (i = 0; i < nDecimusLen; i ++) {
		//绘制第一通道
		int mid_y1 = m_nWaveECG1_Y + nHalfH;
		int mid_y2 = m_nWaveECG1_Y + nHalfH;
		int v1 = min(ECG_MID_VALUE + nHalfH - 1,
					max(ECG_MID_VALUE - nHalfH + 1, m_pWaveECG1[m_nWaveECG_DecimusDataPos + i]));
		int v2 = min(ECG_MID_VALUE + nHalfH - 1,
					max(ECG_MID_VALUE - nHalfH + 1, m_pWaveECG1[m_nWaveECG_DecimusDataPos + i+1]));
		if (0 != m_nWaveECG1_Y) {
			DrawLineAA(pdc, m_crWaveECG1,
				m_x + x,
				m_y + mid_y1 + ECG_MID_VALUE - v1,
				m_y + mid_y1 + ECG_MID_VALUE - v2);
		}
		//绘制第二通道
		mid_y1 = m_nWaveECG2_Y + nHalfH;
		mid_y2 = m_nWaveECG2_Y + nHalfH;
		v1 = min(ECG_MID_VALUE + nHalfH - 1,
				max(ECG_MID_VALUE - nHalfH + 1, m_pWaveECG2[m_nWaveECG_DecimusDataPos + i]));
		v2 = min(ECG_MID_VALUE + nHalfH - 1,
				max(ECG_MID_VALUE - nHalfH + 1, m_pWaveECG2[m_nWaveECG_DecimusDataPos + i+1]));
		if (0 != m_nWaveECG2_Y) {
			DrawLineAA(pdc, m_crWaveECG2,
				m_x + x,
				m_y + mid_y2 + ECG_MID_VALUE - v1,
				m_y + mid_y2 + ECG_MID_VALUE - v2);
		}
		x ++;
		m_nWaveECG_StartPos ++;
		if (m_nWaveECG_StartPos >= m_nWaveECG_W - 1) {
			//绘制到达画布最右端时，跳转到画布的最左端
			m_nWaveECG_StartPos = 0;
			x = m_nWaveECG_X;
		}
	}
	m_nWaveECG_DecimusDataPos += i;

	//s.Format(_T("m_nWaveECG_StartPos = %d\n"), m_nWaveECG_StartPos);
	//theApp.WriteLog(s);
	//s.Format(_T("m_nWaveECG_DecimusDataPos = %d\n"), m_nWaveECG_DecimusDataPos);
	//theApp.WriteLog(s);

	//绘制终结线
	if (0 != m_nWaveECG1_Y) {
		CBrush br(m_crWaveECG1);
		CRect rc2(x+1, m_nWaveECG1_Y,
			min(x+2, m_nWaveECG_X + m_nWaveECG_W), m_nWaveECG1_Y + m_nWaveECG_H);
		CRect rc1(x+2, m_nWaveECG1_Y,
			min(x+15, m_nWaveECG_X + m_nWaveECG_W), m_nWaveECG1_Y + m_nWaveECG_H);
		if (*m_ppBkDC) {
			pdc->BitBlt(m_x + rc1.left, m_y + rc1.top, rc1.Width(), rc1.Height(), *m_ppBkDC, rc1.left, rc1.top, SRCCOPY);
		}
		rc2.OffsetRect(m_x, m_y);
		pdc->FillRect(&rc2, &br);
	}
	if (0 != m_nWaveECG2_Y) {
		CBrush br(m_crWaveECG2);
		CRect rc2(x+1, m_nWaveECG2_Y, 
			min(x+2, m_nWaveECG_X + m_nWaveECG_W), m_nWaveECG2_Y + m_nWaveECG_H);
		CRect rc1(x+2, m_nWaveECG2_Y, 
			min(x+15, m_nWaveECG_X + m_nWaveECG_W), m_nWaveECG2_Y + m_nWaveECG_H);
		if (*m_ppBkDC) {
			pdc->BitBlt(m_x + rc1.left, m_y + rc1.top, rc1.Width(), rc1.Height(), *m_ppBkDC, rc1.left, rc1.top, SRCCOPY);
		}
		rc2.OffsetRect(m_x, m_y);
		pdc->FillRect(&rc2, &br);
	}
}
#endif // DECIMUS_VERSION

#ifdef DECIMUS_VERSION
void CFMMonitorUnit::DrawSPO2WaveDecimus(CDC* pdc, int nDecimusCounter)
{
	if (! m_pWaveSPO2) {
		return;
	}

	if (0 == m_nWaveSPO2_X) {
		return;
	}

	//压缩数据
	float scaleY = float(m_nWaveSPO2_H) / 256;
	BYTE arDispBuf[2000];
	int dispCount = min(2000, (int)(DISPLAY_SPO2_UNIT_SIZE * scaleY));
	int srcCount  = DISPLAY_SPO2_UNIT_SIZE;
	CompressData(arDispBuf, m_pWaveSPO2, dispCount, srcCount, scaleY);

	int nDrawLen = min(dispCount, m_nWaveSPO2_W);
	int nDecimusLen = (nDrawLen-1) * (nDecimusCounter + 1) / 10
					- (nDrawLen-1) * nDecimusCounter / 10;

	//CString s;
	//s.Format(_T("1>>>> dispCount = %d\n"), dispCount);
	//theApp.WriteLog(s);

	if (*m_ppBkDC) {
		int x = m_nWaveSPO2_X + m_nWaveSPO2_StartPos + 1;
		int y = m_nWaveSPO2_Y;
		int w1 = min(nDecimusLen+1, m_nWaveSPO2_W - m_nWaveSPO2_StartPos - 1);
		int w2 = nDecimusLen+1 - w1;
		int h = m_nWaveSPO2_H;
		pdc->BitBlt(m_x+x, m_y+y, w1, h, *m_ppBkDC, x, y, SRCCOPY);
		if (w2 > 0) {
			pdc->BitBlt(m_x+m_nWaveSPO2_X, m_y+y, w2, h, *m_ppBkDC, m_nWaveSPO2_X, y, SRCCOPY);
		}
	}
	int i;
	int x = m_nWaveSPO2_X + m_nWaveSPO2_StartPos;
	for (i = 0; i < nDecimusLen; i ++) {
		int v1 = min(m_nWaveSPO2_H - 1, max(1, arDispBuf[m_nWaveSPO2_DecimusDataPos + i]));
		int v2 = min(m_nWaveSPO2_H - 1, max(1, arDispBuf[m_nWaveSPO2_DecimusDataPos + i+1]));
		DrawLineAA(pdc, m_crWaveSPO2,
			m_x + x,
			m_y + m_nWaveSPO2_Y + m_nWaveSPO2_H - v1,
			m_y + m_nWaveSPO2_Y + m_nWaveSPO2_H - v2);
		x ++;
		m_nWaveSPO2_StartPos ++;
		if (m_nWaveSPO2_StartPos >= m_nWaveSPO2_W - 1) {
			//绘制到达画布最右端时，跳转到画布的最左端
			m_nWaveSPO2_StartPos = 0;
			x = m_nWaveSPO2_X;
		}
	}
	m_nWaveSPO2_DecimusDataPos += i;

	//绘制终结线
	CBrush br(m_crWaveSPO2);
	CRect rc2(x+1, m_nWaveSPO2_Y,
		min(x+2, m_nWaveSPO2_X + m_nWaveSPO2_W), m_nWaveSPO2_Y + m_nWaveSPO2_H);
	CRect rc1(x+2, m_nWaveSPO2_Y, 
		min(x+15, m_nWaveSPO2_X + m_nWaveSPO2_W), m_nWaveSPO2_Y + m_nWaveSPO2_H);
	if (*m_ppBkDC) {
		pdc->BitBlt(m_x + rc1.left, m_y + rc1.top, rc1.Width(), rc1.Height(), *m_ppBkDC, rc1.left, rc1.top, SRCCOPY);
	}
	rc2.OffsetRect(m_x, m_y);
	pdc->FillRect(&rc2, &br);
}
#endif // DECIMUS_VERSION

#ifdef DECIMUS_VERSION
void CFMMonitorUnit::DrawRESPWaveDecimus(CDC* pdc, int nDecimusCounter)
{
	if (! m_pWaveRESP) {
		return;
	}

	if (0 == m_nWaveRESP_X) {
		return;
	}
	
	//压缩数据
	float scaleY = float(m_nWaveRESP_H) / 256;
	BYTE arDispBuf[2000];
	int dispCount = min(2000, (int)(DISPLAY_RESP_UNIT_SIZE * scaleY));
	int srcCount  = DISPLAY_RESP_UNIT_SIZE;
	CompressData(arDispBuf, m_pWaveRESP, dispCount, srcCount, scaleY);

	int nDrawLen = min (dispCount, m_nWaveRESP_W);
	int nDecimusLen = (nDrawLen-1) * (nDecimusCounter + 1) / 10
					- (nDrawLen-1) * nDecimusCounter / 10;

	//CString s;
	//s.Format(_T("2>>>> dispCount = %d\n"), dispCount);
	//theApp.WriteLog(s);

	if (*m_ppBkDC) {
		int x = m_nWaveRESP_X + m_nWaveRESP_StartPos + 1;
		int y = m_nWaveRESP_Y;
		int w1 = min(nDecimusLen+1, m_nWaveRESP_W - m_nWaveRESP_StartPos - 1);
		int w2 = nDecimusLen+1 - w1;
		int h = m_nWaveRESP_H;
		pdc->BitBlt(m_x+x, m_y+y, w1, h, *m_ppBkDC, x, y, SRCCOPY);
		if (w2 > 0) {
			pdc->BitBlt(m_x+m_nWaveRESP_X, m_y+y, w2, h, *m_ppBkDC, m_nWaveRESP_X, y, SRCCOPY);
		}
	}
	int i;
	int x = m_nWaveRESP_X + m_nWaveRESP_StartPos;
	for (i = 0; i < nDecimusLen; i ++) {
		int v1 = min(m_nWaveRESP_H - 1, max(1, arDispBuf[m_nWaveRESP_DecimusDataPos + i]));
		int v2 = min(m_nWaveRESP_H - 1, max(1, arDispBuf[m_nWaveRESP_DecimusDataPos + i+1]));
		DrawLineAA(pdc, m_crWaveRESP,
			m_x + x,
			m_y + m_nWaveRESP_Y + m_nWaveRESP_H - v1,
			m_y + m_nWaveRESP_Y + m_nWaveRESP_H - v2);
		x ++;
		m_nWaveRESP_StartPos ++;
		if (m_nWaveRESP_StartPos >= m_nWaveRESP_W - 1) {
			//绘制到达画布最右端时，跳转到画布的最左端
			m_nWaveRESP_StartPos = 0;
			x = m_nWaveRESP_X;
		}
	}
	m_nWaveRESP_DecimusDataPos += i;

	//绘制终结线
	CBrush br(m_crWaveRESP);
	CRect rc2(x+1, m_nWaveRESP_Y, 
		min(x+2, m_nWaveRESP_X + m_nWaveRESP_W), m_nWaveRESP_Y + m_nWaveRESP_H);
	CRect rc1(x+2, m_nWaveRESP_Y, 
		min(x+15, m_nWaveRESP_X + m_nWaveRESP_W), m_nWaveRESP_Y + m_nWaveRESP_H);
	if (*m_ppBkDC) {
		pdc->BitBlt(m_x + rc1.left, m_y + rc1.top, rc1.Width(), rc1.Height(), *m_ppBkDC, rc1.left, rc1.top, SRCCOPY);
	}
	rc2.OffsetRect(m_x, m_y);
	pdc->FillRect(&rc2, &br);
}
#endif // DECIMUS_VERSION
