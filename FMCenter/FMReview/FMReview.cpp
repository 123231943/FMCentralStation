
// FMReview.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "FMReview.h"
#include "FMReviewDlg.h"
#include "FHRAutodiagnostics.h"
#include "..\\FMCenter\\LogFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFMReviewApp

BEGIN_MESSAGE_MAP(CFMReviewApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CFMReviewApp 构造

CFMReviewApp::CFMReviewApp()
{
	m_pLogFile = NULL;
}


// 唯一的一个 CFMReviewApp 对象

CFMReviewApp theApp;


// CFMReviewApp 初始化

BOOL CFMReviewApp::InitInstance()
{
	CWinApp::InitInstance();


	// 创建 shell 管理器，以防对话框包含
	// 任何 shell 树视图控件或 shell 列表视图控件。
	CShellManager *pShellManager = new CShellManager;

	// 激活“Windows Native”视觉管理器，以便在 MFC 控件中启用主题
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));

	//创建日志文件
	CString strFullPath = theApp.m_pszHelpFilePath;
	CString sLocalPath = GetFilePath(strFullPath);
	CString sLogFile;
	sLogFile.Format(_T("%s%s"), sLocalPath, _T("\\log.txt"));
	//AfxMessageBox(sLogFile);
	m_pLogFile = new CLogFile(sLogFile);

	//预留自动诊断模块接口
	CString sDllPath;
	//sDllPath.Format(_T("%s%s"), theApp.GetDataRoot(), _T("动态库路径"));
	//if (! CFHRAutodiagnostics::InitModule(sDllPath)) {
	//	MessageBox(NULL, _T("自动诊断模块 加载失败！\n\n程序将关闭自动诊断功能！"), _T("错误信息"), MB_OK);
		m_bAutoDiagno = FALSE;
	//} else {
	//	m_bAutoDiagno = TRUE;
	//}

	CFMReviewDlg dlg;
	m_pMainWnd = &dlg;
	m_pMainDlg = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 在此放置处理何时用
		//  “取消”来关闭对话框的代码
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "警告: 对话框创建失败，应用程序将意外终止。\n");
		TRACE(traceAppMsg, 0, "警告: 如果您在对话框上使用 MFC 控件，则无法 #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS。\n");
	}

	// 删除上面创建的 shell 管理器。
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

int CFMReviewApp::ExitInstance()
{
	if (m_pLogFile) {
		delete m_pLogFile;
		m_pLogFile = NULL;
	}
	return CWinApp::ExitInstance();
}

void CFMReviewApp::WriteLog(CString s)
{
	CSmartLock lock(m_csLogLock);
	if (m_pLogFile) {
		m_pLogFile->Write(s);
	}
}

int CFMReviewApp::GetRecordFileList(CStringArray& sa, CUIntArray* paOffset, CUIntArray* paSize)
{
	CStringArray* psa = m_pMainDlg->GetPathGroup();
	if (! psa) {
		return FALSE;
	}

	int i;
	int nOffset = 0;
	int count = psa->GetCount();
	for (i=0; i<count; i++) {
		CString sFullName = psa->GetAt(i);
		sa.Add(sFullName);

		CFileStatus fs;
		CFile::GetStatus(sFullName, fs);
		int nFileSize = (int)(fs.m_size / sizeof(CLIENT_DATA));

		if (paOffset) {
			paOffset->Add((UINT)nOffset);
		}

		if (paSize) {
			paSize->Add((UINT)nFileSize);
		}

		nOffset += nFileSize;
	}

	return sa.GetCount();
}