#include "stdafx.h"
#include "FMReview.h"
#include "ReviewDlg.h"
#include "PrintTendWave.h"

static const int lft_margin = 200;
static const int top_margin = 150;

static const int table_top = 40;
static const int table_gap = 80;
static const int table_left = 200;
static const int grid_w = 40;    //1格10秒钟
static const int grid_cols = 60; //10分钟
static const int table_w = grid_w * grid_cols;
static const float time_scale = 4.0f;

static const int hr_grid_line = 35;
static const int hr_grid_h = 12;
static const int hr_table_h = hr_grid_h * hr_grid_line;
static const int hr_max = 350;
static const float hr_scale = 1.2f;

static const int bp_grid_line = 35;
static const int bp_grid_h = 12;
static const int bp_table_h = bp_grid_h * bp_grid_line;
static const int bp_max = 350;
static const float bp_scale = 1.2f;

static const int rr_grid_line = 24;
static const int rr_grid_h = 12;
static const int rr_table_h = rr_grid_h * rr_grid_line;
static const int rr_max = 120;
static const float rr_scale = 2.4f;

static const int tt_grid_line = 20;
static const int tt_grid_h = 15;
static const int tt_table_h = tt_grid_h * tt_grid_line;
static const int tt_max = 50;
static const float tt_scale = 6.0f;

static const int scale_w = 60;
static const int text_scale_h = 40;
static const int half_scale_h = (text_scale_h / 2);

static const COLORREF cr_bkgd = RGB(0,0,0);
static const COLORREF cr_normal = RGB(225,225,255);
static const COLORREF cr_grid_light = RGB(192,192,255);
static const COLORREF cr_grid_dark = RGB(128,128,255);
static const COLORREF cr_minite = RGB(220,92,128);
static const COLORREF cr_scale = RGB(0,0,128);
static const COLORREF cr_info = RGB(0,0,0);
static const COLORREF cr_wave = RGB(0,0,0);
static const COLORREF cr_wave2 = RGB(92,92,128);


CPrintTendWave::CPrintTendWave(CReviewDlg* pdlg)
	: CPrintTool(pdlg)
{
	m_penStyle1.CreatePen(PS_SOLID, 3, cr_wave);

	LOGBRUSH logBrush;
	logBrush.lbStyle = PS_SOLID;
	logBrush.lbColor = cr_wave;
	DWORD dwF[2] = {1, 3};
	m_penStyle2.CreatePen(PS_USERSTYLE|PS_GEOMETRIC|PS_ENDCAP_ROUND, 3, &logBrush, 2, (LPDWORD)dwF);

	m_penStyle3.CreatePen(PS_SOLID, 3, cr_wave2);

	m_fontInfo.CreateFont(36, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_pOldFont = NULL;
}

CPrintTendWave::~CPrintTendWave(void)
{
	m_pOldFont = NULL;
}

CPen* CPrintTendWave::GetPen()
{
	CPen* p = NULL;
	switch (m_nCurPen) {
	case 0: p = &m_penStyle1; break;
	case 1: p = &m_penStyle2; break;
	case 2: p = &m_penStyle3; break;
	}
	if (m_nCurPen < 2) {
		m_nCurPen ++;
	}
	return p;
}

void CPrintTendWave::InitSetup(int type)
{
}

void CPrintTendWave::Print(CDC* pdc,  CPrintInfo* pInfo)
{
	CRect rc(lft_margin, - top_margin, lft_margin + table_left + table_w,
		- top_margin - hr_table_h - table_gap - bp_table_h - table_gap - rr_table_h - table_gap - tt_table_h);

	m_pOldFont = pdc->SelectObject(&m_fontInfo);
	pdc->SetBkMode(TRANSPARENT);
	DrawPatientInfo(pdc, rc);
	DrawGrid(pdc, rc);
	DrawTime(pdc, rc);
	DrawTableInfo(pdc, rc);
	DrawData(pdc, rc);
	pdc->SelectObject(m_pOldFont);
}

void CPrintTendWave::DrawPatientInfo(CDC* pdc, CRect& rc)
{
	CString sPatientInfo;
	CString sWaveformInfo;

	CString sBedNum;
	CString sMedNum;
	CString sPatName;
	CString sPatGender;
	CString sPatType;
	CString sPatAge;

	sBedNum.Format(_T("床位：%02s"), m_pReviewDlg->m_sBedNum.IsEmpty() ? _T("----") : m_pReviewDlg->m_sBedNum);
	sMedNum.Format(_T("病历号：%s"), m_pReviewDlg->m_sMedicalNum.IsEmpty() ? _T("----") : m_pReviewDlg->m_sMedicalNum);
	sPatName.Format(_T("姓名：%s"), m_pReviewDlg->m_sPatientName.IsEmpty() ? _T("----") : m_pReviewDlg->m_sPatientName);
	sPatGender.Format(_T("性别：%s"), m_pReviewDlg->m_sPatientGender.IsEmpty() ? _T("----") : m_pReviewDlg->m_sPatientGender);
	sPatType.Format(_T("类型：%s"), m_pReviewDlg->m_sPatientType.IsEmpty() ? _T("----") : m_pReviewDlg->m_sPatientType);
	sPatAge.Format(_T("年龄：%s"), m_pReviewDlg->m_sPatientType.IsEmpty() ? _T("--") : m_pReviewDlg->m_sPatientAge);

	sPatientInfo.Format(_T("%5s%20s%30s%20s%20s%20s"), sBedNum, sMedNum, sPatName, sPatGender, sPatType, sPatAge);
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(cr_info);
	pdc->TextOut(rc.left, rc.top + 36, sPatientInfo);

	CString sStartTime;
	CString sWaveformLen;
	CString sPrintTime;
	int nValidLen;
	CTime tmTableStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	sStartTime = tmTableStart.Format(_T("开始时间：%Y-%m-%d %H:%M:%S"));

	int nWaveformLen = min(nValidLen, abs(table_w / grid_w) * 10); //1个格子是10秒
	sWaveformLen.Format(_T("数据长度：%d秒"), nWaveformLen);
	CTime tmNow = CTime::GetCurrentTime();
	sPrintTime = tmNow.Format(_T("打印时间：%Y-%m-%d %H:%M:%S"));

	sWaveformInfo.Format(_T("%20s%30s%68s"), sStartTime, sWaveformLen, sPrintTime);
	pdc->TextOut(rc.left, rc.bottom - 60, sWaveformInfo);
	
	if (! theApp.m_sHospitalName.IsEmpty()) {
		pdc->TextOut(rc.left, rc.bottom - 84, theApp.m_sHospitalName);
	}
}

void CPrintTendWave::DrawGrid(CDC* pdc, CRect& rc)
{
	int i;
	CBrush brTable(cr_normal);

	CPen penRed(PS_SOLID, 1, cr_minite);
	CPen penDark(PS_SOLID, 1, cr_grid_dark);
	CPen penLight(PS_SOLID, 1, cr_grid_light);
	CPen* pOldPen = pdc->SelectObject(&penLight);

	pdc->SetTextColor(cr_scale);

	int nNibpUnit = m_pReviewDlg->GetNIBPUnitOpt();
	int nTempUnit = m_pReviewDlg->GetTEMPUnitOpt();

	//心率
	int x = rc.left + table_left;
	int y = rc.top - table_top;
	int w = rc.Width() - table_left;
	int h = hr_table_h;
	for (i=0; i<=w; i+= grid_w) {
		if (0 == i % (grid_w * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y - h);
	}
	for (i=0; i<=h; i+= hr_grid_h) {
		if (0 == i % (hr_grid_h * 5)) {
			CString s;
			s.Format(_T("%3d"), (int)(hr_max - i / hr_scale));
			pdc->TextOut(x - scale_w, y - i + half_scale_h, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y - i);
		pdc->LineTo(x + table_w, y - i);
	}

	//血压
	y = y - h - table_gap;
	h = bp_table_h;
	for (i=0; i<=w; i+= grid_w) {
		if (0 == i % (grid_w * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y - h);
	}
	for (i=0; i<=h; i+= bp_grid_h) {
		if (0 == i % (bp_grid_h * 5)) {
			CString s = itoNibpString(nNibpUnit, (int)(bp_max - i / bp_scale));
			pdc->TextOut(x - scale_w, y - i + half_scale_h, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y - i);
		pdc->LineTo(x + table_w, y - i);
	}

	//血氧和呼吸率
	y = y - h - table_gap;
	h = rr_table_h;
	for (i=0; i<=w; i+= grid_w) {
		if (0 == i % (grid_w * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y - h);
	}
	for (i=0; i<=h; i+= rr_grid_h) {
		if (0 == i % (rr_grid_h * 4)) {
			CString s;
			s.Format(_T("%3d"), (int)(rr_max - i / rr_scale));
			pdc->TextOut(x - scale_w, y - i + half_scale_h, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y - i);
		pdc->LineTo(x + table_w, y - i);
	}

	//体温
	y = y - h - table_gap;
	h = tt_table_h;
	for (i=0; i<=w; i+= grid_w) {
		if (0 == i % (grid_w * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y - h);
	}
	for (i=0; i<=h; i+= tt_grid_h) {
		if (0 == i % (tt_grid_h * 4)) {
			CString s = itoTempString(nTempUnit, (int)((tt_max - i / tt_scale) * 10));
			pdc->TextOut(x - scale_w - 36, y - i + half_scale_h, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y - i);
		pdc->LineTo(x + table_w, y - i);
	}

	pdc->SelectObject(pOldPen);
}

void CPrintTendWave::DrawTime(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CTime tmTableStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	if (tmTableStart == 0) {
		return;
	}

	CString sTime = tmTableStart.Format(_T("%Y-%m-%d %H:%M:%S"));
	int nDrawLen = min(nValidLen, (int)((table_w - 1) / time_scale));
	pdc->SetTextColor(cr_info);

	int x = rc.left + table_left;
	int y = rc.top - table_top;
	int h = hr_table_h;
	DrawTimeRuler(pdc, x, y-h, tmTableStart, nDrawLen);
	y = y - h - table_gap;
	h = bp_table_h;
	DrawTimeRuler(pdc, x, y-h, tmTableStart, nDrawLen);
	y = y - h - table_gap;
	h = rr_table_h;
	DrawTimeRuler(pdc, x, y-h, tmTableStart, nDrawLen);
}

void CPrintTendWave::DrawTimeRuler(CDC* pdc, int x, int y, CTime tm, int nSeconds)
{
	int j;
	int count = nSeconds / 60; //两个时间标之间间距1分钟
	
	for (j = 0; j <= count; j ++) {
		pdc->MoveTo(x, y - 4);
		pdc->LineTo(x, y - text_scale_h);

		CTimeSpan ts(0, 0, 0, j * 60);
		CTime tmMark = tm + ts;
		pdc->TextOut(x + 2, y - 3, tmMark.Format(_T("%H:%M:%S")));
		x += (int)(60 * time_scale);
	}
}

void CPrintTendWave::DrawTableInfo(CDC* pdc, CRect& rc)
{
	pdc->SetTextColor(cr_info);

	CString sNibpUnit = CFMDict::NIBPUnit_itos(m_pReviewDlg->GetNIBPUnitOpt());
	CString sTempUnit = CFMDict::TEMPUnit_itos(m_pReviewDlg->GetTEMPUnitOpt());
	CString sItemName;

	int y = rc.top - table_top;
	int h = hr_table_h;
	pdc->TextOutW(rc.left - 50, y, _T("心率"));
	y = y - h - table_gap;
	h = bp_table_h;
	sItemName.Format(_T("血压(%s)"), sNibpUnit);
	pdc->TextOutW(rc.left - 50, y, sItemName);
	y = y - h - table_gap;
	h = rr_table_h;
	pdc->TextOutW(rc.left - 50, y, _T("血氧"));
	pdc->TextOutW(rc.left - 50, y - text_scale_h, _T("呼吸"));
	y = y - h - table_gap;
	sItemName.Format(_T("体温(%s)"), sTempUnit.Right(1));
	pdc->TextOutW(rc.left - 50, y, sItemName);

	//绘制颜色标注
	y = rc.top - table_top;
	h = hr_table_h;
	//HR
	ResetGetPen();
	if (m_pReviewDlg->m_bPrintHR) {
		pdc->SelectObject(GetPen());
		pdc->MoveTo(rc.left - 70, y - 200 - half_scale_h);
		pdc->LineTo(rc.left + 30, y - 200 - half_scale_h);
		pdc->TextOutW(rc.left + 50, y - 200, _T("HR"));
	}
	y = y - h - table_gap;
	h = bp_table_h;
	//血压
	ResetGetPen();
	if (m_pReviewDlg->m_bPrintSYS) {
		pdc->SelectObject(GetPen());
		pdc->MoveTo(rc.left - 70, y - 200 - half_scale_h);
		pdc->LineTo(rc.left + 30, y - 200 - half_scale_h);
		pdc->TextOutW(rc.left + 50, y - 200, _T("SYS"));
	}
	if (m_pReviewDlg->m_bPrintMEA) {
		pdc->SelectObject(GetPen());
		pdc->MoveTo(rc.left - 70, y - 250 - half_scale_h);
		pdc->LineTo(rc.left + 30, y - 250 - half_scale_h);
		pdc->TextOutW(rc.left + 50, y - 250, _T("MEA"));
	}
	if (m_pReviewDlg->m_bPrintDIA) {
		pdc->SelectObject(GetPen());
		pdc->MoveTo(rc.left - 70, y - 300 - half_scale_h);
		pdc->LineTo(rc.left + 30, y - 300 - half_scale_h);
		pdc->TextOutW(rc.left + 50, y - 300, _T("DIA"));
	}
	y = y - h - table_gap;
	h = rr_table_h;
	//血氧和呼吸
	ResetGetPen();
	if (m_pReviewDlg->m_bPrintSPO2) {
		pdc->SelectObject(GetPen());
		pdc->MoveTo(rc.left - 70, y - 200 - half_scale_h);
		pdc->LineTo(rc.left + 30, y - 200 - half_scale_h);
		pdc->TextOutW(rc.left + 50, y - 200, _T("SPO2"));
	}
	if (m_pReviewDlg->m_bPrintRR) {
		pdc->SelectObject(GetPen());
		pdc->MoveTo(rc.left - 70, y - 250 - half_scale_h);
		pdc->LineTo(rc.left + 30, y - 250 - half_scale_h);
		pdc->TextOutW(rc.left + 50, y - 250, _T("RR"));
	}
	y = y - h - table_gap;
	//体温
	ResetGetPen();
	if (m_pReviewDlg->m_bPrintT1) {
		pdc->SelectObject(GetPen());
		pdc->MoveTo(rc.left - 70, y - 150 - half_scale_h);
		pdc->LineTo(rc.left + 30, y - 150 - half_scale_h);
		pdc->TextOutW(rc.left + 50, y - 150, _T("T1"));
	}
	if (m_pReviewDlg->m_bPrintT2) {
		pdc->SelectObject(GetPen());
		pdc->MoveTo(rc.left - 70, y - 200 - half_scale_h);
		pdc->LineTo(rc.left + 30, y - 200 - half_scale_h);
		pdc->TextOutW(rc.left + 50, y - 200, _T("T2"));
	}
}

void CPrintTendWave::DrawData(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CLIENT_DATA* pData = m_pReviewDlg->GetCurrentBufferPointer(&nValidLen);
	if (! pData) {
		return;
	}

	int nDrawLen = min(nValidLen, (int)(table_w / time_scale));

	int i;
	WORD HR[2];
	WORD SYS[2];
	WORD MEA[2];
	WORD DIA[2];
	WORD SPO[2];
	WORD RR[2];
	WORD T1[2];
	WORD T2[2];
	int x = rc.left + table_left;
	for (i=0; i<nDrawLen; i++) {
		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		if (0 == i) {
			HR[0]  = CHECK_HR(pe->hr);
			SYS[0] = CHECK_NIBP(pn->sys);
			MEA[0] = CHECK_NIBP(pn->mea);
			DIA[0] = CHECK_NIBP(pn->dia);
			SPO[0] = CHECK_SPO2(ps->spo2);
			RR[0]  = CHECK_RR(pr->rr);
			T1[0]  = CHECK_TEMP(pt->t1);
			T2[0]  = CHECK_TEMP(pt->t2);
		}
		else {
			HR[0]   = HR[1];
			SYS[0]  = SYS[1];
			MEA[0]  = MEA[1];
			DIA[0]  = DIA[1];
			SPO[0]  = SPO[1];
			RR[0]   = RR[1];
			T1[0]   = T1[1];
			T2[0]   = T2[1];
		}
		HR[1]  = CHECK_HR(pe->hr);
		SYS[1] = CHECK_NIBP(pn->sys);
		MEA[1] = CHECK_NIBP(pn->mea);
		DIA[1] = CHECK_NIBP(pn->dia);
		SPO[1] = CHECK_SPO2(ps->spo2);
		RR[1]  = CHECK_RR(pr->rr);
		T1[1]  = CHECK_TEMP(pt->t1);
		T2[1]  = CHECK_TEMP(pt->t2);

		int y = rc.top - table_top;
		int h = hr_table_h;
		int x1 = x + i * time_scale;
		int x2 = x + (i+1) * time_scale;
		ResetGetPen();
		if (m_pReviewDlg->m_bPrintHR) {
			if (INVALID_VALUE != HR[0] && INVALID_VALUE != HR[1]) {
				pdc->SelectObject(GetPen());
				pdc->MoveTo(x1, y - (hr_max - HR[0]) * hr_scale);
				pdc->LineTo(x2, y - (hr_max - HR[1]) * hr_scale);
			}
		}
		y = y - h - table_gap;
		h = bp_table_h;
		ResetGetPen();
		if (m_pReviewDlg->m_bPrintSYS) {
			if (INVALID_VALUE != SYS[0] && INVALID_VALUE != SYS[1]) {
				pdc->SelectObject(GetPen());
				pdc->MoveTo(x1, y - (bp_max - SYS[0]) * bp_scale);
				pdc->LineTo(x2, y - (bp_max - SYS[1]) * bp_scale);
			}
		}
		if (m_pReviewDlg->m_bPrintMEA) {
			if (INVALID_VALUE != MEA[0] && INVALID_VALUE != MEA[1]) {
				pdc->SelectObject(GetPen());
				pdc->MoveTo(x1, y - (bp_max - MEA[0]) * bp_scale);
				pdc->LineTo(x2, y - (bp_max - MEA[1]) * bp_scale);
			}
		}
		if (m_pReviewDlg->m_bPrintDIA) {
			if (INVALID_VALUE != DIA[0] && INVALID_VALUE != DIA[1]) {
				pdc->SelectObject(GetPen());
				pdc->MoveTo(x1, y - (bp_max - DIA[0]) * bp_scale);
				pdc->LineTo(x2, y - (bp_max - DIA[1]) * bp_scale);
			}
		}
		y = y - h - table_gap;
		h = rr_table_h;
		ResetGetPen();
		if (m_pReviewDlg->m_bPrintSPO2) {
			if (INVALID_VALUE != SPO[0] && INVALID_VALUE != SPO[1]) {
				pdc->SelectObject(GetPen());
				pdc->MoveTo(x1, y - (rr_max - SPO[0]) * rr_scale);
				pdc->LineTo(x2, y - (rr_max - SPO[1]) * rr_scale);
			}
		}
		if (m_pReviewDlg->m_bPrintRR) {
			if (INVALID_VALUE != RR[0] && INVALID_VALUE != RR[1]) {
				pdc->SelectObject(GetPen());
				pdc->MoveTo(x1, y - (rr_max - RR[0]) * rr_scale);
				pdc->LineTo(x2, y - (rr_max - RR[1]) * rr_scale);
			}
		}
		y = y - h - table_gap;
		ResetGetPen();
		if (m_pReviewDlg->m_bPrintT1) {
			if (INVALID_VALUE != T1[0] && INVALID_VALUE != T1[1]) {
				pdc->SelectObject(GetPen());
				pdc->MoveTo(x1, y - (tt_max - T1[0] * 0.1f) * tt_scale);
				pdc->LineTo(x2, y - (tt_max - T1[1] * 0.1f) * tt_scale);
			}
		}
		if (m_pReviewDlg->m_bPrintT2) {
			if (INVALID_VALUE != T2[0] && INVALID_VALUE != T2[1]) {
				pdc->SelectObject(GetPen());
				pdc->MoveTo(x1, y - (tt_max - T2[0] * 0.1f) * tt_scale);
				pdc->LineTo(x2, y - (tt_max - T2[1] * 0.1f) * tt_scale);
			}
		}
	}
}
